/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/22 18:03:12 by vlesage           #+#    #+#             */
/*   Updated: 2015/03/06 16:36:51 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include <stdarg.h>
# include <limits.h>
# include <stdint.h>
# include <wchar.h>
# include <unistd.h>
# include <stdlib.h>

typedef struct			s_env
{
	int				minus;
	int				plus;
	int				sharp;
	int				space;
	int				zero;
	int				size;
	int				precision;
	int				isprecision;
	int				per_cent;
	int				hh;
	int				h;
	int				ll;
	int				l;
	int				j;
	int				z;
	char			spec;
	int				result;
	int				i;
	char			*quest;
	char			*world;
	char			c;
	long long int	arg;
}						t_env;

typedef	struct			s_tab
{
	int					(*tab_ft_conv[126])(va_list ap, t_env *env);
}						t_tab;

int						ft_printf(const char *format, ...);
int						ft_conv(va_list ap, t_env *env, t_tab *tab);
int						ft_flag_analyse(char *format, t_env *env);
void					ft_init_flags(t_env *env);
void					ft_init_tab(t_tab *tab);
int						ft_percent(va_list ap, t_env *env);
void					ft_putnbr_ll(long long int arg);
int						ft_ll_len(long long int arg);
void					ft_putnbr_ull(unsigned long long int arg);
int						ft_ull_len(unsigned long long int arg);
int						ft_print_signed_number(long long int arg, t_env *env);
int						ft_print_unsigned_number(char *arg, t_env *env);
int						ft_print_unsigned_o_number(char *arg, t_env *env);
int						ft_print_chars(char *arg, t_env *env);
int						ft_s(va_list ap, t_env *env);
int						ft_d(va_list ap, t_env *env);
long long int			ft_d_jz(va_list ap, t_env *env);
int						ft_c(va_list ap, t_env *env);
int						ft_o(va_list ap, t_env *env);
int						ft_x(va_list ap, t_env *env);
int						ft_u(va_list ap, t_env *env);
int						ft_print_charc(char arg, t_env *env);
int						ft_print_c(t_env *env);
int						ft_print_s(t_env *env);
char					*ft_conv_octal(unsigned long long int arg, t_env *env);
char					*ft_conv_hexa(unsigned long long int arg, t_env *env);
char					*ft_ulltoa(unsigned long long int arg);
unsigned long long int	ft_ojz(va_list ap, t_env *env);
char					*ft_strjoin_c(char c);
void					ft_putstr_n(char *s, int start, int len);
int						ft_strcmp(const char *s1, const char *s2);
void					ft_printf_conv_unicode(wchar_t arg, t_env *env);
char					*ft_strcat(char *s1, const char *s2);
char					*ft_strncpy(char *dst, const char *src, size_t n);
char					*ft_strnew(size_t size);
char					*ft_cs(wchar_t arg);
int						ft_wchar_len(wchar_t arg);
int						ft_size_arg_null_s(int i, char c, int j);
int						ft_size_arg_null(int i, char *c, int j);
int						ft_size_sn(long long int arg, t_env *env, int excep);
int						ft_isdigit(int c);
void					ft_putchar(char c);
void					ft_putstr(char const *s);
int						ft_strcmp(const char *s1, const char *s2);
char					*ft_strdup(const char *src);
char					*ft_strjoin(char const *s1, char const *s2);
size_t					ft_strlen(const char *s);
char					*ft_strnew(size_t size);
char					*ft_strsub(char const *s, unsigned int start,
	size_t len);
char					*ft_strcpy(char *dst, const char *src);
char					*ft_strcat(char *dest, const char *src);

#endif
