/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/12 16:44:57 by vlesage           #+#    #+#             */
/*   Updated: 2015/06/15 17:13:46 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void	ft_init_i(t_env *env)
{
	env->result = 0;
	env->i = 0;
}

int			ft_printf(const char *format, ...)
{
	va_list		ap;
	t_env		env;
	t_tab		tab;

	ft_init_i(&env);
	va_start(ap, format);
	ft_init_tab(&tab);
	while (format[env.i])
	{
		if (format[env.i] == '%' && format[env.i + 1])
		{
			ft_init_flags(&env);
			env.i += 1 + ft_flag_analyse((char *)(format + env.i + 1), &env);
			env.result += ft_conv(ap, &env, &tab);
		}
		else if (format[env.i] != '%' && format[env.i])
		{
			ft_putchar(format[env.i]);
			env.result++;
		}
		else
			return (env.result);
		env.i++;
	}
	return (env.result);
}
