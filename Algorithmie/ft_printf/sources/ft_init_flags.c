/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_flags.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/17 10:34:40 by vlesage           #+#    #+#             */
/*   Updated: 2015/03/06 16:10:26 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void ft_init_flags(t_env *env)
{
	env->space = 0;
	env->sharp = 0;
	env->zero = 0;
	env->minus = 0;
	env->plus = 0;
	env->size = 0;
	env->precision = 0;
	env->isprecision = 0;
	env->per_cent = 0;
	env->hh = 0;
	env->h = 0;
	env->ll = 0;
	env->l = 0;
	env->j = 0;
	env->z = 0;
	env->c = 0;
}
