/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_d_jz.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/24 12:07:31 by vlesage           #+#    #+#             */
/*   Updated: 2015/03/05 09:55:57 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

long long int ft_d_jz(va_list ap, t_env *env)
{
	long long int	arg;
	size_t			arg2;

	arg = 0;
	if (env->j)
	{
		arg = (long long int)va_arg(ap, intmax_t);
	}
	else if (env->z)
	{
		arg = va_arg(ap, long long int);
		if (arg < 0)
			arg = -arg;
		arg2 = (size_t)arg;
		arg = (long long int)arg;
	}
	(void)arg2;
	return (arg);
}
