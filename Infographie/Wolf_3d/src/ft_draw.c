/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_draw.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/23 13:42:10 by vlesage           #+#    #+#             */
/*   Updated: 2016/06/24 19:24:40 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void	ft_pixel_put_to_image(t_env *e, int x, int y)
{
	unsigned char	r;
	unsigned char	g;
	unsigned char	b;

	r = (e->img_color >> 16);
	g = (e->img_color >> 8);
	b = (e->img_color);
	if (x >= 0 && x <= e->width && y >= 0 && y <= e->height)
	{
		e->data[y * e->sizeline + x * e->bpp / 8] = b;
		e->data[y * e->sizeline + x * e->bpp / 8 + 1] = g;
		e->data[y * e->sizeline + x * e->bpp / 8 + 2] = r;
	}
}

int		ft_draw(t_env *e)
{
	int		y;

	y = 0;
	while (y <= e->height)
	{
		if (y == e->drawstart)
		{
			if (e->side == 1)
				ft_color(e, 1);
			else
				ft_color(e, 256);
		}
		else if (y < e->drawstart)
			e->img_color = 0x808080;
		else if (y > e->drawend)
			e->img_color = 0xA0522D;
		e->img_color = mlx_get_color_value(e->mlx, e->img_color);
		ft_pixel_put_to_image(e, e->x, y);
		y++;
	}
	return (0);
}

int		ft_color(t_env *e, int color)
{
	if (e->map[e->mapx][e->mapy] == 1)
		e->img_color = 0xF0F0F0 * color;
	else if (e->map[e->mapx][e->mapy] == 2)
		e->img_color = 0x00FF00 * color;
	else if (e->map[e->mapx][e->mapy] == 3)
		e->img_color = 0x0000FF * color;
	else if (e->map[e->mapx][e->mapy] == 4)
		e->img_color = 0xFFF000 * color;
	else if (e->map[e->mapx][e->mapy] == 5)
		e->img_color = 0x000FFF * color;
	return (0);
}
