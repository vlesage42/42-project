/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/27 18:14:53 by vlesage           #+#    #+#             */
/*   Updated: 2016/06/25 17:11:27 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int		ft_calc(t_env *e)
{
	e->x = 0;
	while (e->x < e->width)
	{
		ft_init_rey(e);
		ft_raycast(e);
		ft_raycast2(e);
		e->lineheight = (int)(e->height / e->perpwalldist);
		e->drawstart = -(e->lineheight) / 2 + e->height / 2;
		if (e->drawstart < 0)
			e->drawstart = 0;
		e->drawend = e->lineheight / 2 + e->height / 2;
		if (e->drawend >= e->height)
			e->drawend = e->height - 1;
		ft_draw(e);
		e->x++;
	}
	e->oldtime = e->tmp;
	e->tmp += 40;
	e->frametime = (e->tmp - e->oldtime) / 1000.0;
	e->movespeed = e->frametime * 6.0;
	e->rotspeed = e->frametime * 5.0;
	return (0);
}

int		ft_init_rey(t_env *e)
{
	e->camerax = 2 * e->x / (double)e->width - 1;
	e->rayposx = e->posx;
	e->rayposy = e->posy;
	e->raydirx = e->dirx + e->planex * e->camerax;
	e->raydiry = e->diry + e->planey * e->camerax;
	e->mapx = (int)e->rayposx;
	e->mapy = (int)e->rayposy;
	e->deltadistx = sqrt(1 + (e->raydiry * e->raydiry) /
	(e->raydirx * e->raydirx));
	e->deltadisty = sqrt(1 + (e->raydirx * e->raydirx) /
	(e->raydiry * e->raydiry));
	e->hit = 0;
	return (0);
}

int		ft_raycast(t_env *e)
{
	if (e->raydirx < 0)
	{
		e->stepx = -1;
		e->sidedistx = (e->rayposx - e->mapx) * e->deltadistx;
	}
	else
	{
		e->stepx = 1;
		e->sidedistx = (e->mapx + 1.0 - e->rayposx) * e->deltadistx;
	}
	if (e->raydiry < 0)
	{
		e->stepy = -1;
		e->sidedisty = (e->rayposy - e->mapy) * e->deltadisty;
	}
	else
	{
		e->stepy = 1;
		e->sidedisty = (e->mapy + 1.0 - e->rayposy) * e->deltadisty;
	}
	return (0);
}

int		ft_raycast2(t_env *e)
{
	while (e->hit == 0)
	{
		if (e->sidedistx < e->sidedisty)
		{
			e->sidedistx += e->deltadistx;
			e->mapx += e->stepx;
			e->side = 0;
		}
		else
		{
			e->sidedisty += e->deltadisty;
			e->mapy += e->stepy;
			e->side = 1;
		}
		if (e->map[e->mapx][e->mapy] > 0)
			e->hit = 1;
	}
	if (e->side == 0)
		e->perpwalldist = (e->mapx - e->rayposx +
			(1 - e->stepx) / 2) / e->raydirx;
	else
		e->perpwalldist = (e->mapy - e->rayposy +
			(1 - e->stepy) / 2) / e->raydiry;
	return (0);
}
