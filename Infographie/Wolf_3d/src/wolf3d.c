/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf3d.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/25 14:59:20 by vlesage           #+#    #+#             */
/*   Updated: 2016/06/25 15:51:05 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int		expose_hook(t_env *e)
{
	ft_calc(e);
	mlx_put_image_to_window(e->mlx, e->win, e->img, 0, 0);
	return (0);
}

int		key_hook(int keycode, t_env *e)
{
	if (keycode == 53)
	{
		ft_freetabint(e);
		mlx_destroy_image(e->mlx, e->img);
		mlx_destroy_window(e->mlx, e->win);
		exit(0);
	}
	if (keycode == 13 || keycode == 126)
	{
		if (!e->map[(int)(e->posx + e->dirx * e->movespeed)][(int)e->posy])
			e->posx += e->dirx * e->movespeed;
		if (!e->map[(int)e->posx][(int)(e->posy + e->diry * e->movespeed)])
			e->posy += e->diry * e->movespeed;
	}
	if (keycode == 1 || keycode == 125)
	{
		if (!e->map[(int)(e->posx - e->dirx * e->movespeed)][(int)e->posy])
			e->posx -= e->dirx * e->movespeed;
		if (!e->map[(int)e->posx][(int)(e->posy - e->diry * e->movespeed)])
			e->posy -= e->diry * e->movespeed;
	}
	ft_key_dir(e, keycode);
	return (0);
}

int		exit_hook(t_env *e)
{
	ft_freetabint(e);
	mlx_destroy_image(e->mlx, e->img);
	mlx_destroy_window(e->mlx, e->win);
	exit(0);
	return (0);
}

int		ft_wolf3d(t_env *e)
{
	e->mlx = mlx_init();
	e->win = mlx_new_window(e->mlx, e->width, e->height, "Wolf3D");
	e->img = mlx_new_image(e->mlx, e->width, e->height);
	e->data = mlx_get_data_addr(e->img, &e->bpp, &e->sizeline, &e->endian);
	mlx_hook(e->win, 2, 0, &key_hook, e);
	mlx_hook(e->win, 17, 0, &exit_hook, e);
	mlx_loop_hook(e->mlx, loop_hook, e);
	mlx_loop(e->mlx);
	return (0);
}

int		main(void)
{
	t_env	e;

	e.av = "./map/map";
	ft_parse(&e);
	e.width = 1270;
	e.height = 720;
	e.posx = 43;
	e.posy = 20;
	e.dirx = -1;
	e.diry = 0;
	e.planex = 0;
	e.planey = 0.66;
	e.tmp = 0;
	e.oldtime = 0;
	ft_wolf3d(&e);
	return (0);
}
