/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_hook.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/29 10:59:29 by vlesage           #+#    #+#             */
/*   Updated: 2016/04/29 11:08:55 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int		ft_key_dir(t_env *e, int keycode)
{
	if (keycode == 2 || keycode == 124)
	{
		e->olddirx = e->dirx;
		e->dirx = e->dirx * cos(-(e->rotspeed)) - e->diry * sin(-(e->rotspeed));
		e->diry = e->olddirx * sin(-(e->rotspeed)) +
		e->diry * cos(-(e->rotspeed));
		e->oldplanex = e->planex;
		e->planex = e->planex * cos(-(e->rotspeed)) -
		e->planey * sin(-(e->rotspeed));
		e->planey = e->oldplanex * sin(-(e->rotspeed)) +
		e->planey * cos(-(e->rotspeed));
	}
	if (keycode == 0 || keycode == 123)
	{
		e->olddirx = e->dirx;
		e->dirx = e->dirx * cos(e->rotspeed) - e->diry * sin(e->rotspeed);
		e->diry = e->olddirx * sin(e->rotspeed) + e->diry * cos(e->rotspeed);
		e->oldplanex = e->planex;
		e->planex = e->planex * cos(e->rotspeed) - e->planey * sin(e->rotspeed);
		e->planey = e->oldplanex * sin(e->rotspeed) +
		e->planey * cos(e->rotspeed);
	}
	return (0);
}

int		loop_hook(t_env *e)
{
	ft_memset(e->data, 0, (4 * e->width * e->height));
	expose_hook(e);
	return (0);
}
