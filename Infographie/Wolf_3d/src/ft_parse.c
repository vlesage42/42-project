/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_parse.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/29 11:14:46 by vlesage           #+#    #+#             */
/*   Updated: 2016/06/23 16:24:15 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int		ft_parse(t_env *e)
{
	char	**numb;
	char	*line;
	int		fd;
	int		i;

	fd = open(e->av, O_RDONLY);
	if (fd == -1)
		ft_parse_error(e, 1);
	i = 0;
	line = NULL;
	while (get_next_line(fd, &line) == 1)
	{
		numb = ft_strsplit(line, ' ');
		ft_strdel(&line);
		if (i == 0 || i == 1)
			ft_parse_start(numb, i, e);
		else
			ft_get_map(e, numb, i - 2);
		i++;
	}
	ft_strdel(&line);
	close(fd);
	return (0);
}

int		ft_parse_start(char **numb, int i, t_env *e)
{
	if (i == 0)
	{
		e->h = ft_atoi(numb[i]);
		e->map = (int **)malloc(sizeof(int *) * e->h);
	}
	if (i == 1)
		e->w = ft_atoi(numb[0]);
	ft_tabdel(numb);
	return (0);
}

int		ft_get_map(t_env *e, char **numb, int i)
{
	int		j;

	j = -1;
	e->map[i] = (int *)malloc(sizeof(int) * e->w);
	while (numb[++j])
	{
		e->map[i][j] = ft_atoi(numb[j]);
		if (i == 0 || i == e->h + 1)
		{
			if (e->map[i][j] == 0)
				ft_parse_error(e, 2);
		}
		else if (e->map[i][0] == 0)
			ft_parse_error(e, 2);
	}
	if (j != e->w || e->map[i][j - 1] == 0)
		ft_parse_error(e, 2);
	ft_tabdel(numb);
	return (0);
}

int		ft_parse_error(t_env *e, int cpt)
{
	if (cpt == 1)
	{
		ft_putstr("Error : file don't open !\n");
		ft_freetabint(e);
		exit(0);
	}
	else if (cpt == 2)
	{
		ft_putstr("Error : invalid map !\n");
		ft_freetabint(e);
		exit(0);
	}
	return (0);
}

void	ft_freetabint(t_env *e)
{
	int		i;

	i = 0;
	while (i < e->h)
	{
		free(e->map[i]);
		i++;
	}
	free(e->map);
}
