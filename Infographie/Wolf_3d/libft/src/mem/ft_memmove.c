/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/06 18:55:27 by vlesage           #+#    #+#             */
/*   Updated: 2014/11/12 13:36:41 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"
#include <string.h>
#include <stdlib.h>

void	*ft_memmove(void *dest, const void *src, size_t len)
{
	size_t		i;
	size_t		j;
	const char	*s;
	char		*d;
	char		*temp;

	i = 0;
	j = 0;
	s = (const char *)src;
	d = (char *)dest;
	temp = (char *)malloc(sizeof(char) * ft_strlen(s));
	while (i < len)
	{
		temp[i] = s[i];
		i++;
	}
	while (j < len)
	{
		d[j] = temp[j];
		j++;
	}
	return (dest);
}
