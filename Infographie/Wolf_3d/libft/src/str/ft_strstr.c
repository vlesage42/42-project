/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 10:58:42 by vlesage           #+#    #+#             */
/*   Updated: 2015/02/20 14:11:35 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

char	*ft_strstr(const char *s1, const char *s2)
{
	int		i;
	int		h;
	char	*a;

	i = 0;
	h = 0;
	a = (char *)s1;
	if (!s1)
		return (NULL);
	if (*s2 == '\0')
		return (a);
	while (s1[i] != '\0')
	{
		if (s1[i] == *s2)
		{
			h = ft_strncmp(&s1[i], s2, ft_strlen(s2));
			if (h == 0)
				return (&a[i]);
		}
		i++;
	}
	return (NULL);
}
