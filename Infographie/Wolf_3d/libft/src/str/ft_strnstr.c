/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/08 16:44:47 by vlesage           #+#    #+#             */
/*   Updated: 2015/02/20 17:05:22 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

char	*ft_strnstr(const char *s1, const char *s2, size_t n)
{
	size_t	i;
	int		h;
	char	*a;

	i = -1;
	h = 0;
	a = (char *)s1;
	if (!s1 || ft_strlen(s2) > n)
		return (NULL);
	if (*s2 == '\0')
		return (a);
	while (s1[++i] != '\0' && i < n)
	{
		if (s1[i] == *s2)
		{
			if (n - i >= ft_strlen(s2))
			{
				h = ft_strncmp(&s1[i], s2, ft_strlen(s2));
				if (h == 0)
					return (&a[i]);
			}
		}
	}
	return (NULL);
}
