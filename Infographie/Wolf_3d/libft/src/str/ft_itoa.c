/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/13 16:15:39 by vlesage           #+#    #+#             */
/*   Updated: 2015/02/26 18:10:41 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

static int		ft_cmp(int n)
{
	int		cmp;

	cmp = 1;
	if (n == 0)
		return (1);
	while (n < -9 || n > 9)
	{
		n /= 10;
		cmp++;
	}
	return (cmp);
}

char			*ft_itoa(int n)
{
	char	*str;
	int		cmp;

	if (n == -2147483648)
		return ("-2147483648");
	cmp = ft_cmp(n) + (n < 0 ? 1 : 0);
	str = (char *)malloc(sizeof(str) * (cmp + 1) + (n < 0 ? 1 : 0));
	if (n < 0)
	{
		str[0] = '-';
		n = n * (-1);
	}
	str[cmp--] = '\0';
	while (n > 9)
	{
		str[cmp--] = n % 10 + '0';
		n /= 10;
	}
	if (n <= 9)
		str[cmp] = n + '0';
	return (str);
}
