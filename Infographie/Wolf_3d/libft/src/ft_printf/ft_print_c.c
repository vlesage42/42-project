/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_c.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/04 15:17:23 by vlesage           #+#    #+#             */
/*   Updated: 2015/03/06 15:18:24 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	ft_minus(int size, t_env *env)
{
	int i;
	int j;

	i = 0;
	j = ft_strlen(env->quest);
	write(1, env->quest, j);
	while (i < (size - j))
	{
		ft_putchar(' ');
		i++;
	}
	return (i + j);
}

static int	ft_zero(int size, t_env *env)
{
	int i;
	int j;

	i = 0;
	j = ft_strlen(env->quest);
	while (i < (size - j))
	{
		ft_putchar('0');
		i++;
	}
	write(1, env->quest, j);
	return (i + j);
}

static int	ft_space(int size, t_env *env)
{
	int i;
	int j;

	i = 0;
	j = ft_strlen(env->quest);
	while (i < (size - j))
	{
		ft_putchar(' ');
		i++;
	}
	write(1, env->quest, j);
	return (i + j);
}

int			ft_print_c(t_env *env)
{
	int i;

	i = 0;
	if (env->size && !env->zero && !env->minus && env->quest)
		i = ft_space(env->size, env);
	else if (env->size && env->zero && !env->minus && env->quest)
		i = ft_zero(env->size, env);
	else if (env->size && env->minus && env->quest)
		i = ft_minus(env->size, env);
	else
	{
		i = ft_strcmp(env->quest, "0") == -48 ? 1 : ft_strlen(env->quest);
		write(1, env->quest, ft_strlen(env->quest));
	}
	return (i);
}
