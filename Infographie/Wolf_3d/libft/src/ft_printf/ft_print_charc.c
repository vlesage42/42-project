/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_charc.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/06 16:42:45 by vlesage           #+#    #+#             */
/*   Updated: 2015/03/06 16:42:51 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static int	ft_minus(int size, char c)
{
	int i;

	i = 0;
	ft_putchar(c);
	while (i < (size - 1))
	{
		ft_putchar(' ');
		i++;
	}
	return (i + 1);
}

static int	ft_zero(int size, char c)
{
	int i;

	i = 0;
	while (i < (size - 1))
	{
		ft_putchar('0');
		i++;
	}
	ft_putchar(c);
	return (i + 1);
}

static int	ft_space(int size, char c)
{
	int i;

	i = 0;
	while (i < (size - 1))
	{
		ft_putchar(' ');
		i++;
	}
	ft_putchar(c);
	return (i + 1);
}

int			ft_print_charc(char arg, t_env *env)
{
	int i;

	i = 0;
	if (env->size && !env->zero && !env->minus)
		i = ft_space(env->size, arg);
	else if (env->size && env->zero && !env->minus)
		i = ft_zero(env->size, arg);
	else if (env->size && env->minus)
		i = ft_minus(env->size, arg);
	else
	{
		i = 1;
		ft_putchar(arg);
	}
	(void)env;
	return (i);
}
