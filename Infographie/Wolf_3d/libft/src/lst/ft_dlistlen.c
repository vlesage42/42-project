/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlistlen.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/19 18:55:52 by vlesage           #+#    #+#             */
/*   Updated: 2015/12/19 18:58:18 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_dlistlen(t_dlist *p_list)
{
	size_t	ret;

	ret = 0;
	if (p_list != NULL)
		ret = p_list->id;
	return (ret);
}
