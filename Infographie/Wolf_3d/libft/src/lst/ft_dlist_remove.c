/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlist_remove.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/19 17:25:15 by vlesage           #+#    #+#             */
/*   Updated: 2015/12/19 17:58:32 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_dlist	*ft_dlist_move(t_dlist *p_list, struct s_elem *p_temp)
{
	if (p_temp->p_next == NULL)
	{
		p_list->p_tail = p_temp->p_prev;
		p_list->p_tail->p_next = NULL;
	}
	else if (p_temp->p_prev == NULL)
	{
		p_list->p_head = p_temp->p_next;
		p_list->p_head->p_prev = NULL;
	}
	else
	{
		p_temp->p_next->p_prev = p_temp->p_prev;
		p_temp->p_prev->p_next = p_temp->p_next;
	}
	free(p_temp);
	p_list->id--;
	return (p_list);
}

t_dlist	*ft_dlist_remove(t_dlist *p_list, int data)
{
	struct s_elem	*p_temp;
	int				f;

	if (p_list != NULL)
	{
		p_temp = p_list->p_head;
		f = 0;
		while (p_temp != NULL && !f)
		{
			if (p_temp->data == data)
			{
				p_list = ft_dlist_move(p_list, p_temp);
				f = 1;
			}
			else
				p_temp = p_temp->p_next;
		}
	}
	return (p_list);
}
