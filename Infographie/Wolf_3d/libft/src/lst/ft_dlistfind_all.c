/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlistfind_all.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/19 19:39:50 by vlesage           #+#    #+#             */
/*   Updated: 2015/12/19 19:39:55 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_dlist	*ft_dlistfind_all(t_dlist *p_list, int data)
{
	t_dlist			*ret;
	struct s_elem	*p_temp;

	ret = NULL;
	if (p_list != NULL)
	{
		p_temp = p_list->p_head;
		while (p_temp != NULL)
		{
			if (p_temp->data == data)
			{
				if (ret == NULL)
					ret = ft_dlistnew();
				ret = ft_dlistend(ret, data);
			}
			p_temp = p_temp->p_next;
		}
	}
	return (ret);
}
