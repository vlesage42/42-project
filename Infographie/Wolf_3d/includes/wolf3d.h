/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf3d.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/25 14:58:05 by vlesage           #+#    #+#             */
/*   Updated: 2016/06/24 17:12:02 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF3D_H
# define WOLF3D_H

# include <mlx.h>
# include <math.h>
# include <fcntl.h>
# include <sys/types.h>
# include <sys/uio.h>
# include <unistd.h>
# include <stdlib.h>
# include <stdio.h>
# include "libft.h"
# include "get_next_line.h"

typedef struct	s_env
{
	void		*mlx;
	void		*win;
	void		*img;
	char		*data;
	char		*av;
	int			width;
	int			height;
	int			bpp;
	int			sizeline;
	int			endian;
	int			img_color;
	int			**map;
	int			mapwidth;
	int			mapheight;
	double		posx;
	double		posy;
	double		dirx;
	double		diry;
	double		planex;
	double		planey;
	double		oldtime;
	double		tmp;
	double		camerax;
	double		rayposx;
	double		rayposy;
	double		raydirx;
	double		raydiry;
	int			mapx;
	int			mapy;
	double		sidedistx;
	double		sidedisty;
	double		deltadistx;
	double		deltadisty;
	double		perpwalldist;
	int			stepx;
	int			stepy;
	int			hit;
	int			side;
	int			lineheight;
	int			drawstart;
	int			drawend;
	double		frametime;
	double		movespeed;
	double		rotspeed;
	double		olddirx;
	double		oldplanex;
	int			x;
	int			h;
	int			w;
}				t_env;

int				ft_wolf3d(t_env *e);
int				expose_hook(t_env *e);
int				key_hook(int keycode, t_env *e);
int				mouse_hook(int button, int x, int y, t_env *e);
int				exit_hook(t_env *e);
int				loop_hook(t_env *e);
int				ft_parse(t_env *e);
int				ft_calc(t_env *e);
int				ft_draw(t_env *e);
void			ft_pixel_put_to_image(t_env *e, int x, int y);
int				ft_key_dir(t_env *e, int keycode);
int				ft_init_rey(t_env *e);
int				ft_raycast(t_env *e);
int				ft_raycast2(t_env *e);
int				ft_parse_start(char **numb, int i, t_env *e);
int				ft_get_map(t_env *e, char **numb, int i);
int				ft_parse_error(t_env *e, int cpt);
int				ft_get_error(t_env *e);
void			ft_freetabint(t_env *e);
int				ft_color(t_env *e, int color);

#endif
