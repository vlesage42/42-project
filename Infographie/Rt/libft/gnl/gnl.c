/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gnl.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/30 17:13:23 by abartz            #+#    #+#             */
/*   Updated: 2014/11/30 17:13:25 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "get_next_line.h"

static void		st_init(t_link **lst, int fd)
{
	t_lstgnl	*gnl;

	if ((gnl = (t_lstgnl*)malloc(sizeof(t_lstgnl))))
	{
		ft_bzero(gnl->b, BS + 1);
		gnl->ex = NULL;
		if (!(*lst))
			*lst = ft_newlink(fd, gnl);
		else if (!ft_getlink_by_id(*lst, fd))
		{
			ft_addlink_tail(lst, ft_newlink(fd, gnl));
			*lst = ft_getlink_last(*lst);
		}
		else
		{
			*lst = ft_getlink_by_id(*lst, fd);
			ft_memdel((void**)(&gnl));
		}
	}
}

static int		st_process(int fd, t_lstgnl **gnl, char **line)
{
	int		err;
	char	*tmp;

	tmp = NULL;
	ft_bzero((*gnl)->b, BS + 1);
	while ((err = RD(fd, (*gnl)->b, BS)) > 0 && !ft_strchr((*gnl)->b, '\n'))
	{
		tmp = ft_strdup((*gnl)->b);
		*line = (!(*line)) ? ft_strdup_del(&tmp) : ft_strjoin_del(line, &tmp);
		ft_bzero((*gnl)->b, BS + 1);
	}
	if (ft_strchr((*gnl)->b, '\n'))
	{
		tmp = ft_strndup((*gnl)->b, ft_strchr((*gnl)->b, '\n') - (*gnl)->b);
		*line = (!(*line)) ? ft_strdup_del(&tmp) : ft_strjoin_del(line, &tmp);
		(*gnl)->ex = (ft_strchr((*gnl)->b, '\n') + 1) ?
			ft_strdup(ft_strchr((*gnl)->b, '\n') + 1) : NULL;
	}
	if (err >= 0)
		err = (!*line && !(*gnl)->ex) ? 0 : 1;
	ft_bzero((*gnl)->b, BS + 1);
	return (err);
}

static int		st_excess(int fd, t_lstgnl **gnl, char **line)
{
	int		err;
	char	*tmp;

	err = 1;
	tmp = NULL;
	if (ft_strchr((*gnl)->ex, '\n'))
	{
		*line = ft_strndup(
			(*gnl)->ex, ft_strchr((*gnl)->ex, '\n') - (*gnl)->ex);
		if (ft_strchr((*gnl)->ex, '\n') + 1)
		{
			tmp = ft_strdup(ft_strchr((*gnl)->ex, '\n') + 1);
			ft_strdel(&(*gnl)->ex);
			(*gnl)->ex = ft_strdup_del(&tmp);
		}
		else
			ft_strdel(&((*gnl)->ex));
	}
	else
	{
		*line = ft_strdup_del(&((*gnl)->ex));
		err = st_process(fd, gnl, line);
	}
	return (err);
}

int				get_next_line(int const fd, char **line)
{
	static t_link	*lst;
	t_lstgnl		*gnl;
	int				err;

	err = -1;
	if (line && fd >= 0 && !RD(fd, &err, 0))
	{
		st_init(&lst, fd);
		ft_strdel(line);
		gnl = (t_lstgnl*)(lst->child);
		err = (gnl && !(gnl->ex)) ? st_process(lst->id, &gnl, line) :
									st_excess(lst->id, &gnl, line);
		(err <= 0 && lst) ? ft_dellink_one(&lst) : ft_bzero(gnl->b, BS + 1);
	}
	return (err);
}

short			gnl(int const fd, char **line)
{
	short	err;
	short	i;
	char	c;
	char	b[256];

	i = 0;
	ft_bzero(b, 256);
	ft_strdel(line);
	while (RD(fd, &c, 1) > 0 && c != '\n' && i < 255)
	{
		b[i] = c;
		++i;
	}
	err = (b[0] || c == '\n') ? 1 : 0;
	*line = ft_strdup(b);
	return ((err));
}
