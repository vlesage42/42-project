/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector_magn_norm.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/21 13:34:28 by abartz            #+#    #+#             */
/*   Updated: 2015/11/21 13:34:30 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "infographics.h"

double	vec3_magnitude(t_vec3 vec)
{
	return (sqrt(vec.x * vec.x + vec.y * vec.y + vec.z * vec.z));
}

double	vec4_magnitude(t_vec4 vec)
{
	return (sqrt(vec.x * vec.x + vec.y * vec.y + vec.z * vec.z));
}

double	vec3_square_length(t_vec3 vec)
{
	return (vec.x * vec.x + vec.y * vec.y + vec.z * vec.z);
}

t_vec3	vec3_normalize(t_vec3 vec)
{
	double l;

	vec.len = vec3_magnitude(vec);
	l = 1 / ((!(vec.len)) ? 1 : vec.len);
	vec.x *= l;
	vec.y *= l;
	vec.z *= l;
	return (vec);
}

t_vec4	vec4_normalize(t_vec4 vec)
{
	const double m = vec4_magnitude(vec);

	vec.x /= m;
	vec.y /= m;
	vec.z /= m;
	return (vec);
}
