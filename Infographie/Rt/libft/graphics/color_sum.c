/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color_sum.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/21 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/07/20 15:54:08 by acollet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "infographics.h"

t_v3color	v3color_add(t_v3color color, double add)
{
	color.r += add;
	color.g += add;
	color.b += add;
	return (color);
}

t_v3color	v3color_sum(t_v3color c0, t_v3color c1)
{
	c0.r += c1.r;
	c0.g += c1.g;
	c0.b += c1.b;
	return (c0);
}

t_v3color	v3color_normalize(t_v3color color)
{
	(color.r > 1.) ? (color.r) = 1. : 0;
	(color.r < 0.) ? (color.r) = 0. : 0;
	(color.g > 1.) ? (color.g) = 1. : 0;
	(color.g < 0.) ? (color.g) = 0. : 0;
	(color.b > 1.) ? (color.b) = 1. : 0;
	(color.b < 0.) ? (color.b) = 0. : 0;
	return (color);
}

void		put_v3color_pixel(t_image *img, t_vec2i coord, t_v3color color)
{
	const int	i = coord.y * ((int)img->width * 4) + coord.x * 4;

	if (coord.x >= 0 && coord.x < img->width
		&& coord.y >= 0 && coord.y < img->height)
	{
		(img->data)[i + 2] = (t_uint8)((color.r <= 255.) ? color.r : 255.);
		(img->data)[i + 1] = (t_uint8)((color.g <= 255.) ? color.g : 255.);
		(img->data)[i] = (t_uint8)((color.b <= 255.) ? color.b : 255.);
		(img->data)[i + 3] = 0x00;
	}
}
