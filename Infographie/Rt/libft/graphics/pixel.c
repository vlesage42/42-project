/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pixel.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/12 16:17:57 by abartz            #+#    #+#             */
/*   Updated: 2015/09/12 16:18:02 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "infographics.h"

/*
** c++	mlx	0xRRGGBBAA avec l'alpha a 0xff
** c	mlx	0xAABBGGRR avec l'alpha a 0x00
*/

void	clear_image(t_image *img)
{
	t_uint32		i;
	t_uint8			x;
	const size_t	nb_pixel = ((img->bpp / 8) * (img->width * img->height));
	unsigned char	*ptr;

	ft_memset(img->data, 0, nb_pixel);
	i = 0;
	x = 0;
	ptr = img->data;
	while (i < nb_pixel)
	{
		if (x == 3)
		{
			x = 0;
			ptr[i] = 0xff;
		}
		else
			++x;
		++i;
	}
}

void	put_rgba_pixel(t_image *img, t_vec2i coord, unsigned int color)
{
	const int	i = coord.y * ((int)img->width * 4) + coord.x * 4;

	if (coord.x >= 0 && coord.x < img->width
		&& coord.y >= 0 && coord.y < img->height)
	{
		(img->data)[i] = ((color >> 24) & 0xff);
		(img->data)[i + 1] = ((color >> 16) & 0xff);
		(img->data)[i + 2] = ((color >> 8) & 0xff);
		(img->data)[i + 3] = (color & 0xff);
	}
}

void	put_argb_pixel(t_image *img, t_vec2i coord, unsigned int color)
{
	const int	i = coord.y * ((int)img->width * 4) + coord.x * 4;

	if (coord.x >= 0 && coord.x < img->width
		&& coord.y >= 0 && coord.y < img->height)
	{
		(img->data)[i] = (color & 0xff);
		(img->data)[i + 1] = ((color >> 24) & 0xff);
		(img->data)[i + 2] = ((color >> 16) & 0xff);
		(img->data)[i + 3] = ((color >> 8) & 0xff);
	}
}

void	put_bgra_pixel(t_image *img, t_vec2i coord, unsigned int color)
{
	const int	i = coord.y * ((int)img->width * 4) + coord.x * 4;

	if (coord.x >= 0 && coord.x < img->width
		&& coord.y >= 0 && coord.y < img->height)
	{
		(img->data)[i] = ((color >> 8) & 0xff);
		(img->data)[i + 1] = ((color >> 16) & 0xff);
		(img->data)[i + 2] = ((color >> 24) & 0xff);
		(img->data)[i + 3] = (color & 0xff);
	}
}

void	put_abgr_pixel(t_image *img, t_vec2i coord, unsigned int color)
{
	const int	i = coord.y * ((int)img->width * 4) + coord.x * 4;

	if (coord.x >= 0 && coord.x < img->width
		&& coord.y >= 0 && coord.y < img->height)
	{
		(img->data)[i] = (color & 0xff);
		(img->data)[i + 1] = ((color >> 8) & 0xff);
		(img->data)[i + 2] = ((color >> 16) & 0xff);
		(img->data)[i + 3] = ((color >> 24) & 0xff);
	}
}
