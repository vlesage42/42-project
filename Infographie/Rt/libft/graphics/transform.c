/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   transform.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/16 15:12:04 by abartz            #+#    #+#             */
/*   Updated: 2015/11/16 15:12:11 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "infographics.h"

void	rotation(t_vec4 rotate, t_vec4 *vec)
{
	t_matrix	mat_a;
	t_matrix	mat_b;
	t_matrix	mat_xyz[3];
	t_vec4		tmp;

	init_matrix(mat_a);
	init_matrix(mat_b);
	init_matrix_rotation(mat_xyz[0], rotate.x, 'x');
	init_matrix_rotation(mat_xyz[1], rotate.y, 'y');
	init_matrix_rotation(mat_xyz[2], rotate.z, 'z');
	matrice_mult(mat_xyz[0], mat_xyz[1], mat_a);
	matrice_mult(mat_a, mat_xyz[2], mat_b);
	vec4_cpy(&tmp, *vec);
	mat_vec4_mult(mat_b, tmp, vec);
}

void	translation(t_vec4 translate, t_vec4 *vec)
{
	vec->x += translate.x;
	vec->y += translate.y;
}

void	scaling(t_vec4 scaling, t_vec4 *vec)
{
	t_vec4		tmp;
	t_matrix	mat_s;

	init_matrix_scaling(mat_s, scaling);
	vec4_cpy(&tmp, *vec);
	mat_vec4_mult(mat_s, tmp, vec);
}

void	to_isometric(t_vec4 *vec)
{
	static double	tile_width = 10;
	static double	tile_height = 5;
	double			tmp;

	tmp = vec->x;
	vec->x = ((vec->x + vec->z) * tile_width) +
		((vec->y - vec->z) * tile_width);
	vec->y = ((vec->y - vec->z) * tile_height) -
		((tmp + vec->z) * tile_height);
}
