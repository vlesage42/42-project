/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector_sum.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/21 13:34:28 by abartz            #+#    #+#             */
/*   Updated: 2015/11/21 13:34:30 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "infographics.h"

t_vec3	vec3_sum(t_vec3 vec_a, t_vec3 vec_b)
{
	vec_a.x += vec_b.x;
	vec_a.y += vec_b.y;
	vec_a.z += vec_b.z;
	return (vec_a);
}

t_vec4	vec4_sum(t_vec4 vec_a, t_vec4 vec_b)
{
	vec_a.x += vec_b.x;
	vec_a.y += vec_b.y;
	vec_a.z += vec_b.z;
	return (vec_a);
}

t_vec3	vec3_add(t_vec3 vec, double add)
{
	vec.x += add;
	vec.y += add;
	vec.z += add;
	return (vec);
}

t_vec4	vec4_add(t_vec4 vec, double add)
{
	vec.x += add;
	vec.y += add;
	vec.z += add;
	return (vec);
}
