/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color_sum.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/21 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/07/20 15:54:08 by acollet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "infographics.h"

t_v3color	v3color_extract(t_v3color color)
{
	t_v3color	extract;

	extract.r = 1 - color.r;
	extract.g = 1 - color.g;
	extract.b = 1 - color.b;
	return (extract);
}

t_v3color	v3color_sub(t_v3color c0, t_v3color c1)
{
	c0.r -= c1.r;
	c0.g -= c1.g;
	c0.b -= c1.b;
	return (c0);
}
