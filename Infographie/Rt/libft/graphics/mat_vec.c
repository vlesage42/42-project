/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mat_vec.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/21 13:34:28 by abartz            #+#    #+#             */
/*   Updated: 2015/11/21 13:34:30 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "infographics.h"

void	init_vec3(t_vec3 *vec)
{
	vec->x = 0;
	vec->y = 0;
	vec->z = 0;
	vec->len = 0;
}

void	init_vec4(t_vec4 *vec)
{
	vec->x = 0;
	vec->y = 0;
	vec->z = 0;
	vec->w = 1;
}

void	mat_vec3_mult(t_matrix const mat, t_vec3 const vec, t_vec3 *result)
{
	result->x = mat[0][0] * vec.x + mat[0][1] * vec.y + mat[0][2] * vec.z;
	result->y = mat[1][0] * vec.x + mat[1][1] * vec.y + mat[1][2] * vec.z;
	result->z = mat[2][0] * vec.x + mat[2][1] * vec.y + mat[2][2] * vec.z;
}

void	mat_vec4_mult(t_matrix const mat, t_vec4 const vec, t_vec4 *result)
{
	result->x += mat[0][0] * vec.x + mat[0][1] * vec.y + mat[0][2] * vec.z
				+ mat[0][3] * vec.w;
	result->y += mat[1][0] * vec.x + mat[1][1] * vec.y + mat[1][2] * vec.z
				+ mat[1][3] * vec.w;
	result->z += mat[2][0] * vec.x + mat[2][1] * vec.y + mat[2][2] * vec.z
				+ mat[2][3] * vec.w;
	result->w += mat[3][0] * vec.x + mat[3][1] * vec.y + mat[3][2] * vec.z
				+ mat[3][3] * vec.w;
}

void	matrice_mult(t_matrix const mat0, t_matrix const mat1, t_matrix result)
{
	int i;
	int j;
	int k;

	k = 0;
	while (k < 3)
	{
		i = 0;
		while (i < 3)
		{
			j = 0;
			while (j < 3)
			{
				(result[k][i]) += mat0[k][j] * mat1[j][i];
				j++;
			}
			i++;
		}
		k++;
	}
}
