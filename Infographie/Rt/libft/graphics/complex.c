/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   infographics.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/12 16:17:57 by abartz            #+#    #+#             */
/*   Updated: 2015/09/12 16:18:02 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "infographics.h"

void	complex_add(t_complex *z1, t_complex *z2, t_complex *ret)
{
	ret->re = z1->re + z2->re;
	ret->im = z1->im + z2->im;
}

void	complex_mult(t_complex *z1, t_complex *z2, t_complex *ret)
{
	ret->re = z1->re * z2->re - z1->im * z2->im;
	ret->im = z1->re * z2->im + z2->re * z1->im;
}

void	complex_div(t_complex *z1, t_complex *z2, t_complex *ret)
{
	t_complex	tmp;
	double		m;

	m = z2->im * z2->im + z2->re * z2->re;
	tmp.re = z2->re / m;
	tmp.im = -z2->im / m;
	complex_mult(z1, &tmp, ret);
}
