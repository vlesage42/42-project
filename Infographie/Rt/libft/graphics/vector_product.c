/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector_product.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/21 13:34:28 by abartz            #+#    #+#             */
/*   Updated: 2015/11/21 13:34:30 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "infographics.h"

t_vec3	vec3_mult(t_vec3 vec, double mult)
{
	vec.x *= mult;
	vec.y *= mult;
	vec.z *= mult;
	return (vec);
}

t_vec4	vec4_mult(t_vec4 vec, double mult)
{
	vec.x *= mult;
	vec.y *= mult;
	vec.z *= mult;
	return (vec);
}

double	vec3_scalar(t_vec3 vec_a, t_vec3 vec_b)
{
	return (vec_a.x * vec_b.x + vec_a.y * vec_b.y + vec_a.z * vec_b.z);
}

t_vec3	vec3_product(t_vec3 const vec_a, t_vec3 const vec_b)
{
	t_vec3	result;

	result.x = (vec_a.y * vec_b.z) - (vec_a.z * vec_b.y);
	result.y = (vec_a.z * vec_b.x) - (vec_a.x * vec_b.z);
	result.z = (vec_a.x * vec_b.y) - (vec_a.y * vec_b.x);
	return (result);
}

t_vec4	vec4_product(t_vec4 const vec_a, t_vec4 const vec_b)
{
	t_vec4	result;

	result.x = (vec_a.y * vec_b.z) - (vec_a.z * vec_b.y);
	result.y = (vec_a.z * vec_b.x) - (vec_a.x * vec_b.z);
	result.z = (vec_a.x * vec_b.y) - (vec_a.y * vec_b.x);
	return (result);
}
