/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector_copy.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/21 13:34:28 by abartz            #+#    #+#             */
/*   Updated: 2015/11/21 13:34:30 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "infographics.h"

void	vec2_cpy(t_vec2 *dst, t_vec2 const src)
{
	dst->x = src.x;
	dst->y = src.y;
}

void	vec2i_cpy(t_vec2i *dst, t_vec2i const src)
{
	dst->x = src.x;
	dst->y = src.y;
}

void	vec3_cpy(t_vec3 *dst, t_vec3 const src)
{
	dst->x = src.x;
	dst->y = src.y;
	dst->z = src.z;
}

void	vec4_cpy(t_vec4 *dst, t_vec4 const src)
{
	dst->x = src.x;
	dst->y = src.y;
	dst->z = src.z;
	dst->w = src.w;
}
