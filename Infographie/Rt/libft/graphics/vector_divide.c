/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector_divide.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/21 13:34:28 by abartz            #+#    #+#             */
/*   Updated: 2015/11/21 13:34:30 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "infographics.h"

t_vec3	vec3_div(t_vec3 vec, double div)
{
	vec.x /= div;
	vec.y /= div;
	vec.z /= div;
	return (vec);
}

t_vec4	vec4_div(t_vec4 vec, double div)
{
	vec.x /= div;
	vec.y /= div;
	vec.z /= div;
	return (vec);
}
