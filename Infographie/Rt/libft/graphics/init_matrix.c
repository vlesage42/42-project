/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_matrix.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/21 13:33:28 by abartz            #+#    #+#             */
/*   Updated: 2015/11/21 13:33:32 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_math.h"
#include "infographics.h"

void	init_matrix(t_matrix mat)
{
	int i;
	int j;

	i = 0;
	while (i < 4)
	{
		j = 0;
		while (j < 4)
		{
			(mat[i][j]) = 0;
			j++;
		}
		i++;
	}
	mat[3][3] = 1;
}

void	init_matrix_identity(t_matrix mat)
{
	init_matrix(mat);
	mat[0][0] = 1;
	mat[1][1] = 1;
	mat[2][2] = 1;
}

void	init_matrix_rotation(t_matrix mat, double rotate, char xyz)
{
	init_matrix(mat);
	if (xyz == 'x')
	{
		mat[0][0] = 1;
		mat[1][1] = ft_cos(rotate);
		mat[1][2] = -ft_sin(rotate);
		mat[2][1] = ft_sin(rotate);
		mat[2][2] = ft_cos(rotate);
	}
	else if (xyz == 'y')
	{
		mat[0][0] = ft_cos(rotate);
		mat[0][2] = ft_sin(rotate);
		mat[1][1] = 1;
		mat[2][0] = -ft_sin(rotate);
		mat[2][2] = ft_cos(rotate);
	}
	else if (xyz == 'z')
	{
		mat[0][0] = ft_cos(rotate);
		mat[0][1] = -ft_sin(rotate);
		mat[1][0] = ft_sin(rotate);
		mat[1][1] = ft_cos(rotate);
		mat[2][2] = 1;
	}
}

void	init_matrix_scaling(t_matrix mat, t_vec4 scale)
{
	init_matrix(mat);
	mat[0][0] = scale.x;
	mat[1][1] = scale.y;
	mat[2][2] = scale.z;
	mat[3][3] = 1;
}

void	copy_matrix(t_matrix src, t_matrix dst)
{
	int i;
	int j;

	i = 0;
	while (i < 4)
	{
		j = 0;
		while (j < 4)
		{
			dst[i][j] = src[i][j];
			++j;
		}
		++i;
	}
}
