/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color_product.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/21 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/06/21 16:42:42 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "infographics.h"

t_v3color	v3color_mult(t_v3color color, double mult)
{
	color.r *= mult;
	color.g *= mult;
	color.b *= mult;
	return (color);
}

t_v3color	v3color_div(t_v3color color, double div)
{
	color.r /= div;
	color.g /= div;
	color.b /= div;
	return (color);
}

t_v3color	v3color_product(t_v3color c0, t_v3color c1)
{
	c0.r *= c1.r;
	c0.g *= c1.g;
	c0.b *= c1.b;
	return (c0);
}
