/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_nbrlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/20 23:59:10 by abartz            #+#    #+#             */
/*   Updated: 2014/11/20 23:59:12 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Retourne la longueur d'un entier.
*/

size_t	ft_nbrlen(int n)
{
	size_t	len;
	int		modulo;
	int		remain;

	len = 0;
	modulo = 10;
	remain = 0;
	if (n != 0)
	{
		while (remain != n)
		{
			remain = n % modulo;
			modulo *= 10;
			len++;
		}
	}
	else
		len = 1;
	return (len);
}
