/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dec2base8.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/16 09:16:29 by abartz            #+#    #+#             */
/*   Updated: 2015/02/16 09:16:41 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_dec2base8(int dec)
{
	char	*oct;
	int		i;
	int		remainder;

	i = 0;
	oct = NULL;
	remainder = 0;
	if ((oct = ft_strnew(ft_nbrlen(dec) + 1)))
	{
		while (dec)
		{
			remainder = dec % 8;
			remainder += 48;
			*(oct + i) = remainder;
			i++;
			dec /= 8;
		}
		oct = ft_strrev(oct);
	}
	return (oct);
}
