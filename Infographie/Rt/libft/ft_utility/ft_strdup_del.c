/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup_del.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/30 17:13:23 by abartz            #+#    #+#             */
/*   Updated: 2014/11/30 17:13:25 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Alloue et retourne une copie "fraiche" de la chaine de caracteres s1.
** Cette chaine retournee peut etre, par la suite, utilisee comme argument
** pou la fonction free. Libere s.
*/

char	*ft_strdup_del(char **s)
{
	char	*cpy;

	cpy = NULL;
	if (s && *s)
	{
		if ((cpy = ft_strnew(ft_strlen(*s))))
		{
			cpy = ft_strcpy(cpy, *s);
			ft_strdel(s);
		}
	}
	return (cpy);
}
