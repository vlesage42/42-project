/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atof.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/13 15:05:00 by abartz            #+#    #+#             */
/*   Updated: 2016/08/13 15:05:00 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

float	ft_atof(char *str)
{
	char	sign;
	float	value;
	float	decimal;

	sign = 1;
	value = 0.;
	decimal = 10.;
	while (str && *str && !ft_isdigit(*str))
	{
		if (*str == '-' && ft_isdigit(*(str + 1)))
			sign = -1;
		str++;
	}
	while (str && *str && ft_isdigit(*str))
		value = value * 10 + (*str++ - '0');
	if (str && *str == '.' && ++str)
		while (str && *str && ft_isdigit(*str))
		{
			value += (float)(*str - '0') / decimal;
			decimal *= 10.;
			str++;
		}
	value *= sign;
	return (value);
}

double	ft_ato2f(char *str)
{
	char	sign;
	double	value;
	double	decimal;

	sign = 1;
	value = 0.;
	decimal = 10.;
	while (str && *str && !ft_isdigit(*str))
	{
		if (*str == '-' && ft_isdigit(*(str + 1)))
			sign = -1;
		str++;
	}
	while (str && *str && ft_isdigit(*str))
		value = value * 10 + (*str++ - '0');
	if (str && *str == '.' && ++str)
		while (str && *str && ft_isdigit(*str))
		{
			value += (double)(*str - '0') / decimal;
			decimal *= 10.;
			str++;
		}
	value *= sign;
	return (value);
}
