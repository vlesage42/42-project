/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_wordcount.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/22 14:03:21 by abartz            #+#    #+#             */
/*   Updated: 2015/11/22 14:03:28 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Compte le nombre de 'mot' dans une chaine. Les espaces, tabulations, et
** retours a la ligne separant chaque mot.
*/

int		ft_wordcount(char *str)
{
	int count;

	count = 0;
	while (str && *str)
	{
		while (str && *str && ft_isblank(*str))
			++str;
		if (str && *str)
		{
			++count;
			while (str && *str && !ft_isblank(*str))
				++str;
		}
	}
	return (count);
}
