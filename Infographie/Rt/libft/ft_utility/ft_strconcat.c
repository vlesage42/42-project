/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strconcat.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acollet <acollet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/20 15:27:44 by acollet           #+#    #+#             */
/*   Updated: 2016/07/20 15:53:36 by acollet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_strconcat(char **s1, const char *s2)
{
	char	*temp;

	if (s2 == NULL)
		return ;
	if (*s1 == NULL)
		*s1 = ft_strdup(s2);
	else
	{
		temp = ft_strdup(*s1);
		ft_strdel(s1);
		*s1 = ft_strjoin(temp, s2);
		ft_strdel(&temp);
	}
}
