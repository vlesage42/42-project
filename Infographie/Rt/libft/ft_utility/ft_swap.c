/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_swap.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/13 15:05:00 by abartz            #+#    #+#             */
/*   Updated: 2016/08/13 15:05:00 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_swap(int *a, int *b)
{
	const int tmp = *a;

	*a = *b;
	*b = tmp;
}

void	ft_swapf(float *a, float *b)
{
	const float tmp = *a;

	*a = *b;
	*b = tmp;
}

void	ft_swap2f(double *a, double *b)
{
	const double tmp = *a;

	*a = *b;
	*b = tmp;
}
