/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbrendl.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/17 16:30:00 by abartz            #+#    #+#             */
/*   Updated: 2016/02/17 16:30:00 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Affiche l'entier n sur la sortie standard, suivi d'un retour a la ligne.
*/

void	ft_putnbrendl(int n)
{
	ft_putnbr(n);
	ft_putchar('\n');
}
