/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_newtab.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/30 14:36:00 by abartz            #+#    #+#             */
/*   Updated: 2016/01/30 14:36:00 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	st_zerotab(void **s, int count)
{
	int		i;

	i = 0;
	while (i <= count)
	{
		s[i] = NULL;
		++i;
	}
}

void	ft_deltab(void ***tab)
{
	int i;

	i = 0;
	while (*tab && *tab[i])
	{
		free(tab[i]);
		tab[i] = NULL;
		++i;
	}
	free(**tab);
	**tab = NULL;
}

void	**ft_newtab(int size, int count)
{
	void	**tab;

	if ((tab = (void**)malloc(size * (count + 1))))
		st_zerotab(tab, count);
	return (tab);
}
