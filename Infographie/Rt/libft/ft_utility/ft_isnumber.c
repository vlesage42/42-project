/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isnumber.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/22 16:14:48 by abartz            #+#    #+#             */
/*   Updated: 2015/03/22 16:14:51 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Retourne 1 ou -1, selon si la chaine de caracteres recu en parametre
** represente un entier positif ou negatif. Sinon la fonction renvoie 0.
** Bug 1 :la fonction renvoie 1 avec "0" en parametre.
** Bug 2 : la fonction renvoie -1 avec "-0" en parametre.
*/

int		ft_isnumber(char const *s)
{
	int		err;
	int		i;

	err = 1;
	i = 0;
	if (s && *s)
	{
		while (*(s + i) && !ft_isblank(*(s + i)))
		{
			if (!ft_isdigit(*(s + i)))
				err = (!i && *(s + i) == '-') ? -1 : 0;
			i++;
		}
	}
	return (err);
}
