/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_endianess.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/13 16:24:05 by abartz            #+#    #+#             */
/*   Updated: 2015/02/16 09:14:44 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Si on se trouve sur une machine little-endian, revoie 0.
** Si on se trouve sur une machine big-endian, revoie 1.
*/

int		ft_endianess(void)
{
	int x;

	x = 1;
	if (*(char*)&x == 1)
		x = 0;
	return (x);
}
