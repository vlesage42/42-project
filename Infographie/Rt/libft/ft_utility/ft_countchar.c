/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_countchar.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/30 14:36:00 by abartz            #+#    #+#             */
/*   Updated: 2016/01/30 14:36:00 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_uint8			ft_countchar(char *line, char c)
{
	t_uint8 count;

	count = 0;
	while (line && *line)
	{
		if (*line == c)
			++count;
		++line;
	}
	return (count);
}
