/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstaddend.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/10 12:51:09 by abartz            #+#    #+#             */
/*   Updated: 2014/11/10 12:51:13 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Ajoute l'element new_elem en fin de liste.
*/

void	ft_lstaddend(t_list **alst, t_list *new_elem)
{
	t_list	*cpy;

	if (alst != NULL && *alst != NULL && new_elem != NULL)
	{
		cpy = *alst;
		while (cpy->next != NULL)
			cpy = cpy->next;
		cpy->next = new_elem;
	}
}
