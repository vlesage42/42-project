/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isinteger.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/16 09:15:23 by abartz            #+#    #+#             */
/*   Updated: 2015/02/16 09:15:27 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Une chaine de caractere valide, representant un nombre, est transmis
** a cette fonction pour savoir si il s'agit d'un entier codable sur 32 bits.
*/

static void			st_del(t_number **nbr)
{
	ft_strdel(&((*nbr)->str));
	ft_memdel(((void**)(nbr)));
}

static t_number		*st_init(char const *s)
{
	t_number	*nbr;

	nbr = NULL;
	if ((nbr = (t_number*)malloc(sizeof(t_number))))
	{
		nbr->str = ft_strdup(s);
		if ((nbr->sign = ft_isnumber(s)) == 1)
			nbr->len = ft_strlen(s);
		else if (nbr->sign == -1)
			nbr->len = ft_strlen(s + 1);
		else
			st_del(&nbr);
	}
	return (nbr);
}

static int			st_compare(char *limit, char *s, int i)
{
	int		err;

	err = 1;
	if (*(s + i) && *(limit + i))
	{
		if (*(s + i) == *(limit + i))
			err = st_compare(limit, s, ++i);
		else if (*(s + i) > *(limit + i))
			err = 0;
	}
	return (err);
}

int					ft_isinteger(char const *s)
{
	int			err;
	t_number	*max;
	t_number	*min;
	t_number	*nbr;

	max = st_init("2147483647");
	min = st_init("-2147483648");
	nbr = st_init(s);
	err = 0;
	if (max && min && nbr)
	{
		if (nbr->sign == 1)
			err = (nbr->len < max->len) ? 1 : st_compare(max->str, nbr->str, 0);
		else if (nbr->sign == -1)
			err = (nbr->len < min->len) ? 1 : st_compare(min->str, nbr->str, 1);
		st_del(&nbr);
	}
	if (max)
		st_del(&max);
	if (min)
		st_del(&min);
	return (err);
}
