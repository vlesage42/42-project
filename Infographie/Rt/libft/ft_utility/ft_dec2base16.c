/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dec2base16.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/16 09:15:50 by abartz            #+#    #+#             */
/*   Updated: 2015/02/16 09:16:13 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_dec2base16(int dec)
{
	char	*hex;
	int		i;
	int		remainder;

	i = 0;
	hex = NULL;
	remainder = 0;
	if ((hex = ft_strnew(ft_nbrlen(dec))))
	{
		while (dec)
		{
			remainder = dec % 16;
			if (remainder > 9)
				remainder += 87;
			else
				remainder += 48;
			*(hex + i) = remainder;
			i++;
			dec /= 16;
		}
		hex = ft_strrev(hex);
	}
	return (hex);
}
