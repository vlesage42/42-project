/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_noblank.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/30 14:36:00 by abartz            #+#    #+#             */
/*   Updated: 2016/01/30 14:36:00 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_uint8	ft_noblank(char *line)
{
	t_uint8	err;

	err = 1;
	while (err && line && *line)
	{
		if (ft_isblank(*line))
			err = 0;
		else
			++line;
	}
	return (err);
}
