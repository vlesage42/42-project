/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin_del.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/06 17:59:59 by abartz            #+#    #+#             */
/*   Updated: 2015/03/22 16:15:55 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Alloue et retourne une chaine de caractere "fraiche" terminee
** par un '\0' resultant de la concatention de s1 et s2.
** Libere s1 et s2 en fin d'operations.
** Si l'allocation echoue, la fonction retourne NULL.
*/

char	*ft_strjoin_del(char **s1, char **s2)
{
	char	*str;
	int		len_s1;
	int		len_s2;
	int		ij[2];

	str = NULL;
	if (*s1 && *s2 && !(ij[0] = 0))
	{
		len_s1 = ft_strlen(*s1);
		len_s2 = ft_strlen(*s2);
		if ((str = ft_strnew(len_s1 + len_s2)))
		{
			ij[1] = -1;
			while (len_s1-- && ij[1]++)
				*(str + ij[0]++) = *((*s1) + ij[1]);
			ij[1] = -1;
			while (len_s2-- && ij[1]++)
				*(str + ij[0]++) = *((*s2) + ij[1]);
			ft_strdel(s1);
			ft_strdel(s2);
		}
	}
	return (str);
}
