/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strndup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/30 17:13:23 by abartz            #+#    #+#             */
/*   Updated: 2014/11/30 17:13:25 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Alloue et retourne une copie "fraiche" des n premiers charactere de
** la chaine s1. Cette chaine retournee peut etre, par la suite,
** utilisee comme argument pour la fonction free.
*/

char	*ft_strndup(char const *s, size_t n)
{
	char	*cpy;

	cpy = NULL;
	if (s)
		if ((cpy = ft_strnew(n)))
			cpy = ft_strncpy(cpy, s, n);
	return (cpy);
}
