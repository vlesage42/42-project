/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getword.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/30 14:36:00 by abartz            #+#    #+#             */
/*   Updated: 2016/01/30 14:36:00 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Retourne l'adresse du premier caractere du n eme mot de la chaine str.
*/

char	*ft_getword(t_uint8 n, char *str)
{
	t_uint8	i;
	char	*w;

	i = 0;
	w = NULL;
	while (!w && str && *str && i < n)
	{
		while (str && *str && ft_isblank(*str))
			++str;
		++i;
		if (str && *str && i == n)
			w = str;
		while (!w && str && *str && !ft_isblank(*str))
			++str;
	}
	return (w);
}
