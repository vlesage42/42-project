/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 17:00:47 by abartz            #+#    #+#             */
/*   Updated: 2014/11/21 00:12:44 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Compare lexicographiquement les chaines s1 et s2.
** Retourne un entier plus grand, egal ou plus petit que 0, suivant si
** la chaine s1 est plus grande, egale ou plus petite que la chaine s2.
** On compare la valeur decimal des caracteres.
*/

int		ft_strcmp(const char *s1, const char *s2)
{
	int err;
	int flag;

	err = 0;
	flag = 1;
	if (s1 != NULL && s2 != NULL)
	{
		while (flag)
		{
			if (*s1 != *s2)
				err = (unsigned char)*s1 - (unsigned char)*s2;
			if (err != 0 || (!*s1 && !*s2))
				flag = 0;
			s1++;
			s2++;
		}
	}
	return (err);
}
