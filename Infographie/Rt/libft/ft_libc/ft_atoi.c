/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 11:42:03 by abartz            #+#    #+#             */
/*   Updated: 2014/11/20 23:48:33 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Convertit une chaine de caractere ASCII en entier.
** Retourne le premier entier rencontre dans la chaine de caractere pointee par
** str, converti en int.
*/

int			ft_atoi(const char *str)
{
	int		sign;
	int		value;

	sign = 1;
	value = 0;
	while (str && *str && !ft_isdigit(*str))
	{
		if (*str == '-' && ft_isdigit(*(str + 1)))
			sign = -1;
		str++;
	}
	while (str && *str && ft_isdigit(*str))
		value = value * 10 + (*str++ - '0');
	value *= sign;
	return (value);
}
