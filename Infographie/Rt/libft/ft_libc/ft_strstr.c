/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 17:05:34 by abartz            #+#    #+#             */
/*   Updated: 2014/11/19 16:56:06 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Recherche la chaine s2 dans la chaine s1. Si s2 est vide, s1 est retorunee.
** Si s2 n'occure pas dans s1, NULL est retourne. Sinon, un pointeur vers le
** le premier caractere de la premiere occurence de s2 est retourne.
*/

char	*ft_strstr(const char *s1, const char *s2)
{
	int		flag;
	char	*ptr;

	ptr = NULL;
	flag = 1;
	if (s1 != NULL && s2 != NULL)
	{
		if (*s2)
		{
			while (flag && *s1)
			{
				if (ft_strncmp(s1, s2, ft_strlen(s2)) == 0)
				{
					flag = 0;
					ptr = (char*)s1;
				}
				s1++;
			}
		}
		else
			ptr = (char*)s1;
	}
	return (ptr);
}
