/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 17:00:40 by abartz            #+#    #+#             */
/*   Updated: 2014/11/21 00:12:24 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Localise la premiere occurence de c (converti en char) dans la chaine
** pointee par s. Si c est '\0', la fonction localise le '\0' de fin de chaine.
*/

char	*ft_strchr(const char *s, int c)
{
	char *ptr;

	ptr = NULL;
	if (s != NULL)
	{
		ptr = (char*)s;
		if (c == '\0')
			while (*ptr)
				ptr++;
		else
		{
			while (*s && *ptr != c)
			{
				if (*s == c)
					ptr = (char*)s;
				s++;
			}
		}
		if (*ptr != c)
			ptr = NULL;
	}
	return (ptr);
}
