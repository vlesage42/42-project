/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 17:05:03 by abartz            #+#    #+#             */
/*   Updated: 2014/11/21 00:16:11 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Localise la derniere occurence de c (converti en char) dans la chaine
** pointee par s. Si c est egale a '\0', la fonction localise le '\0'
** de fin de chaine.
*/

char	*ft_strrchr(const char *s, int c)
{
	char	*ptr;

	ptr = NULL;
	if (s != NULL)
	{
		ptr = (char*)s;
		if (c == '\0')
			while (*ptr)
				ptr++;
		else
		{
			while (*s)
			{
				if (*s == c)
				{
					ptr = (char*)s;
				}
				s++;
			}
		}
		if (*ptr != c)
			ptr = NULL;
	}
	return (ptr);
}
