/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 17:03:35 by abartz            #+#    #+#             */
/*   Updated: 2014/11/18 18:53:11 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Compare lexicographiquement les chaines s1 et s2 jusqu'a n caracteres.
** Retourne un entier plus grand, egal ou plus petit que 0, suivant si
** la chaine s1 est plus grande, egale ou plus petite que la chaine s2.
** On compare la valeur decimal des caracteres.
*/

int		ft_strncmp(const char *s1, const char *s2, size_t n)
{
	int err;
	int flag;

	err = 0;
	flag = 1;
	if (s1 != NULL && s2 != NULL)
	{
		while (flag && n--)
		{
			if (*s1 != *s2)
				err = (unsigned char)*s1 - (unsigned char)*s2;
			if (err != 0 || (!*s1 && !*s2))
				flag = 0;
			s1++;
			s2++;
		}
	}
	return (err);
}
