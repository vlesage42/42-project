/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 17:01:59 by abartz            #+#    #+#             */
/*   Updated: 2014/11/21 00:13:47 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Alloue et retourne une copie "fraiche" de la chaine de caracteres s1.
** Cette chaine retournee peut etre, par la suite, utilisee comme argument
** pou la fonction free.
*/

char	*ft_strdup(char const *s1)
{
	char	*cpy;

	cpy = NULL;
	if (s1 != NULL)
	{
		if ((cpy = ft_strnew(ft_strlen(s1))))
		{
			cpy = ft_strcpy(cpy, s1);
		}
	}
	return (cpy);
}
