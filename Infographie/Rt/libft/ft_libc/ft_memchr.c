/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/10 12:04:20 by abartz            #+#    #+#             */
/*   Updated: 2014/11/15 18:47:30 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Localise la premiere occurence de c (converti en unsigned int)
** dans la chaine de caractere s.
*/

void	*ft_memchr(const void *s, int c, size_t n)
{
	char	*ptr;
	int		flag;

	ptr = NULL;
	if (s != NULL)
	{
		flag = 1;
		ptr = (char*)s;
		while (n-- && flag)
		{
			if ((unsigned int)*ptr == (unsigned int)c)
				flag = 0;
			ptr++;
		}
		if (flag != 0)
			ptr = NULL;
		else
			ptr--;
	}
	return (ptr);
}
