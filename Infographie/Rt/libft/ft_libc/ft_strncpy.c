/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 17:03:43 by abartz            #+#    #+#             */
/*   Updated: 2014/11/18 18:14:47 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Copie n caracteres depuis src vers dst. Si src est plus petite que n
** caracteres, le reste est remplie de '\0'. Sinon, dst est non termine.
*/

char	*ft_strncpy(char *dst, const char *src, size_t n)
{
	size_t i;

	i = 0;
	if (dst != NULL && src != NULL)
	{
		while ((n - i) && *(src + i))
		{
			*(dst + i) = *(src + i);
			i++;
		}
		if (ft_strlen(src) < n)
		{
			while (i != n)
				*(dst + i++) = '\0';
		}
		else
			*(dst + i) = '\0';
	}
	return (dst);
}
