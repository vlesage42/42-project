/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/06 14:36:47 by abartz            #+#    #+#             */
/*   Updated: 2014/11/20 23:56:45 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Copie les octets de la chaine de caracteres src, vers la chaine de
** caracteres dst. Si le caractere c (converti en unsigned char)
** occure dans la chaine src, la copie s'arrete et un pointeur vers l'octet
** apres la copie de c dans la chaine dst est retourne. Sinon, n octets sont
** copies et un pointeur NULL est retourne.
*/

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	size_t	i;
	char	*cpdst;
	char	*cpsrc;
	char	*ptr;

	ptr = NULL;
	if (dst != NULL && src != NULL)
	{
		i = 0;
		cpdst = (char*)dst;
		cpsrc = (char*)src;
		while (i != n && (unsigned char)c != *(cpsrc + i))
		{
			*(cpdst + i) = *(cpsrc + i);
			i++;
		}
		if (i != n)
			ptr = cpdst + i + 1;
	}
	return (ptr);
}
