/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/06 14:37:50 by abartz            #+#    #+#             */
/*   Updated: 2014/11/10 17:00:43 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Copie n octets de la zone memoire de src vers la zone memoire dst.
** Si dst et src se chevauchent, le comportement est indetermine.
*/

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	char			*cpy_dst;
	unsigned int	i;

	if (dst != NULL && src != NULL)
	{
		cpy_dst = (char*)dst;
		i = 0;
		while (i != n)
		{
			*(cpy_dst + i) = *(((char*)src) + i);
			i++;
		}
	}
	return (dst);
}
