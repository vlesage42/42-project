/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/06 14:38:25 by abartz            #+#    #+#             */
/*   Updated: 2014/11/20 23:58:51 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Copie len octets de la chaine de caractere src vers
** la chaine de caractere dst. Les deux chaines peuvent se chevaucher.
** La copie est toujours correcte dans une maniere non destructive.
*/

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	int				i;
	char			*cpy;
	char			*cpdst;

	if (dst != NULL && src != NULL)
	{
		i = 0;
		cpy = (char*)malloc(len);
		cpy = (char*)ft_memcpy(cpy, src, len);
		cpdst = (char*)dst;
		while (len--)
		{
			*(cpdst + i) = *(cpy + i);
			i++;
		}
	}
	return (dst);
}
