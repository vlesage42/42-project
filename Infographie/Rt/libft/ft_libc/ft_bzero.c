/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 14:22:28 by abartz            #+#    #+#             */
/*   Updated: 2014/11/15 16:39:35 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Met a 0 les n premiers octets dans la chaine de caractere s.
** Si n est egale a zero, la fonction ne fait rien.
*/

void	ft_bzero(void *s, size_t n)
{
	if (s != NULL && n > 0)
	{
		ft_memset(s, 0, n);
	}
}
