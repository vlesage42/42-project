/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/06 14:37:36 by abartz            #+#    #+#             */
/*   Updated: 2014/11/20 23:58:06 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Compare les octets de la chaine de caracteres s1 a la chaine de caractere
** s2. Retourne 0 si les deux chaines sont egales. Si les deux chaines sont
** zero-length, elles sont considerees comme egales.
*/

int		ft_memcmp(const void *s1, const void *s2, size_t n)
{
	int		diff;
	char	*cps1;
	char	*cps2;

	diff = 0;
	if (s1 != NULL && s2 != NULL)
	{
		cps1 = (char*)s1;
		cps2 = (char*)s2;
		if (*cps1 == '\0' && *cps2 == '\0')
			diff = 0;
		else
		{
			while (n-- > 0 &&
					(unsigned char)*(cps1++) == (unsigned char)*(cps2++))
				;
			diff = (unsigned char)*(--cps1) - (unsigned char)*(--cps2);
		}
	}
	return (diff);
}
