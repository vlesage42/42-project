/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 17:02:37 by abartz            #+#    #+#             */
/*   Updated: 2014/11/20 22:36:09 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>

/*
** Concatene la chaine dst et src. Retourne la longueur de la chain de
** caracteres qu'elle essaye de creer. Cad la longueur de dst plus la longueur
** de src.
*/

size_t		ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t	len;
	int		i;

	len = 0;
	i = 0;
	if (dst != NULL && src != NULL)
	{
		if (size >= ft_strlen(dst))
		{
			len = ft_strlen(dst);
			size -= len + 1;
			while (size-- && *(src + i))
			{
				*(dst + len) = *(src + i);
				len++;
				i++;
			}
			*(dst + len) = '\0';
		}
		else
			len = size + ft_strlen(src);
	}
	return (len);
}
