/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/03 14:04:25 by abartz            #+#    #+#             */
/*   Updated: 2014/11/06 21:45:38 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Revoie le nombre de caractere de la chaine (le '\0' de fin
** non compris).
*/

size_t	ft_strlen(const char *s)
{
	size_t len;

	len = 0;
	while ((s + len) && *(s + len))
		len++;
	return (len);
}
