/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 17:03:28 by abartz            #+#    #+#             */
/*   Updated: 2014/11/19 18:48:37 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Ajoute les n premiers caracteres de la chaine s2 a la chaine s1
** en ecrasant le caractere nul ('\0') a la fin de s1, puis en ajoutant
** un nouveau caractere nul final. Les chaines ne doivent pas se chevaucher,
** et la chaine s1 doit etre assez grande pour acceuillir le resultat.
*/

char	*ft_strncat(char *s1, const char *s2, size_t n)
{
	int i;
	int j;

	if (s1 != NULL && s2 != NULL)
	{
		i = 0;
		while (*(s1 + i))
			i++;
		j = 0;
		while (n-- && *(s2 + j))
		{
			*(s1 + i) = *(s2 + j);
			i++;
			j++;
		}
		*(s1 + i) = '\0';
	}
	return (s1);
}
