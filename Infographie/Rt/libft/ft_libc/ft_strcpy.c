/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 17:01:37 by abartz            #+#    #+#             */
/*   Updated: 2014/11/21 00:13:23 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Copie la chaine src vers la chaine dst. Les chaines ne doivent pas
** se chevaucher. La chaine dst doit etre assez grande pour accueillir
** la copie.
*/

char	*ft_strcpy(char *dst, const char *src)
{
	int i;

	i = 0;
	if (dst != NULL && src != NULL)
	{
		while (*src)
			*(dst + i++) = *(src++);
		*(dst + i) = *(src);
	}
	return (dst);
}
