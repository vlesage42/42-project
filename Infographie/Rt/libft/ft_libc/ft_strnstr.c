/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 17:04:46 by abartz            #+#    #+#             */
/*   Updated: 2014/11/20 19:52:48 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Localise la premiere occurence de la chaine s2 dans la chaine s1, ou pas
** plus de n caractes sont recherches. Si s2 est vide, s1 est retournee.
** Si s2 n'occure pas dans s1, NULL est retourne. Sinon, un pointeur vers
** le premier caracter de la premiere occurence de s2 est retourne.
** Si on ne trouve qu'un morceau de s2 a cause d'un n trop petit,
** NULL est retourne.
*/

char	*ft_strnstr(const char *s1, const char *s2, size_t n)
{
	char			*ptr;
	int				flag;
	unsigned int	j;

	ptr = NULL;
	flag = 1;
	if (s1 != NULL && s2 != NULL)
	{
		if (*s2)
			while (*s1 && n-- && flag)
			{
				j = 0;
				while (*(s1 + j) == *(s2 + j) && j <= n)
					if (ft_strlen(s2) == ++j)
						ptr = (char*)s1;
				if (ptr)
					flag = 0;
				s1++;
			}
		else
			ptr = (char*)s1;
	}
	return (ptr);
}
