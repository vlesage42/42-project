/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 16:05:41 by abartz            #+#    #+#             */
/*   Updated: 2014/11/10 13:48:02 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Ecrit len octets de la valeur c (convertis en unsigned char)
** dans la chaine de caracteres b.
*/

void	*ft_memset(void *b, int c, size_t len)
{
	int		i;
	char	*cpy;

	i = 0;
	if (b != NULL)
	{
		cpy = (char*)b;
		while (len--)
			*(cpy + i++) = (unsigned char)c;
	}
	return (b);
}
