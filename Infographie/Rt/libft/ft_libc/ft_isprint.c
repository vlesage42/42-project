/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isprint.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 16:57:24 by abartz            #+#    #+#             */
/*   Updated: 2014/11/06 17:27:27 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Test si le caractere passe en parametre est affichable (en incluant
** les espaces).
*/

int		ft_isprint(int c)
{
	return ((c >= ' ' && c <= '~') ? 1 : 0);
}
