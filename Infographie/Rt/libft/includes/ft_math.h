/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_math.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/21 13:34:28 by abartz            #+#    #+#             */
/*   Updated: 2015/11/21 13:34:30 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_MATH_H
# define FT_MATH_H

# ifndef HAVE_M_PI
#  ifndef M_PI
#   define M_PI    3.14159265358979323846264338327950288
#  endif
# endif

double	ft_cos(double degr);
double	ft_sin(double degr);

int		ft_min(int i, int j);
float	ft_minf(float i, float j);
double	ft_min2f(double i, double j);

int		ft_max(int i, int j);
float	ft_maxf(float i, float j);
double	ft_max2f(double i, double j);

int		ft_clamp(int v, int lo, int hi);
float	ft_clampf(float v, float lo, float hi);
double	ft_clamp2f(double v, double lo, double hi);

#endif
