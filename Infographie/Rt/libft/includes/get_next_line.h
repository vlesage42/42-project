/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/21 14:09:59 by abartz            #+#    #+#             */
/*   Updated: 2015/02/22 13:26:34 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# include <fcntl.h>
# include <sys/types.h>
# include <sys/stat.h>

# define BS 2

typedef struct	s_lstgnl
{
	char	b[BS + 1];
	char	*ex;
}				t_lstgnl;

int				get_next_line(int const fd, char **line);
short			gnl(int const fd, char **line);

#endif
