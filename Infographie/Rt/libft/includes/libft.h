/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 15:33:51 by abartz            #+#    #+#             */
/*   Updated: 2016/08/05 16:45:48 by acollet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H
# ifndef _WIN32
#  include <unistd.h>
#  define OP open
#  define RD read
#  define WR write
#  define CL close
# else
#  include <io.h>
#  define OP _open
#  define RD _read
#  define WR _write
#  define CL _close
# endif

# include <stdlib.h>
# include <string.h>

# define _INT_MAX_ (2147483647)
# define _INT_MIN_ (-2147483647 -1)

typedef unsigned char	t_uint8;
typedef unsigned short	t_uint16;
typedef unsigned int	t_uint32;
typedef size_t			t_size;
typedef enum	e_bool
{
	_false,
	_true
}				t_bool;

/*
** I
*/

void				*ft_memset(void *b, int c, size_t len);
void				ft_bzero(void *s, size_t n);
void				*ft_memcpy(void *dst, const void *src, size_t n);
void				*ft_memccpy(void *dst, const void *src, int c, size_t n);
void				*ft_memmove(void *dst, const void *src, size_t len);
void				*ft_memchr(const void *s, int c, size_t n);
int					ft_memcmp(const void *s1, const void *s2, size_t n);
size_t				ft_strlen(const char *s);
char				*ft_strdup(const char *s1);
char				*ft_strcpy(char *dst, const char *src);
char				*ft_strncpy(char *dst, const char *src, size_t n);
char				*ft_strcat(char *s1, const char *s2);
char				*ft_strncat(char *s1, const char *s2, size_t n);
size_t				ft_strlcat(char *dst, const char *src, size_t size);
char				*ft_strchr(const char *s, int c);
char				*ft_strrchr(const char *s, int c);
char				*ft_strstr(const char *s1, const char *s2);
char				*ft_strnstr(const char *s1, const char *s2, size_t n);
int					ft_strcmp(const char *s1, const char *s2);
int					ft_strncmp(const char *s1, const char *s2, size_t n);
int					ft_atoi(const char *str);
int					ft_isalpha(int c);
int					ft_isdigit(int c);
int					ft_isalnum(int c);
int					ft_isascii(int c);
int					ft_isprint(int c);
int					ft_toupper(int c);
int					ft_tolower(int c);

/*
** II
*/

void				*ft_memalloc(size_t size);
void				ft_memdel(void **ap);
char				*ft_strnew(size_t size);
void				ft_strdel(char **as);
void				ft_strclr(char *s);
void				ft_striter(char *s, void(*f)(char *));
void				ft_striteri(char *s, void(*f)(unsigned int, char *));
char				*ft_strmap(char const *s, char(*f)(char));
char				*ft_strmapi(char const *s, char(*f)(unsigned int, char));
int					ft_strequ(char const *s1, char const *s2);
int					ft_strnequ(char const *s1, char const *s2, size_t n);
char				*ft_strsub(char const *s, unsigned int start, size_t len);
char				*ft_strjoin(char const *s1, char const *s2);
char				*ft_strtrim(char const *s);
char				**ft_strsplit(char const *s, char c);
char				*ft_itoa(int n);
void				ft_putchar(char c);
void				ft_putstr(char const *s);
void				ft_putendl(char const *s);
void				ft_putnbr(int n);
void				ft_putchar_fd(char c, int fd);
void				ft_putstr_fd(char const *s, int fd);
void				ft_putendl_fd(char const *s, int fd);
void				ft_putnbr_fd(int n, int fd);

/*
** Bonus
*/

typedef struct		s_list
{
	void			*content;
	size_t			content_size;
	struct s_list	*next;
	struct s_list	*prev;
}					t_list;

void				ft_lstadd(t_list **alst, t_list *m_new);
void				ft_lstaddqueue(t_list **alst, t_list *m_new);
void				ft_lstdel(t_list **alst, void (*del)(void *, size_t));
void				ft_lstdelone(t_list **alst, void (*del)(void *, size_t));
void				ft_lstinsert(t_list *list, t_list	**bef, t_list **node);
void				ft_lstiter(t_list *lst, void (*f)(t_list *elem));
t_list				*ft_lstlast(t_list *alst);
size_t				ft_lstlen(t_list *lst);
t_list				*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem));
t_list				*ft_lstnew(void const *content, size_t content_size);
t_list				*ft_lstprevious(t_list *head, t_list *srch);
void				ft_lstsort(t_list **lst, int (*f)(t_list*, t_list*));
int					ft_lstswap(t_list **head, t_list **node1, t_list **node2);
void				ft_lstaddend(t_list **alst, t_list *new_elem);

/*
** Perso
*/

int					ft_endianess(void);
size_t				ft_nbrlen(int n);
int					ft_pow(int n, int power);
char				*ft_strdup_del(char **s);
char				*ft_strndup(char const *s1, size_t n);
char				*ft_strndup_del(char **s, size_t n);
char				*ft_strrev(char *str);
char				*ft_dec2base16(int dec);
char				*ft_dec2base8(int dec);
void				ft_cpynchar(char *dest, char *src, size_t n);
char				*ft_strjoin_del(char **s1, char **s2);
size_t				ft_wordlen(char *word);
int					ft_wordcount(char *str);
t_uint8				ft_countchar(char *line, char c);
char				*ft_getword(t_uint8 n, char *str);
int					ft_isblank(char c);
t_uint8				ft_noblank(char *line);
void				ft_putnbrendl(int n);
void				**ft_newtab(int size, int count);
void				ft_deltab(void ***tab);
int					ft_min(int i, int j);
int					ft_max(int i, int j);
int					ft_clamp(int v, int lo, int hi);
float				ft_atof(char *str);
double				ft_ato2f(char *str);
void				ft_swap(int *a, int *b);
void				ft_swapf(float *a, float *b);
void				ft_swap2f(double *a, double *b);

/*
** Acollet for my gnl
*/
void				ft_strconcat(char **s1, const char *s2);
char				*ft_strmove(char **str);
int					ft_strpos(const char *str, int c);
int					ft_isstrblank(const char *str);

typedef struct		s_number
{
	size_t		len;
	int			sign;
	char		*str;
}					t_number;

int					ft_isnumber(char const *s);
int					ft_isinteger(char const *s);

typedef struct		s_link
{
	int				id;
	struct s_link	*prior;
	struct s_link	*next;
	void			*child;
}					t_link;

t_link				*ft_newlink(int id, void const *child);
void				ft_dellink_one(t_link **alink);
void				ft_dellink_all(t_link **alink);
void				ft_addlink_head(t_link **alink, t_link *new_link);
void				ft_addlink_tail(t_link **alink, t_link *new_link);
int					ft_addlink(t_link *new_link, t_link **prior, t_link **next);
t_link				*ft_getlink_by_id(t_link *lst, int id);
t_link				*ft_getlink_first(t_link *lst);
t_link				*ft_getlink_last(t_link *lst);
size_t				ft_getlink_count(t_link *lst);
t_link				*ft_getlink_biggest_id(t_link *lst);
t_link				*ft_getlink_smallest_id(t_link *lst);
t_link				*ft_breaklink(t_link **alink);

#endif
