/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   infographics.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/09/12 16:17:57 by abartz            #+#    #+#             */
/*   Updated: 2015/09/12 16:18:02 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INFOGRAPHICS_H
# define INFOGRAPHICS_H

# include <math.h>
# include <stdio.h>

/*
** matrix & vector
*/
typedef double	t_matrix[4][4];

typedef struct	s_vec2i
{
	int		x;
	int		y;
}				t_vec2i;

typedef struct	s_vec2
{
	double	x;
	double	y;
}				t_vec2;

typedef struct	s_vec3
{
	double	x;
	double	y;
	double	z;
	double	len;
}				t_vec3;

typedef struct	s_vec4
{
	double	x;
	double	y;
	double	z;
	double	w;
}				t_vec4;

typedef struct	s_v3color
{
	double	r;
	double	g;
	double	b;
}				t_v3color;

/*
** some objects
*/
typedef struct	s_window
{
	void	*ptr;
	double	width;
	double	height;
}				t_window;

typedef struct	s_image
{
	void			*ptr;
	unsigned char	*data;
	int				bpp;
	int				size_line;
	double			width;
	double			height;
}				t_image;

typedef struct	s_cursor
{
	t_vec2	state;
	t_vec2	gl;
}				t_cursor;

/*
** init_matrix.c
*/
void			init_matrix(t_matrix mat);
void			init_matrix_identity(t_matrix mat);
void			init_matrix_rotation(t_matrix mat, double rotate, char xyz);
void			init_matrix_scaling(t_matrix mat, t_vec4 scale);
void			copy_matrix(t_matrix src, t_matrix dst);

/*
** vector_copy.c
*/
void			vec2_cpy(t_vec2 *dst, t_vec2 const src);
void			vec2i_cpy(t_vec2i *dst, t_vec2i const src);
void			vec3_cpy(t_vec3 *dst, t_vec3 const src);
void			vec4_cpy(t_vec4 *dst, t_vec4 const src);

/*
** vector_product.c
*/
t_vec3			vec3_mult(t_vec3 vec, double mult);
t_vec4			vec4_mult(t_vec4 vec, double mult);
double			vec3_scalar(t_vec3 vec_a, t_vec3 vec_b);
t_vec3			vec3_product(t_vec3 const vec_a, t_vec3 const vec_b);
t_vec4			vec4_product(t_vec4 const vec_a, t_vec4 const vec_b);

# define DOT	vec3_scalar
# define CROSS	vec3_product

/*
** vector_divide.c
*/
t_vec3			vec3_div(t_vec3 vec, double div);
t_vec4			vec4_div(t_vec4 vec, double div);

/*
** vector_sum.c
*/
t_vec3			vec3_sum(t_vec3 vec_a, t_vec3 vec_b);
t_vec4			vec4_sum(t_vec4 vec_a, t_vec4 vec_b);
t_vec3			vec3_add(t_vec3 vec, double add);
t_vec4			vec4_add(t_vec4 vec, double add);

/*
** vector_sub.c
*/
t_vec3			vec3_sub(t_vec3 vec_a, t_vec3 vec_b);
t_vec4			vec4_sub(t_vec4 vec_a, t_vec4 vec_b);
t_vec3			vec3_abd(t_vec3 vec, double abd);
t_vec4			vec4_abd(t_vec4 vec, double abd);

/*
** vec_magn_norm.c
*/
double			vec3_magnitude(t_vec3 vec);
double			vec4_magnitude(t_vec4 vec);
double			vec3_square_length(t_vec3 vec);
t_vec3			vec3_normalize(t_vec3 vec);
t_vec4			vec4_normalize(t_vec4 vec);

/*
** mat_vec.c
*/
void			init_vec3(t_vec3 *vec);
void			init_vec4(t_vec4 *vec);
void			mat_vec3_mult(t_matrix const mat, t_vec3 const vec,
					t_vec3 *result);
void			mat_vec4_mult(t_matrix const mat, t_vec4 const vec,
					t_vec4 *result);
void			matrice_mult(t_matrix const mat_a, t_matrix const mat_b,
					t_matrix result);

/*
** transform.c
*/
void			rotation(t_vec4 rotate, t_vec4 *vec);
void			translation(t_vec4 translate, t_vec4 *vec);
void			scaling(t_vec4 scaling, t_vec4 *vec);
void			to_isometric(t_vec4 *vec);

/*
** pixel.c
*/
void			put_rgba_pixel(t_image *img, t_vec2i coord, unsigned int color);
void			put_argb_pixel(t_image *img, t_vec2i coord, unsigned int color);
void			put_bgra_pixel(t_image *img, t_vec2i coord, unsigned int color);
void			put_abgr_pixel(t_image *img, t_vec2i coord, unsigned int color);
void			clear_image(t_image *img);

/*
** color.c
*/
t_v3color		v3color_normalize(t_v3color color);
t_v3color		v3color_product(t_v3color c0, t_v3color c1);
t_v3color		v3color_mult(t_v3color color, double mult);
t_v3color		v3color_div(t_v3color color, double div);
t_v3color		v3color_sum(t_v3color c0, t_v3color c1);
t_v3color		v3color_sub(t_v3color c0, t_v3color c1);
t_v3color		v3color_extract(t_v3color color);
t_v3color		v3color_add(t_v3color color, double add);
void			put_v3color_pixel(t_image *img, t_vec2i coord, t_v3color color);

/*
** complex numbers
*/
typedef struct	s_complex
{
	double	re;
	double	im;
}				t_complex;

void			complex_add(t_complex *z1, t_complex *z2, t_complex *ret);
void			complex_mult(t_complex *z1, t_complex *z2, t_complex *ret);
void			complex_div(t_complex *z1, t_complex *z2, t_complex *ret);

#endif
