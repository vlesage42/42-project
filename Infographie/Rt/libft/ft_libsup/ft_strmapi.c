/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/06 15:10:16 by abartz            #+#    #+#             */
/*   Updated: 2014/11/06 22:41:37 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Applique la fonction f a chaque caractere de la chaine de caracteres
** passee en parametre, en precisant son index, pour creer une nouvelle
** chaine "fraiche" resultant des applications successives de f.
*/

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	char			*str;
	unsigned int	i;

	str = NULL;
	if (s && (str = ft_strnew(ft_strlen(s))))
	{
		i = 0;
		while (*(s + i))
		{
			*(str + i) = f(i, *(s + i));
			i++;
		}
	}
	return (str);
}
