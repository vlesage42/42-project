/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/06 18:06:03 by abartz            #+#    #+#             */
/*   Updated: 2014/11/21 00:17:28 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Alloue et retourne une copie de la chaine passee en parametre,
** sans les espaces blancs au debut et a la fin de la chaine.
** On considere comme espaces blancs les caracteres ' ', '\n' et
** '\t'. Si s ne contient pas d'espaces blancs au debut ou a la fin,
** la fonction renvoie une copie de s. Si l'allocation echoue,
** la fonction renvoie NULL.
*/

static	int		s_isws(char c)
{
	int err;

	err = 0;
	if (c == ' ' || c == '\t' || c == '\n')
		err = 1;
	return (err);
}

static	int		s_wslen(char const *s)
{
	int len;

	len = 0;
	while (s_isws(*(s++)))
		len++;
	while (*s)
		s++;
	if (s_isws(*(s - 1)))
	{
		s--;
		while (s_isws(*(s--)))
			len++;
	}
	return (len);
}

static	int		s_wsend(char const *s)
{
	int err;

	err = 0;
	while (*s)
		s++;
	if (s_isws(*(s - 1)))
		err = 1;
	return (err);
}

static	void	s_processing(char const *s, char *str, int wslen)
{
	int		i;
	int		j;
	size_t	len;

	i = 0;
	j = 0;
	len = (ft_strlen(s) - wslen);
	while (s_isws(*(s)))
		s++;
	while (len--)
		*(str + j++) = *(s + i++);
}

char			*ft_strtrim(char const *s)
{
	char	*str;
	int		wslen;

	str = NULL;
	if (s)
	{
		wslen = s_wslen(s);
		if ((str = ft_strnew(ft_strlen(s) - wslen)))
		{
			if (!s_isws(*s) && !s_wsend(s))
				ft_strcpy(str, s);
			else
				s_processing(s, str, wslen);
		}
	}
	return (str);
}
