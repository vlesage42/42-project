/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 16:02:17 by abartz            #+#    #+#             */
/*   Updated: 2014/11/15 16:25:46 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Alloue et retourne une zone de memoire "fraiche". La memoire allouee
** est initialisee a 0. Si l'allocation echoue, la focntion renvoie NULL.
*/

void	*ft_memalloc(size_t size)
{
	void *mem;

	mem = NULL;
	if (size > 0)
		mem = malloc(size);
	if (mem)
		ft_bzero(mem, size);
	return (mem);
}
