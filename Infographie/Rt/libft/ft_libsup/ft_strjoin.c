/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/06 17:59:59 by abartz            #+#    #+#             */
/*   Updated: 2015/03/22 16:15:55 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Alloue et retourne une chaine de caractere "fraiche" terminee
** par un '\0' resultant de la concatention de s1 et s2.
** Si l'allocation echoue, la fonction retourne NULL.
*/

char	*ft_strjoin(char const *s1, char const *s2)
{
	char	*str;
	int		len_s1;
	int		len_s2;
	int		i;

	str = NULL;
	if (s1 && s2)
	{
		i = 0;
		len_s1 = ft_strlen(s1);
		len_s2 = ft_strlen(s2);
		if ((str = ft_strnew(len_s1 + len_s2)))
		{
			while (len_s1--)
				*(str + i++) = *(s1++);
			while (len_s2--)
				*(str + i++) = *(s2++);
		}
	}
	return (str);
}
