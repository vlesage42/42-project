/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnequ.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 17:03:57 by abartz            #+#    #+#             */
/*   Updated: 2014/11/06 17:43:14 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Compare lexicographiquement s1 et s2 jusqu'a n caracteres maximum.
** Si les deux chaines sont egales, la fonction retourne 1,
** ou 0 sinon.
*/

int		ft_strnequ(char const *s1, char const *s2, size_t n)
{
	int err;

	err = 0;
	if (s1 && s2)
	{
		if (ft_strncmp(s1, s2, n) == 0)
			err = 1;
	}
	return (err);
}
