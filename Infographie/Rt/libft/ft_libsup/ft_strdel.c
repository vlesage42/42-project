/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 17:01:52 by abartz            #+#    #+#             */
/*   Updated: 2014/11/06 21:38:57 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Prend en parametre une chaine de caractere qui doit etre
** liberee qvec free() et son pointeur mis a NULL.
*/

void	ft_strdel(char **as)
{
	ft_memdel((void**)as);
}
