/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 10:25:23 by abartz            #+#    #+#             */
/*   Updated: 2014/11/20 23:51:07 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Alloue et retourne une chaine de caracteres "fraiche", terminees par un
** '\0', representant l'entier n passe en parametre. Les nombres negatifs
** sont geres. Si l'allocation echoue, la fonction renvoie NULL.
*/

static	void	s_processing(char *str, int n, size_t len)
{
	int		modulo;

	modulo = ft_pow(10, len);
	if (n < 0)
	{
		n *= -1;
		*(str++) = '-';
		modulo /= 10;
	}
	while (modulo > 1)
	{
		*(str++) = ((n % modulo) / (modulo / 10)) + '0';
		modulo /= 10;
	}
}

char			*ft_itoa(int n)
{
	char	*str;
	size_t	len;

	if (n == _INT_MIN_)
	{
		str = "-2147483648";
	}
	else
	{
		len = ft_nbrlen(n);
		if (n < 0)
			len++;
		str = ft_strnew(len);
		if (str != NULL)
		{
			if (n != 0)
				s_processing(str, n, len);
			else
				*str = '0';
		}
	}
	return (str);
}
