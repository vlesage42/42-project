/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 10:55:45 by abartz            #+#    #+#             */
/*   Updated: 2014/12/15 16:11:18 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Affiche l'entier n sur la sortie standard.
*/

void	ft_putnbr(int n)
{
	if (n >= _INT_MIN_)
	{
		if (n < 0 && n)
		{
			ft_putchar('-');
			n *= -1;
		}
		if (n >= 10)
		{
			ft_putnbr(n / 10);
			ft_putnbr(n % 10);
		}
		else
			ft_putchar('0' + n);
	}
	else
		ft_putstr("-2147483648");
}
