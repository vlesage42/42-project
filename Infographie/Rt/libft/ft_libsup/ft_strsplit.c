/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 17:05:17 by abartz            #+#    #+#             */
/*   Updated: 2014/12/13 16:50:50 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Alloue et retourne un tableau de chaines de caracteres "fraiche"
** (toutes terminees par un '\0', le tableau egalement donc) resultant
** de la decoupe de s selon le caractere c. Si l'allocation echoue,
** la fonction renvoie NULL.
*/

static char		*st_getslice(int n, const char *s, char c)
{
	int		i;
	char	*slice;

	i = 0;
	slice = NULL;
	while (!slice && s && *s && i < n)
	{
		while (s && *s && *s == c)
			++s;
		++i;
		if (s && *s && i == n)
			slice = (char*)s;
		while (!slice && s && *s && *s != c)
			++s;
	}
	return (slice);
}

static int		st_strclen(const char *s, char c)
{
	int	len;

	len = 0;
	while (s && *s && *s != c)
	{
		++s;
		++len;
	}
	return (len);
}

static int		st_slicenbr(char const *s, char c)
{
	int		nbr;

	nbr = 0;
	while (s && *s)
	{
		if (*s == c && *(s + 1) != c && *(s + 1))
			++nbr;
		++s;
	}
	++nbr;
	return (nbr);
}

char			**ft_strsplit(char const *s, char c)
{
	int		i;
	int		len;
	char	**tab;
	int		slices;
	char	*slice;

	i = 0;
	tab = NULL;
	if (s)
	{
		slices = st_slicenbr(s, c);
		tab = (char**)malloc(sizeof(char*) * (slices + 1));
		if (tab)
		{
			while (i < slices)
			{
				slice = st_getslice(i + 1, s, c);
				len = st_strclen(slice, c);
				tab[i] = ft_strndup(slice, len);
				++i;
			}
			tab[i] = NULL;
		}
	}
	return (tab);
}
