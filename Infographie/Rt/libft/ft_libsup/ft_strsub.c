/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/06 17:44:42 by abartz            #+#    #+#             */
/*   Updated: 2014/11/15 19:03:19 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Alloue et retourne une copie "fraiche" d'un troncon de la chaine
** de caractere passee en parametre. Le troncon commence a l'index
** start et a pour longueur len. Si start et len ne designent pas
** une troncon valide, le comportement est indetermine. Si lallocation
** echoue, la fonction renvoie NULL.
*/

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char	*str;
	int		i;

	str = NULL;
	if (s && (str = ft_strnew(len)))
	{
		i = 0;
		while (len--)
			*(str + (i++)) = *(s + (start++));
	}
	return (str);
}
