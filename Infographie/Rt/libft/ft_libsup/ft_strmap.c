/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 17:03:10 by abartz            #+#    #+#             */
/*   Updated: 2014/11/06 22:40:01 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Applique la fonction f a chaque caractere de la chaine de caracteres
** passee en parametre pour creer une nouvelle chaine "fraiche" resultant
** des applications successives de f.
*/

char	*ft_strmap(char const *s, char (*f)(char))
{
	int		i;
	char	*str;

	str = NULL;
	if (s && (str = ft_strnew(ft_strlen(s))))
	{
		i = 0;
		while (*(s + i))
		{
			*(str + i) = f(*(s + i));
			i++;
		}
	}
	return (str);
}
