/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strclr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/06 14:41:03 by abartz            #+#    #+#             */
/*   Updated: 2014/11/06 14:41:05 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Assigne la valeur '\0' a tous les caracteres de la chaine
** pasee en parametre.
*/

void	ft_strclr(char *s)
{
	if (s != NULL)
	{
		while (*s)
			*(s++) = '\0';
		*s = '\0';
	}
}
