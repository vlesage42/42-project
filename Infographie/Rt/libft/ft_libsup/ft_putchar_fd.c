/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putchar_fd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 09:58:21 by abartz            #+#    #+#             */
/*   Updated: 2014/11/21 00:10:34 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Ecrit le charactere c sur le descripteur de fichier fd.
*/

void	ft_putchar_fd(char c, int fd)
{
	WR(fd, &c, 1);
}
