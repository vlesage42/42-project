/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_addlink.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/05 17:30:18 by abartz            #+#    #+#             */
/*   Updated: 2014/12/05 17:30:20 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int			ft_addlink(t_link *new_link, t_link **prior, t_link **next)
{
	int err;

	err = 0;
	if (new_link && *prior && *next)
	{
		if ((*prior)->next == (*next) && (*next)->prior == (*prior))
		{
			new_link->prior = (*prior);
			new_link->next = (*next);
			(*prior)->next = new_link;
			(*next)->prior = new_link;
			err = 1;
		}
	}
	return (err);
}
