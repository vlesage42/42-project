/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_newlink.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/05 17:23:50 by abartz            #+#    #+#             */
/*   Updated: 2015/03/22 15:08:20 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_link		*ft_newlink(int id, void const *child)
{
	t_link	*link;

	if ((link = (t_link*)malloc(sizeof(t_link))))
	{
		link->id = id;
		link->prior = NULL;
		link->next = NULL;
		link->child = (void*)child;
	}
	return (link);
}
