/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getlink_smallest_id.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/30 13:50:02 by abartz            #+#    #+#             */
/*   Updated: 2015/03/30 15:54:57 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_link		*ft_getlink_smallest_id(t_link *lst)
{
	t_link	*min;
	int		i;

	if ((lst = ft_getlink_first(lst)))
		i = lst->id;
	min = NULL;
	while (lst)
	{
		if (lst->id <= i)
		{
			i = lst->id;
			min = lst;
		}
		lst = lst->next;
	}
	return (min);
}
