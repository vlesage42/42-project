/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_addlink_tail.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/05 17:30:34 by abartz            #+#    #+#             */
/*   Updated: 2014/12/05 17:30:36 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_addlink_tail(t_link **alink, t_link *new_link)
{
	if (alink && *alink && new_link)
	{
		new_link->next = NULL;
		new_link->prior = ft_getlink_last(*alink);
		(new_link->prior)->next = new_link;
	}
}
