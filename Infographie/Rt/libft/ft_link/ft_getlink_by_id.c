/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getlink_by_id.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/05 17:30:56 by abartz            #+#    #+#             */
/*   Updated: 2014/12/05 17:30:57 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_link		*ft_getlink_by_id(t_link *link, int id)
{
	t_link	*ptr;
	int		flag;

	ptr = NULL;
	if (link)
	{
		link = ft_getlink_first(link);
		flag = 1;
		while (link && flag)
		{
			if (link->id == id)
			{
				ptr = link;
				flag = 0;
			}
			else
				link = link->next;
		}
	}
	return (ptr);
}
