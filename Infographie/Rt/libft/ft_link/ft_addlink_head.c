/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_addlink_head.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/05 17:30:26 by abartz            #+#    #+#             */
/*   Updated: 2014/12/05 17:30:28 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_addlink_head(t_link **alink, t_link *new_link)
{
	if (alink && *alink && new_link)
	{
		new_link->next = ft_getlink_first(*alink);
		new_link->prior = NULL;
		(new_link->next)->prior = new_link;
	}
}
