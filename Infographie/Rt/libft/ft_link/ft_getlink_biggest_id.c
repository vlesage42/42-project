/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getlink_biggest_id.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/30 13:49:55 by abartz            #+#    #+#             */
/*   Updated: 2015/03/30 15:56:13 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_link		*ft_getlink_biggest_id(t_link *lst)
{
	t_link	*max;
	int		i;

	if ((lst = ft_getlink_first(lst)))
		i = lst->id;
	max = NULL;
	while (lst)
	{
		if (lst->id >= i)
		{
			i = lst->id;
			max = lst;
		}
		lst = lst->next;
	}
	return (max);
}
