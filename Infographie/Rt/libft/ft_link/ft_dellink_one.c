/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dellink_one.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/05 17:30:47 by abartz            #+#    #+#             */
/*   Updated: 2015/05/18 15:36:09 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_dellink_one(t_link **alink)
{
	t_link	*ptr;

	if (alink && *alink)
	{
		ptr = ft_breaklink(alink);
		ft_memdel((void**)(&(ptr->child)));
		ft_memdel((void**)(&ptr));
	}
}
