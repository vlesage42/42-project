/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_breaklink.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/26 10:42:36 by abartz            #+#    #+#             */
/*   Updated: 2015/02/26 12:06:40 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Met les liens d'un maillon a NULL et relie son prior et son next.
** Le prior du maillon affecte, si il est non NULL, est retourne via le
** parametre alink (si prior est NULL, next est affecte a alink).
** Le retour de la fonction est le maillon affecte.
*/

t_link		*ft_breaklink(t_link **alink)
{
	t_link *link;
	t_link *prior;
	t_link *next;

	link = NULL;
	if (alink && *alink)
	{
		link = *alink;
		prior = (*alink)->prior;
		next = (*alink)->next;
		if (prior)
			prior->next = next;
		if (next)
			next->prior = prior;
		(*alink)->prior = NULL;
		(*alink)->next = NULL;
		(*alink) = (prior) ? prior : next;
	}
	return (link);
}
