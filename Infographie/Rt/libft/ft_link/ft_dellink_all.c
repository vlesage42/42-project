/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dellink_all.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/05 17:30:41 by abartz            #+#    #+#             */
/*   Updated: 2015/02/16 11:50:17 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_dellink_all(t_link **alink)
{
	t_link	*ptr;

	if (alink && *alink)
	{
		ptr = ft_getlink_first(*alink);
		while (ptr)
			ft_dellink_one(&ptr);
		*alink = NULL;
	}
}
