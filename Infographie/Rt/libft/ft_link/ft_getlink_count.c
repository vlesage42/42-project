/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getlink_count.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/15 14:58:46 by abartz            #+#    #+#             */
/*   Updated: 2015/03/27 14:49:19 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_getlink_count(t_link *link)
{
	size_t count;

	count = 0;
	link = ft_getlink_first(link);
	while (link)
	{
		link = link->next;
		count++;
	}
	return (count);
}
