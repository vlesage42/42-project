/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pow.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/20 23:59:25 by abartz            #+#    #+#             */
/*   Updated: 2014/11/21 00:00:05 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

/*
** Calcule une puissance.
*/

int		ft_pow(int n, int power)
{
	int i;

	i = n;
	while (--power)
	{
		n *= i;
	}
	return (n);
}
