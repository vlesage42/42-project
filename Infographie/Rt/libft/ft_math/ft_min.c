/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_min.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/20 23:59:25 by abartz            #+#    #+#             */
/*   Updated: 2014/11/21 00:00:05 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_min(int i, int j)
{
	return ((i < j) ? i : j);
}

float	ft_minf(float i, float j)
{
	return ((i < j) ? i : j);
}

double	ft_min2f(double i, double j)
{
	return ((i < j) ? i : j);
}
