/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_clamp.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/21 13:34:28 by abartz            #+#    #+#             */
/*   Updated: 2015/11/21 13:34:30 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_math.h"

/*
** If v compares less than hi, returns the larger of v and lo,
** otherwise returns hi.
*/

int		ft_clamp(int v, int lo, int hi)
{
	return ((v < hi) ? ft_max(v, lo) : hi);
}

float	ft_clampf(float v, float lo, float hi)
{
	return ((v < hi) ? ft_maxf(v, lo) : hi);
}

double	ft_clamp2f(double v, double lo, double hi)
{
	return ((v < hi) ? ft_max2f(v, lo) : hi);
}
