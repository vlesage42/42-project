/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_trigo.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/20 23:59:25 by abartz            #+#    #+#             */
/*   Updated: 2014/11/21 00:00:05 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <math.h>
#include "ft_math.h"

double	ft_cos(double degr)
{
	return ((degr == 90. || degr == 270.) ? 0. : cos(degr * (M_PI / 180.)));
}

double	ft_sin(double degr)
{
	return ((degr == 180.) ? 0. : sin(degr * (M_PI / 180.)));
}
