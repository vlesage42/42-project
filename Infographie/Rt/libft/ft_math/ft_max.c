/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_max.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/20 23:59:25 by abartz            #+#    #+#             */
/*   Updated: 2014/11/21 00:00:05 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_max(int i, int j)
{
	return ((i > j) ? i : j);
}

float	ft_maxf(float i, float j)
{
	return ((i > j) ? i : j);
}

double	ft_max2f(double i, double j)
{
	return ((i > j) ? i : j);
}
