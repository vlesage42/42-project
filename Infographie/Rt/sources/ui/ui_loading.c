/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ui_loading.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/04/13 16:42:42 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

void	ui_loading(t_env *env)
{
	if (!ft_strcmp(env->ui.path, "./home"))
		rt_ui_home(&(env->pmlx.interface), &(env->ui.home), env->ui.path);
	else if (!ft_strncmp(env->ui.path, "./home/multi", 12))
		rt_ui_multi(&(env->pmlx.interface), &(env->ui.multi), env->ui.path);
	else if (!ft_strcmp(env->ui.path, "./home/RT"))
		rt_ui_render(env, &(env->ui.render));
	mlx_put_image_to_window(
		env->pmlx.handler, env->pmlx.window.ptr, env->pmlx.interface.ptr, 0, 0);
	glFlush();
}

void	st_ext_loading(t_env *env, t_vec2i coord, const char *msg)
{
	const t_v3color	clr = rt_init_v3color(0xff, 0xff, 0xff);
	const t_text	text = rt_init_text(coord, clr, msg, 64);

	clear_image(&env->pmlx.interface);
	glClear(GL_COLOR_BUFFER_BIT);
	put_string_to_image(&env->pmlx.interface, text);
	mlx_put_image_to_window(env->pmlx.handler,
							env->pmlx.window.ptr,
							env->pmlx.interface.ptr, 0, 0);
	glFlush();
}
