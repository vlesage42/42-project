/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ui_init.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/12/01 03:27:11 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static void	st_init_home(t_home *home, t_window win, t_config *cfg)
{
	char			*path;
	const t_text	text0 = {{ 35, 15 }, { 0xff, 0xff, 0xff }, "Solo", 64};
	const t_text	text1 = {{ 35, 15 }, { 0xff, 0xff, 0xff }, "Multi", 64};
	const t_text	text2 = {{ 35, 15 }, { 0xff, 0xff, 0xff }, "Options", 64};

	path = ft_strjoin(cfg->images_dir, "ui/large_button.png");
	set_button(&(home->btn_solo), path, text0);
	home->btn_solo.coord.x = (int)(0.365 * win.width);
	home->btn_solo.coord.y = (int)(0.26 * win.height);
	set_button(&(home->btn_multi), path, text1);
	home->btn_multi.coord.x = (int)(0.365 * win.width);
	home->btn_multi.coord.y = (int)(0.442 * win.height);
	set_button(&(home->btn_options), path, text2);
	home->btn_options.coord.x = (int)(0.365 * win.width);
	home->btn_options.coord.y = (int)(0.627 * win.height);
	ft_strdel(&path);
}

static void	st_ext_init_multi(t_config *cfg,
	t_multi *multi, const double w, const double h)
{
	char			*path;
	const t_text	text0 = {{12, 12}, {0xff, 0xff, 0xff}, "Connection", 27};
	const t_text	text1 = {{17, 8}, {0xff, 0xff, 0xff}, "Apply", 30};
	const t_text	text2 = {{10, 12}, {0xff, 0xff, 0xff}, "Launch", 27};

	path = ft_strjoin(cfg->images_dir, "ui/field_l.png");
	load_png(&(multi->client[0].img), path);
	multi->client[0].coord = rt_init_coord((int)(0.2286 * w), (int)(0.57 * h));
	load_png(&(multi->client[1].img), path);
	multi->client[1].coord = rt_init_coord((int)(0.2286 * w), (int)(0.72 * h));
	ft_strdel(&path);
	path = ft_strjoin(cfg->images_dir, "ui/ok_button.png");
	set_button(&(multi->client[2]), path, text0);
	multi->client[2].coord = rt_init_coord((int)(0.2286 * w), (int)(0.82 * h));
	ft_strdel(&path);
	path = ft_strjoin(cfg->images_dir, "ui/field.png");
	load_png(&(multi->server[0].img), path);
	multi->server[0].coord = rt_init_coord((int)(0.62 * w), (int)(0.57 * h));
	ft_strdel(&path);
	path = ft_strjoin(cfg->images_dir, "ui/num_button_64_l.png");
	set_button(&(multi->server[1]), path, text1);
	multi->server[1].coord = rt_init_coord((int)(0.7 * w), (int)(0.57 * h));
	set_button(&(multi->server[2]), path, text2);
	multi->server[2].coord = rt_init_coord((int)(0.62 * w), (int)(0.82 * h));
	ft_strdel(&path);
}

static void	st_init_multi(t_multi *mlt, t_window win, t_config *cfg)
{
	const double	w = win.width;
	const double	h = win.height;
	const t_text	text0 = {{ 35, 15 }, { 0xff, 0xff, 0xff }, "Back", 64};
	const t_text	text1 = {{ 105, 15 }, { 0xff, 0xff, 0xff }, "X", 64};
	char			*path;

	path = ft_strjoin(cfg->images_dir, "ui/button.png");
	set_button(&(mlt->back[0]), path, text0);
	mlt->back[0].coord = rt_init_coord((int)(0.026 * w), (int)(0.0423 * h));
	set_button(&(mlt->back[1]), path, text1);
	mlt->back[1].coord = rt_init_coord((int)(0.026 * w), (int)(0.0423 * h));
	ft_strdel(&path);
	path = ft_strjoin(cfg->images_dir, "ui/radio_on.png");
	load_png(&(mlt->radio[0].img), path);
	mlt->radio[0].coord = rt_init_coord((int)(0.2 * w), (int)(0.22 * h));
	ft_strdel(&path);
	path = ft_strjoin(cfg->images_dir, "ui/radio_off.png");
	load_png(&(mlt->radio[1].img), path);
	mlt->radio[1].coord = rt_init_coord((int)(0.2 * w), (int)(0.375 * h));
	load_png(&(mlt->radio[2].img), path);
	mlt->radio[2].coord = rt_init_coord((int)(0.606 * w), (int)(0.375 * h));
	rt_init_keypad(cfg, mlt, rt_init_coord((int)(0.026 * w), (int)(0.344 * h)));
	ft_strdel(&path);
	st_ext_init_multi(cfg, mlt, w, h);
}

static void	st_init_render(t_render *render, double height, t_config *cfg)
{
	const t_text	t1 = {{12, 5}, {0xff, 0xff, 0xff}, "Help (H to close)", 24};
	const t_text	t = {{12, 45}, {255, 255, 255}, "* X, Y, Z, A to move", 20};
	const t_text	t2 = {{12, 75}, {0xff, 0xff, 0xff}, "* F5 to refresh", 20};
	const t_text	t3 = {{12, 105}, {255, 255, 255}, "* S to save an img", 20};
	char			*path;

	path = ft_strjoin(cfg->images_dir, "ui/help.png");
	set_button(&(render->btn_help), path, t1);
	put_string_to_image(&(render->btn_help.img), t);
	put_string_to_image(&(render->btn_help.img), t2);
	put_string_to_image(&(render->btn_help.img), t3);
	render->btn_help.coord = rt_init_coord(10,
			(int)(height - render->btn_help.img.height) - 10);
	ft_strdel(&path);
}

void		rt_init_ui(t_ui *ui, t_window win, t_config *cfg)
{
	ui->path = ft_strdup("./home");
	st_init_home(&(ui->home), win, cfg);
	st_init_multi(&(ui->multi), win, cfg);
	st_init_render(&(ui->render), win.height, cfg);
}
