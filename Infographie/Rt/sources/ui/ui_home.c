/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ui_home.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/04/13 16:42:42 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static void	st_ext_solo(t_env *env)
{
	if (rt_parser(env))
	{
		ft_strdel(&(env->ui.path));
		env->ui.path = ft_strdup("./home/RT");
		rt_init_cam(env);
		rt_render(env);
		glClear(GL_COLOR_BUFFER_BIT);
		mlx_put_image_to_window(env->pmlx.handler, env->pmlx.window.ptr,
			env->pmlx.rendering.ptr, 0, 0);
		glFlush();
	}
	else
		ft_putendl("Parser KO");
}

static void	st_ext_multi(t_env *env)
{
	ft_strdel(&(env->ui.path));
	env->ui.path = ft_strdup("./home/multi");
	ui_loading(env);
}

void		rt_ui_home_keyup(t_env *env, int keycode)
{
	if (keycode == key_1)
	{
		st_ext_solo(env);
	}
	else if (keycode == key_2)
	{
		st_ext_multi(env);
	}
}

void		rt_ui_home_click(t_env *env, const t_vec2i coord)
{
	if (check_btn_click(env->ui.home.btn_solo, coord))
	{
		st_ext_solo(env);
	}
	else if (check_btn_click(env->ui.home.btn_multi, coord))
	{
		st_ext_multi(env);
	}
	else if (check_btn_click(env->ui.home.btn_options, coord))
	{
	}
}

void		rt_ui_home(t_image *screen, t_home *home, char *path)
{
	const t_vec2i	coord = {20, (int)(screen->height - 60)};
	const t_v3color	clr = {0xff, 0xff, 0xff};
	const t_text	text = {coord, clr, path, 42};

	clear_image(screen);
	put_image_to_image(screen, home->btn_solo.coord, home->btn_solo.img);
	put_image_to_image(screen, home->btn_multi.coord, home->btn_multi.img);
	put_image_to_image(screen, home->btn_options.coord, home->btn_options.img);
	put_string_to_image(screen, text);
	glClear(GL_COLOR_BUFFER_BIT);
}
