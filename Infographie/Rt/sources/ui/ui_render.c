/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rt_help.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/04/13 16:42:42 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

void	rt_ui_render(t_env *env, t_render *render)
{
	static t_bool	flag = _false;

	clear_image(&(env->pmlx.interface));
	if (flag == _false)
	{
		flag = _true;
		put_image_to_image(&(env->pmlx.interface),
			render->btn_help.coord, render->btn_help.img);
	}
	else if (flag == _true)
	{
		flag = _false;
		glClear(GL_COLOR_BUFFER_BIT);
		mlx_put_image_to_window(
			env->pmlx.handler, env->pmlx.window.ptr,
			env->pmlx.rendering.ptr, 0, 0);
		glFlush();
	}
}
