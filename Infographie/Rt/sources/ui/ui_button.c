/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ui_button.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/04/13 16:42:42 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

int			check_btn_click(t_button btn, t_vec2i coord)
{
	return ((coord.x >= btn.coord.x && coord.x <= btn.coord.x + btn.img.width &&
			coord.y >= btn.coord.y && coord.y <= btn.coord.y + btn.img.height) ?
			1 : 0);
}

void		set_button(t_button *btn, const char *file, t_text txt)
{
	btn->img.data = NULL;
	load_png(&(btn->img), file);
	put_string_to_image(&(btn->img), txt);
}

static void	st_ext_switch(t_button *btn0, t_button *btn1, t_uint8 *err)
{
	unsigned char	*tmp;

	tmp = btn0->img.data;
	btn0->img.data = btn1->img.data;
	btn1->img.data = tmp;
	*err = 1;
}

t_uint8		check_radio_btn(t_button *radio, t_vec2i coord, char **path)
{
	static t_uint8	flag = 0;
	t_uint8			err;

	if (check_btn_click(radio[0], coord) && flag != 0)
	{
		st_ext_switch(&radio[0], &radio[flag], &err);
		ft_strdel(path);
		*path = ft_strdup("./home/multi");
		flag = 0;
	}
	else if (check_btn_click(radio[1], coord) && flag != 1)
	{
		st_ext_switch(&radio[1], &radio[flag], &err);
		ft_strdel(path);
		*path = ft_strdup("./home/multi/client");
		flag = 1;
	}
	else if ((check_btn_click(radio[2], coord) && flag != 2) || (err = 0))
	{
		st_ext_switch(&radio[2], &radio[flag], &err);
		ft_strdel(path);
		*path = ft_strdup("./home/multi/server");
		flag = 2;
	}
	return (err);
}
