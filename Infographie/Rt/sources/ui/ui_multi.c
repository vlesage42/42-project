/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ui_multi.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/12/01 03:22:16 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

void		rt_ui_multi_click(t_env *env, const t_vec2i coord)
{
	if (check_btn_click(env->ui.multi.back[0], coord) &&
		!ft_strcmp(env->ui.path, "./home/multi"))
	{
		ft_strdel(&(env->ui.path));
		env->ui.path = ft_strdup("./home");
		ui_loading(env);
	}
	else if (check_radio_btn(env->ui.multi.radio, coord, &(env->ui.path)))
		ui_loading(env);
	else if (!ft_strcmp(env->ui.path, "./home/multi/client"))
	{
		if (check_btn_click(env->ui.multi.client[0], coord))
			ft_putendl("IP Address");
		else if (check_btn_click(env->ui.multi.client[1], coord))
			ft_putendl("Port");
	}
	else if (!ft_strcmp(env->ui.path, "./home/multi/server"))
	{
		if (check_btn_click(env->ui.multi.server[0], coord))
			ft_putendl("Client count");
	}
}

static void	st_ext_client(t_image *screen, t_multi *m)
{
	const t_vec2i	coord0 = {m->client[0].coord.x, m->client[0].coord.y - 42};
	const t_vec2i	coord1 = {m->client[1].coord.x, m->client[1].coord.y - 42};
	const t_v3color	clr = {0xff, 0xff, 0xff};
	const t_text	text0 = {coord0, clr, "IP Address :", 30};
	const t_text	text1 = {coord1, clr, "Port :", 30};

	put_string_to_image(screen, text0);
	put_image_to_image(screen, m->client[0].coord, m->client[0].img);
	put_string_to_image(screen, text1);
	put_image_to_image(screen, m->client[1].coord, m->client[1].img);
	put_image_to_image(screen, m->client[2].coord, m->client[2].img);
}

static void	st_ext_server(t_image *screen, t_multi *m)
{
	const t_vec2i	coord = {m->server[0].coord.x, m->server[0].coord.y - 42};
	const t_v3color	clr = {0xff, 0xff, 0xff};
	const t_text	text = {coord, clr, "Client count (1 ~ 3):", 30};

	put_string_to_image(screen, text);
	put_image_to_image(screen, m->server[0].coord, m->server[0].img);
	put_image_to_image(screen, m->server[1].coord, m->server[1].img);
	put_image_to_image(screen, m->server[2].coord, m->server[2].img);
}

static void	st_ext_radio(t_image *screen, t_multi *m, const t_v3color clr)
{
	t_vec2i		coord[3];
	t_text		text[3];

	coord[0].x = m->radio[0].coord.x + 125;
	coord[0].y = m->radio[0].coord.y + 10;
	coord[1].x = m->radio[1].coord.x + 125;
	coord[1].y = m->radio[1].coord.y + 10;
	coord[2].x = m->radio[2].coord.x + 125;
	coord[2].y = m->radio[2].coord.y + 10;
	text[0].coord = coord[0];
	text[0].color = clr;
	text[0].str = "Off";
	text[0].police_size = 50;
	text[1].coord = coord[1];
	text[1].color = clr;
	text[1].str = "Client";
	text[1].police_size = 50;
	text[2].coord = coord[2];
	text[2].color = clr;
	text[2].str = "Server";
	text[2].police_size = 50;
	put_string_to_image(screen, text[0]);
	put_string_to_image(screen, text[1]);
	put_string_to_image(screen, text[2]);
}

void		rt_ui_multi(t_image *screen, t_multi *multi, char *path)
{
	const t_vec2i	coord = {20, (int)(screen->height - 60)};
	const t_v3color	clr = {0xff, 0xff, 0xff};
	const t_text	text = {coord, clr, path, 42};

	clear_image(screen);
	if (!ft_strcmp(path, "./home/multi"))
		put_image_to_image(screen, multi->back[0].coord, multi->back[0].img);
	else
		put_image_to_image(screen, multi->back[1].coord, multi->back[1].img);
	put_image_to_image(screen, multi->radio[0].coord, multi->radio[0].img);
	put_image_to_image(screen, multi->radio[1].coord, multi->radio[1].img);
	put_image_to_image(screen, multi->radio[2].coord, multi->radio[2].img);
	st_ext_radio(screen, multi, clr);
	st_ext_client(screen, multi);
	st_ext_server(screen, multi);
	put_string_to_image(screen, text);
	rt_ui_keypad(screen, multi);
	glClear(GL_COLOR_BUFFER_BIT);
}
