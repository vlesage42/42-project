/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ui_keypad.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/04/13 16:42:42 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static void	st_ext_numbers(t_vec2i *c, t_config *cfg,
								t_multi *m, t_vec2i crd)
{
	int				i;
	const t_vec2i	coord = {15, -5};
	const char		*path = ft_strjoin(cfg->images_dir, "ui/num_button_64.png");
	const t_v3color	clr = {0xff, 0xff, 0xff};
	t_text			text;

	text.coord = coord;
	text.color = clr;
	text.str = ft_strdup("1");
	text.police_size = 50;
	i = -1;
	while (c->y < 3 && !(c->x = 0))
	{
		while (c->x < 3 && ((++i) || !i))
		{
			set_button(&(m->kp[i]), path, text);
			m->kp[i].coord.x = crd.x + c->x * (int)m->kp[i].img.width + c->x;
			m->kp[i].coord.y = crd.y + c->y * (int)m->kp[i].img.height + c->y;
			text.str[0]++;
			++(c->x);
		}
		++(c->y);
	}
	ft_strdel((char**)&text.str);
	ft_strdel((char**)&path);
}

static void	st_ext_zero(t_vec2i *c, t_config *cfg,
								t_multi *m, t_vec2i coord)
{
	const t_v3color	clr = {0xff, 0xff, 0xff};
	const t_text	text = {{ 15, -5 }, clr, "0", 50};
	char			*path;

	path = ft_strjoin(cfg->images_dir, "ui/num_button_64_l.png");
	set_button(&(m->kp[9]), path, text);
	m->kp[9].coord.x = coord.x;
	m->kp[9].coord.y = coord.y + c->y *
		(int)(m->kp[9].img.height) + c->y;
	ft_strdel(&path);
}

static void	st_ext_point(t_vec2i *c, t_config *cfg,
								t_multi *m, t_vec2i coord)
{
	const t_v3color	clr = {0xff, 0xff, 0xff};
	const t_text	text = {{ 15, -5 }, clr, ".", 50};
	char			*path;

	path = ft_strjoin(cfg->images_dir, "ui/num_button_64.png");
	set_button(&(m->kp[10]), path, text);
	m->kp[10].coord.x = coord.x +
		(int)(m->kp[9].img.width) + 1;
	m->kp[10].coord.y = coord.y + c->y *
		(int)(m->kp[10].img.height) + c->y;
	ft_strdel(&path);
}

void		rt_init_keypad(t_config *cfg, t_multi *m, t_vec2i coord)
{
	t_vec2i	c;

	c.x = 0;
	c.y = 0;
	st_ext_numbers(&c, cfg, m, coord);
	st_ext_zero(&c, cfg, m, coord);
	st_ext_point(&c, cfg, m, coord);
}

void		rt_ui_keypad(t_image *screen, t_multi *m)
{
	int	i;

	i = 0;
	while (i < 11)
	{
		put_image_to_image(screen, m->kp[i].coord, m->kp[i].img);
		++i;
	}
}
