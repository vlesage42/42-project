/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ui_init.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/04/13 16:42:42 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static void	st_del_home(t_home *home)
{
	if (home->btn_solo.img.data)
		free(home->btn_solo.img.data);
	if (home->btn_multi.img.data)
		free(home->btn_multi.img.data);
	if (home->btn_options.img.data)
		free(home->btn_options.img.data);
}

static int	st_del_multi_norm(int i, t_multi *multi)
{
	while (i < 3 || (i = 0))
	{
		if (multi->client[i].img.data)
			free(multi->client[i].img.data);
		++i;
	}
	while (i < 3 || (i = 0))
	{
		if (multi->server[i].img.data)
			free(multi->server[i].img.data);
		++i;
	}
	return (i);
}

static void	st_del_multi(t_multi *multi)
{
	int	i;

	i = 0;
	if (multi->back[0].img.data)
		free(multi->back[0].img.data);
	if (multi->back[1].img.data)
		free(multi->back[1].img.data);
	while (i < 3 || (i = 0))
	{
		if (multi->radio[i].img.data)
			free(multi->radio[i].img.data);
		++i;
	}
	while (i < 11 || (i = 0))
	{
		if (multi->kp[i].img.data)
			free(multi->kp[i].img.data);
		++i;
	}
	i = st_del_multi_norm(i, multi);
}

static void	st_del_render(t_render *render)
{
	if (render->btn_help.img.data)
		free(render->btn_help.img.data);
}

void		rt_delete_ui(t_ui *ui)
{
	ft_memdel((void**)&(ui->path));
	st_del_home(&(ui->home));
	st_del_multi(&(ui->multi));
	st_del_render(&(ui->render));
}
