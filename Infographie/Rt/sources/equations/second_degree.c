/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   second_degree.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/04/13 16:42:42 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

int			st_limitation(int mode, double height, double n)
{
	return (((mode == k_center && (n <= (height / 2) && n >= -(height / 2))) ||
			(mode == k_offset && (n <= height && n >= 0)) ||
			(mode == k_infinity)) ? 1 : 0);
}

static void	st_ext_equation(t_equa_2nd *equa, t_equa_shape *sh, t_object obj)
{
	if (equa->x[0] >= 0 && (equa->t = equa->x[0]) >= 0 &&
		st_limitation(obj.mode, obj.height,
			(sh->n[0] = sh->dv * equa->x[0] + sh->xv)))
		sh->m = sh->n[0];
	else if (equa->x[1] >= 0 && (equa->t = equa->x[1]) >= 0 &&
		st_limitation(obj.mode, obj.height,
			(sh->n[0] = sh->dv * equa->x[1] + sh->xv)))
		sh->m = sh->n[0];
	else
		sh->err = MISS;
}

void		st_equation_2nd(t_equa_2nd *equa, t_equa_shape *sh, t_object obj)
{
	t_vec2i	err;

	equa->x[0] = ((-equa->b) + equa->det) / (2 * equa->a);
	equa->x[1] = ((-equa->b) - equa->det) / (2 * equa->a);
	if ((err.x = 1) &&
		(err.y = 1) && equa->x[0] >= 0 && equa->x[1] >= 0)
	{
		sh->n[0] = sh->dv * equa->x[0] + sh->xv;
		sh->n[1] = sh->dv * equa->x[1] + sh->xv;
		if ((equa->x[0] < equa->t &&
			st_limitation(obj.mode, obj.height, sh->n[0])) || (err.x = 0))
		{
			sh->m = sh->n[0];
			equa->t = equa->x[0];
		}
		if ((equa->x[1] < equa->t &&
			st_limitation(obj.mode, obj.height, sh->n[1])) || (err.y = 0))
		{
			sh->m = sh->n[1];
			equa->t = equa->x[1];
		}
		(!(err.x) && !(err.y)) ? (sh->err = MISS) : 0;
	}
	else
		st_ext_equation(equa, sh, obj);
}
