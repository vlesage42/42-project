/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   blend.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/04/13 16:42:42 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static t_v3color	st_map_blend(t_info *info, t_texture *const tex,
						t_material *const mat)
{
	const double	k = 1 - tex->blend;

	return (v3color_sum(
		v3color_mult(mat->albedo, k),
		v3color_mult(info->object->uv_m(*info), tex->blend)));
}

static t_v3color	st_proc_blend(t_info *info, t_texture *const tex,
						t_material *const mat)
{
	const double	k = 1 - tex->blend;

	return (v3color_sum(
		v3color_mult(mat->albedo, k),
		v3color_mult(mat->albedo, info->object->proc(*info) * tex->blend)));
}

t_v3color			get_tex(t_info *info)
{
	t_v3color			c;
	t_texture *const	tex = (info->object->texture);
	t_material *const	mat = (info->object->material);

	if (info->object->texture)
	{
		if (tex->image && tex->image->data)
			c = st_map_blend(info, tex, mat);
		else if (info->object->proc)
			c = st_proc_blend(info, tex, mat);
		else
			c = mat->albedo;
	}
	else
		c = mat->albedo;
	return (c);
}
