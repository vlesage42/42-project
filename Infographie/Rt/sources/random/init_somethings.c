/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_somethings.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/04/13 16:42:42 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

t_text		rt_init_text(t_vec2i coord, t_v3color color, const char *msg,
						t_uint8 police_size)
{
	t_text	text;

	text.coord = coord;
	text.color = color;
	text.str = (char*)msg;
	text.police_size = police_size;
	return (text);
}

t_vec2i		rt_init_coord(int x, int y)
{
	t_vec2i coord;

	coord.x = x;
	coord.y = y;
	return (coord);
}
