/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   put_to_image.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/04/13 16:42:42 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static int	st_check_string_pixel(t_uint32 *p)
{
	return ((!((t_uint8*)p)[0] && !((t_uint8*)p)[1] &&
		!((t_uint8*)p)[2] && !((t_uint8*)p)[3]) ? 0 : 1);
}

static void	st_ext_str_to_img(t_image *img, SDL_Surface *initial,
								SDL_Surface *intermediary, t_text text)
{
	int			x;
	int			y;
	t_uint32	c;
	t_vec2i		co;
	t_v3color	clr;

	SDL_BlitSurface(initial, 0, intermediary, 0);
	y = 0;
	while (y < intermediary->h)
	{
		x = 0;
		while (x < intermediary->w)
		{
			c = ((t_uint32*)(intermediary->pixels))[(y * intermediary->w) + x];
			clr.r = ((t_uint8*)&c)[0];
			clr.g = ((t_uint8*)&c)[1];
			clr.b = ((t_uint8*)&c)[2];
			co = rt_init_coord(text.coord.x + x, text.coord.y + y);
			if (st_check_string_pixel(&c))
				put_v3color_pixel(img, co, clr);
			++x;
		}
		++y;
	}
}

void		put_string_to_image(t_image *img, t_text text)
{
	TTF_Font		*font;
	SDL_Surface		*initial;
	SDL_Surface		*intermediary;
	SDL_Color		rgba;

	rgba.r = (t_uint8)text.color.b;
	rgba.g = (t_uint8)text.color.g;
	rgba.b = (t_uint8)text.color.r;
	rgba.a = 0xff;
	if (TTF_Init() == -1 || !(font = TTF_OpenFont(FONT_PATH, text.police_size)))
		ft_putendl(TTF_GetError());
	else
	{
		initial = TTF_RenderText_Blended(font, text.str, rgba);
		intermediary = SDL_CreateRGBSurface(0,
			(int)(round(pow(2, ceil(log(initial->w) / log(2))))),
			(int)(round(pow(2, ceil(log(initial->h) / log(2))))),
			32, RMASK, GMASK, BMASK, AMASK);
		st_ext_str_to_img(img, initial, intermediary, text);
		TTF_CloseFont(font);
		TTF_Quit();
		SDL_FreeSurface(intermediary);
		SDL_FreeSurface(initial);
	}
}

void		put_image_to_image(t_image *img0, t_vec2i coord, t_image img1)
{
	t_v3color	c;
	int			x;
	int			y;
	t_uint32	i;
	t_vec2i		co;

	y = 0;
	while (y < img1.height)
	{
		x = 0;
		while (x < img1.width)
		{
			i = y * ((int)(img1.width * img1.bpp)) + x * img1.bpp;
			c.r = (img1.data)[i];
			c.g = (img1.data)[i + 1];
			c.b = (img1.data)[i + 2];
			co = rt_init_coord(coord.x + x, coord.y + y);
			if (st_check_string_pixel(&i))
				put_v3color_pixel(img0, co, c);
			++x;
		}
		++y;
	}
}
