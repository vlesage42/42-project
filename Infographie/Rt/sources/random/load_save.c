/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   load_save.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/04/13 16:42:42 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static void	st_ext_load(t_image **png, SDL_Surface *surface)
{
	(*png)->ptr = NULL;
	(*png)->bpp = surface->format->BytesPerPixel;
	(*png)->size_line = surface->pitch;
	(*png)->width = surface->w;
	(*png)->height = surface->h;
}

void		load_png(t_image *png, const char *file)
{
	SDL_Surface		*surface;
	t_size			size;

	if (IMG_Init(IMG_INIT_PNG))
	{
		if ((surface = IMG_Load(file)))
		{
			st_ext_load(&png, surface);
			size = png->size_line * (int)surface->h;
			png->data = (t_uint8*)ft_memalloc(size);
			ft_memcpy(png->data, (t_uint8*)surface->pixels, size);
			SDL_FreeSurface(surface);
		}
		else
		{
			ft_putstr("IMG_Load error : ");
			ft_putendl(IMG_GetError());
		}
		IMG_Quit();
	}
	else
	{
		ft_putstr("IMG_Init error : ");
		ft_putendl(IMG_GetError());
	}
}

void		save_png(t_window *win, t_image *png, const char *file)
{
	SDL_Surface	*surface;
	t_size		size;

	surface = SDL_CreateRGBSurface(0,
		(int)win->width, (int)win->height, 32, RMASK, GMASK, BMASK, AMASK);
	size = png->size_line * (int)surface->h;
	ft_memcpy(surface->pixels, png->data, size);
	IMG_SavePNG(surface, file);
	SDL_FreeSurface(surface);
}
