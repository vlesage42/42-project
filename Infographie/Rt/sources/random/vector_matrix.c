/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   random.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/04/13 16:42:42 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

t_vec3		world_to_local(t_info info)
{
	t_vec3	s;
	t_vec3	t;

	s = vec3_sub(info.intersect, info.object->position);
	if (info.object->comp)
		mat_vec3_mult(info.object->comp->inv, s, &t);
	mat_vec3_mult(info.object->inverse, s, &t);
	check_vector(&t);
	return (t);
}

t_v3color	rt_init_v3color(double r, double g, double b)
{
	t_v3color	color;

	color.r = r;
	color.g = g;
	color.b = b;
	return (color);
}

t_vec3		rt_init_vec3(double x, double y, double z)
{
	t_vec3	vec;

	vec.x = x;
	vec.y = y;
	vec.z = z;
	vec.len = 0.;
	return (vec);
}

void		transpose(t_matrix mat, t_matrix transp)
{
	t_uint8		i;
	t_uint8		j;

	i = 0;
	while (i < 4)
	{
		j = 0;
		while (j < 4)
		{
			transp[i][j] = mat[j][i];
			++j;
		}
		++i;
	}
}
