/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_somethings.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/05 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/09/05 16:42:42 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static int	st_ext_check_vector(double xyz)
{
	return ((xyz > 0 && xyz < ZERO) || (xyz < 0 && xyz > -ZERO));
}

void		check_vector(t_vec3 *v)
{
	v->x = (st_ext_check_vector(v->x)) ? 0. : v->x;
	v->y = (st_ext_check_vector(v->y)) ? 0. : v->y;
	v->z = (st_ext_check_vector(v->z)) ? 0. : v->z;
}

int			check_planar_types(t_object *o)
{
	return ((o->type == k_plane || o->type == k_square ||
			o->type == k_triangle || o->type == k_disk));
}

void		check_data_index(const t_image *img, int *i)
{
	if ((*i >= (int)(img->width * img->bpp * img->height)))
		*i = 0;
}
