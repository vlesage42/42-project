/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   triangle.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/12/01 08:25:51 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static int	st_ext_triangle(t_info inf, t_object triangle, t_info *tmp, int err)
{
	const t_vec3 c[3] = {
		vec3_sub(inf.intersect, triangle.vertices.v0),
		vec3_sub(inf.intersect, triangle.vertices.v1),
		vec3_sub(inf.intersect, triangle.vertices.v2)
	};

	if ((DOT(triangle.normal, CROSS(triangle.edges[0], c[0])) > ZERO &&
		DOT(triangle.normal, CROSS(triangle.edges[1], c[1])) > ZERO &&
		DOT(triangle.normal, CROSS(triangle.edges[2], c[2])) > ZERO))
	{
		err = HIT;
		tmp->intersect = inf.intersect;
		tmp->normal = vec3_normalize(triangle.normal);
	}
	return (err);
}

int			rt_triangle_intersects(const t_ray ray, t_object triangle,
				t_info *tmp)
{
	t_info	inf;
	int		err;

	err = MISS;
	if (rt_plane_intersects(ray, triangle, &inf))
		err = st_ext_triangle(inf, triangle, tmp, err);
	return (err);
}

/*
** t_vec3	c[3];
** c[0] = vec3_sub(inf.intersect, square.vertices.v0);
** c[1] = vec3_sub(inf.intersect, square.vertices.v1);
** c[2] = vec3_sub(inf.intersect, square.vertices.v2);
*/
