/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   disk.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/04/13 16:42:42 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

int		rt_disk_intersects(const t_ray ray, t_object disk, t_info *tmp)
{
	t_info	inf;
	int		err;
	t_vec3	v;

	err = MISS;
	if (rt_plane_intersects(ray, disk, &inf))
	{
		v = vec3_sub(inf.intersect, disk.position);
		if (DOT(v, v) <= disk.radius * disk.radius)
		{
			err = HIT;
			tmp->intersect = inf.intersect;
			tmp->normal = inf.normal;
		}
	}
	return (err);
}
