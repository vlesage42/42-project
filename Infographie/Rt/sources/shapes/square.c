/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   square.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/12/01 08:13:29 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static int	st_ext_square(t_info inf, t_object square, t_info *tmp, int err)
{
	const t_vec3	c[4] = {
		vec3_sub(inf.intersect, square.vertices.v0),
		vec3_sub(inf.intersect, square.vertices.v1),
		vec3_sub(inf.intersect, square.vertices.v2),
		vec3_sub(inf.intersect, square.vertices.v3)
	};

	if ((DOT(square.normal, CROSS(square.edges[0], c[0])) > ZERO &&
		DOT(square.normal, CROSS(square.edges[1], c[1])) > ZERO &&
		DOT(square.normal, CROSS(square.edges[2], c[2])) > ZERO &&
		DOT(square.normal, CROSS(square.edges[3], c[3])) > ZERO))
	{
		err = HIT;
		tmp->intersect = inf.intersect;
		tmp->normal = square.normal;
	}
	return (err);
}

int			rt_square_intersects(const t_ray ray, t_object square, t_info *tmp)
{
	t_info			inf;
	int				err;

	err = MISS;
	if (rt_plane_intersects(ray, square, &inf))
		err = st_ext_square(inf, square, tmp, err);
	return (err);
}
