/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   plane.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/12/01 07:15:45 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

int		rt_plane_intersects(const t_ray ray, t_object plane, t_info *tmp)
{
	double			t;
	const double	dv = DOT(plane.normal, ray.dir);

	t = 0.;
	if (dv > ZERO || dv < -ZERO)
	{
		t = DOT(vec3_sub(plane.position, ray.origin), plane.normal) / dv;
		if (t > ZERO)
		{
			tmp->intersect = vec3_sum(ray.origin, vec3_mult(ray.dir, t));
			tmp->normal = (dv > ZERO) ? vec3_mult(plane.normal, -1) :
				plane.normal;
		}
	}
	return (t > ZERO);
}
