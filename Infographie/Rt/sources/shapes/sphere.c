/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sphere.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/12/01 08:50:49 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static int	st_ext_sphere(int err, const t_ray ray, t_object s, t_info *tmp)
{
	const t_vec3	vec = vec3_sub(ray.origin, s.position);
	double			b;
	double			det;
	t_vec2			t;

	b = DOT(vec, ray.dir);
	det = (b * b) - (vec3_square_length(vec) - (s.radius * s.radius));
	if (det >= 0 && (err = HIT))
	{
		if (det && (det = sqrt(det)))
		{
			t.x = (-b + det);
			t.y = (-b - det);
			(t.x < 0 || t.y < 0) ? (err = MISS) : 0;
			b = (t.x < t.y) ? t.x : t.y;
		}
		b = (det) ? b : -b;
		if (err == HIT || (err == MISS && s.material->type == k_negative))
		{
			tmp->intersect = vec3_sum(ray.origin, vec3_mult(ray.dir, b));
			tmp->normal = vec3_normalize(vec3_mult(vec3_sub(tmp->intersect,
				s.position), 1 / s.radius));
		}
	}
	return (err);
}

int			rt_sphere_intersects(const t_ray ray, t_object s, t_info *tmp)
{
	const t_vec3	vec = vec3_sub(ray.origin, s.position);
	const double	r2 = s.radius * s.radius;
	int				err;

	err = MISS;
	if (ray.type == k_primary && vec3_magnitude(vec) < s.radius &&
		s.material->type != k_negative)
	{
		err = HIT;
		tmp->intersect = ray.origin;
		while (vec3_square_length(vec3_sub(tmp->intersect, s.position)) <= r2)
			tmp->intersect = vec3_sum(tmp->intersect, vec3_mult(ray.dir, 0.01));
		tmp->normal = vec3_normalize(vec3_mult(vec3_mult(
			vec3_sub(tmp->intersect, s.position), 1 / s.radius), -1));
	}
	else
		err = st_ext_sphere(err, ray, s, tmp);
	return (err);
}
