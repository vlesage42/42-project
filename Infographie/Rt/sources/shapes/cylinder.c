/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cylinder.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/12/01 08:01:04 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static t_equa_2nd	st_ext_equa(const t_ray ray, t_object c, t_equa_shape sh)
{
	t_equa_2nd		equa;

	equa.t = 9999999999.;
	equa.a = DOT(ray.dir, ray.dir) - (sh.dv * sh.dv);
	equa.b = 2 * (DOT(ray.dir, sh.x) - (sh.dv * sh.xv));
	equa.c = DOT(sh.x, sh.x) - (sh.xv * sh.xv) - (c.radius * c.radius);
	equa.det = (equa.b * equa.b) - (4 * equa.a * equa.c);
	return (equa);
}

int					rt_cylinder_intersects(const t_ray ray, t_object c,
						t_info *tmp)
{
	t_equa_2nd		equa;
	t_equa_shape	sh;

	sh.err = MISS;
	sh.x = vec3_sub(ray.origin, c.position);
	sh.dv = DOT(ray.dir, c.axis);
	sh.xv = DOT(sh.x, c.axis);
	equa = st_ext_equa(ray, c, sh);
	if (equa.det >= 0 && (sh.err = HIT))
	{
		if (equa.det && (equa.det = sqrt(equa.det)))
			st_equation_2nd(&equa, &sh, c);
		else if ((equa.t = (-equa.b / (2 * equa.a))) >= 0 || equa.t < 0)
			sh.m = sh.dv * equa.t + sh.xv;
		else
			sh.err = MISS;
		if (sh.err == HIT)
		{
			tmp->intersect = vec3_sum(ray.origin, vec3_mult(ray.dir, equa.t));
			tmp->normal = vec3_normalize(vec3_sub(vec3_sum(sh.x,
				vec3_mult(ray.dir, equa.t)), vec3_mult(c.axis, sh.m)));
		}
	}
	return (sh.err);
}
