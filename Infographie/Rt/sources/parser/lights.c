/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lights.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/12/01 04:18:46 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static void	st_ext_light(t_light **l_ptr, t_pxml *xml, t_pxml_node *n_ptr)
{
	t_pxml_node	*c_ptr;

	if ((c_ptr = pxml_getchild(xml, n_ptr, "position")))
		(*l_ptr)->position = ft_get_vec3(c_ptr->content);
	if ((c_ptr = pxml_getchild(xml, n_ptr, "direction")))
		(*l_ptr)->direction = vec3_normalize(ft_get_vec3(c_ptr->content));
	if ((c_ptr = pxml_getchild(xml, n_ptr, "angle")))
		(*l_ptr)->angle = ft_clamp2f(ft_ato2f(c_ptr->content), 5., 90.);
	if ((c_ptr = pxml_getchild(xml, n_ptr, "intensity")))
		(*l_ptr)->intensity = ft_ato2f(c_ptr->content);
	if ((c_ptr = pxml_getchild(xml, n_ptr, "color")))
		(*l_ptr)->color = ft_get_v3color(c_ptr->content);
}

static void	st_new_light(t_light **l_ptr, t_pxml *xml, t_pxml_node *n_ptr)
{
	t_pxml_node	*c_ptr;

	c_ptr = NULL;
	*l_ptr = NULL;
	if ((*l_ptr = (t_light*)ft_memalloc(sizeof(t_light))))
	{
		(*l_ptr)->direction = rt_init_vec3(0., 0., 0.);
		(*l_ptr)->angle = 0.;
		(*l_ptr)->intensity = 100;
		if (!ft_strcmp(n_ptr->name, "point") && ((*l_ptr)->type = k_point) &&
			((*l_ptr)->illuminate = &rt_point) &&
			((*l_ptr)->get_pos = &get_point_pos))
			(*l_ptr)->get_dir = &get_point_dir;
		else if (!ft_strcmp(n_ptr->name, "spot") && ((*l_ptr)->type = k_spot) &&
			((*l_ptr)->illuminate = &rt_spot) &&
			((*l_ptr)->get_pos = &get_spot_pos))
			(*l_ptr)->get_dir = &get_spot_dir;
		else if (!ft_strcmp(n_ptr->name, "directional") &&
			((*l_ptr)->type = k_directional) &&
			((*l_ptr)->illuminate = &rt_directional) &&
			((*l_ptr)->get_pos = &get_directional_pos))
			(*l_ptr)->get_dir = &get_directional_dir;
		st_ext_light(l_ptr, xml, n_ptr);
	}
}

static void	st_rec_lights(t_light **l_ptr,
						t_pxml *xml, t_pxml_node *n_ptr)
{
	if ((n_ptr = (t_pxml_node*)pxml_next(xml, n_ptr)))
	{
		st_new_light(&((*l_ptr)->next), xml, n_ptr);
		st_rec_lights(&((*l_ptr)->next), xml, n_ptr);
	}
}

int			parser_lights(t_light **lights, t_pxml *xml, t_pxml_node *node)
{
	int			err;
	t_pxml_node	*n_ptr;

	err = 1;
	*lights = NULL;
	if ((n_ptr = (t_pxml_node*)pxml_children(xml, node)))
	{
		st_new_light(lights, xml, n_ptr);
		st_rec_lights(lights, xml, n_ptr);
	}
	return (err);
}
