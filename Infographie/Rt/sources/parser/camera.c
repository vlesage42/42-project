/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   camera.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/12/01 03:28:55 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

int			parser_camera(t_camera *cam, t_pxml *xml, t_pxml_node *node)
{
	int			err;
	t_pxml_node	*n_cam;

	err = 1;
	if ((n_cam = pxml_getchild(xml, node, "position")) ||
		(err = 0))
		cam->pos = ft_get_vec3(n_cam->content);
	if (err && ((n_cam = pxml_getchild(xml, node, "look_at")) ||
		(err = 0)))
		cam->look_at = ft_get_vec3(n_cam->content);
	return (err);
}
