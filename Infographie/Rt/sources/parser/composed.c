/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   composed.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/12/01 03:50:15 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static void	st_new_composed(t_engine *engine, t_object **o,
								t_pxml *xml, t_pxml_node *node)
{
	t_pxml_node	*child;
	t_object	*first;

	if ((child = (t_pxml_node*)pxml_children(xml, node)))
	{
		(*o) = rt_new_object(engine, xml, child);
		(*o)->comp = (t_composed*)ft_memalloc(sizeof(t_composed));
		(*o)->comp->pos = (*o)->position;
		first = (*o);
		while ((child = (t_pxml_node*)pxml_next(xml, child)))
		{
			(*o)->next = rt_new_object(engine, xml, child);
			(*o)->next->comp = (t_composed*)ft_memalloc(sizeof(t_composed));
			(*o)->next->comp->pos = (*o)->next->position;
			(*o) = (*o)->next;
		}
		(*o) = first;
	}
}

static void	st_get_composed(t_engine *engine, t_object **o, char *name)
{
	t_config const	*cfg = &(engine->config);
	t_pxml			*pxml;
	t_pxml_node		*root;
	char			*path;

	path = ft_strjoin(cfg->composed_dir, name);
	pxml = NULL;
	if (open_xml(&pxml, &root, path))
		st_new_composed(engine, o, pxml, root);
	if (pxml)
		pxml_close(&pxml);
	ft_strdel(&path);
}

static void	st_set_composed(t_object **o, t_engine *engine,
								t_pxml *xml, t_pxml_node *child)
{
	t_object	*box;
	t_pxml_node	*n;
	t_object	*tmp;

	box = (t_object*)ft_memalloc(sizeof(t_object));
	if ((n = pxml_getchild(xml, child, "position")))
		box->position = ft_get_vec3(n->content);
	get_mat_tex(&box, engine, xml, child);
	if ((n = pxml_getchild(xml, child, "rotation")))
		box->rotation = ft_get_vec3(n->content);
	tmp = (*o);
	while (*o)
	{
		(*o)->rotation = box->rotation;
		(*o)->position = box->position;
		n ? rotate_composed(o) : (void)n;
		(*o)->position = vec3_sum((*o)->comp->pos, box->position);
		if (!(*o)->texture)
			(*o)->texture = box->texture;
		if (!(*o)->material)
			(*o)->material = box->material;
		(*o) = (*o)->next;
	}
	(*o) = tmp;
	ft_memdel((void**)&box);
}

void		parser_conposed(t_object **o, t_engine *engine,
								t_pxml *xml, t_pxml_node *child)
{
	t_pxml_node	*n;
	char		*tmp;
	char		*name;

	if ((n = pxml_getchild(xml, child, "name")))
	{
		tmp = ft_strtrim(n->content);
		name = (!ft_strstr(tmp, ".xml")) ?
			ft_strjoin(tmp, ".xml") : ft_strdup(tmp);
		st_get_composed(engine, o, name);
		st_set_composed(o, engine, xml, child);
		ft_strdel(&name);
		ft_strdel(&tmp);
	}
}
