/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   materials.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/12/01 03:47:01 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static void	st_get_type(t_material **m_ptr,
							t_pxml *xml, t_pxml_node *n_ptr)
{
	char		*type;
	t_pxml_node	*c_ptr;

	c_ptr = NULL;
	if ((c_ptr = pxml_getchild(xml, n_ptr, "type")))
	{
		type = ft_strtrim(c_ptr->content);
		if (!ft_strcmp(type, "phong"))
			(*m_ptr)->type = k_phong;
		else if (!ft_strcmp(type, "reflection"))
			(*m_ptr)->type = k_reflection;
		else if (!ft_strcmp(type, "refraction"))
			(*m_ptr)->type = k_refraction;
		else if (!ft_strcmp(type, "negative"))
			(*m_ptr)->type = k_negative;
		ft_strdel(&type);
	}
}

static void	st_new_material(t_material **m_ptr,
							t_pxml *xml, t_pxml_node *n_ptr)
{
	t_pxml_node	*c_ptr;

	c_ptr = NULL;
	if ((*m_ptr = (t_material*)ft_memalloc(sizeof(t_material))))
	{
		(*m_ptr)->name = ft_strdup(n_ptr->name);
		st_get_type(m_ptr, xml, n_ptr);
		if ((c_ptr = pxml_getchild(xml, n_ptr, "albedo")))
			(*m_ptr)->albedo = ft_get_v3color(c_ptr->content);
		if ((c_ptr = pxml_getchild(xml, n_ptr, "transparency")))
			(*m_ptr)->transparency = ft_ato2f(c_ptr->content);
		if ((c_ptr = pxml_getchild(xml, n_ptr, "kdiffuse")))
			(*m_ptr)->kdiffuse = ft_ato2f(c_ptr->content);
		if ((c_ptr = pxml_getchild(xml, n_ptr, "kspecular")))
			(*m_ptr)->kspecular = ft_ato2f(c_ptr->content);
		if ((c_ptr = pxml_getchild(xml, n_ptr, "ks_exp")))
			(*m_ptr)->ks_exp = ft_ato2f(c_ptr->content);
		if ((c_ptr = pxml_getchild(xml, n_ptr, "kreflection")))
			(*m_ptr)->kreflection = ft_ato2f(c_ptr->content);
		if ((c_ptr = pxml_getchild(xml, n_ptr, "krefraction")))
			(*m_ptr)->krefraction = ft_ato2f(c_ptr->content);
	}
}

static void	st_get_material(t_config *cfg,
				t_object **o_ptr, t_material **mat, char *cnt)
{
	t_pxml		*xml;
	t_pxml_node	*node;
	char		*name;
	char		*path;

	name = NULL;
	if ((name = (!ft_strstr(cnt, ".xml")) ?
		ft_strjoin(cnt, ".xml") : ft_strdup(cnt)))
	{
		path = ft_strjoin(cfg->materials_dir, name);
		if ((xml = pxml_init(path)))
		{
			if ((node = (t_pxml_node*)pxml_root(xml)))
				st_new_material(mat, xml, node);
			else
				ft_putendl("material pxml_root fail");
			pxml_close(&xml);
		}
		else
			ft_putendl("material pxml_init fail");
		ft_strdel(&path);
	}
	if (*mat)
		(*o_ptr)->material = *mat;
	ft_strdel(&name);
}

int			parser_materials(t_config *cfg,
				t_object **o_ptr, t_material *mat, char *content)
{
	char	*name;

	name = ft_strtrim(content);
	while (mat && !((*o_ptr)->material))
	{
		if (!ft_strcmp(mat->name, name))
			(*o_ptr)->material = mat;
		else
			mat = mat->next;
	}
	if (!(*o_ptr)->material)
	{
		st_get_material(cfg, o_ptr, &mat, name);
	}
	ft_memdel((void**)&name);
	return ((*o_ptr)->material != NULL);
}
