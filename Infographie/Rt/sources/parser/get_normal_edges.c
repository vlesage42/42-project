/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_normal_edges.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/12/01 03:30:23 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

void	get_square_triangle_edges(t_object **o)
{
	(**o).edges[0] = vec3_sub((**o).vertices.v1, (**o).vertices.v0);
	(**o).edges[1] = vec3_sub((**o).vertices.v2, (**o).vertices.v1);
	if ((**o).type == k_square)
	{
		(**o).edges[2] = vec3_sub((**o).vertices.v3, (**o).vertices.v2);
		(**o).edges[3] = vec3_sub((**o).vertices.v0, (**o).vertices.v3);
	}
	else if ((**o).type == k_triangle)
	{
		(**o).edges[2] = vec3_sub((**o).vertices.v0, (**o).vertices.v2);
	}
}

void	get_square_triangle_normal(t_object **o)
{
	const t_vec3 v0v1 = vec3_sub((**o).vertices.v1, (**o).vertices.v0);
	const t_vec3 v0v2 = vec3_sub((**o).vertices.v2, (**o).vertices.v0);

	(**o).normal = vec3_normalize(CROSS(v0v1, v0v2));
}
