/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_mat_tex.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/04/13 16:42:42 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

int		get_mat_tex(t_object **o, t_engine *egn, t_pxml *xml, t_pxml_node *node)
{
	int			e;
	t_pxml_node	*n;

	if ((n = pxml_getchild(xml, node, "material")) ||
		(e = 0))
		e = parser_materials(&(egn->config), o, egn->materials, n->content);
	if ((n = pxml_getchild(xml, node, "texture")) &&
		parser_textures(&(egn->config), o, egn->textures, n->content))
	{
		if ((*o)->texture && (*o)->texture->mode == k_procedural)
		{
			if ((*o)->texture->pattern == k_sinus)
				(*o)->proc = &procedural_sinus;
			else if ((*o)->texture->pattern == k_lines)
				(*o)->proc = &procedural_lines;
			else if ((*o)->texture->pattern == k_checkerboard)
				(*o)->proc = &procedural_checkerboard;
		}
	}
	return (e);
}
