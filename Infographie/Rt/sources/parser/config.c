/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   config.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/12/01 06:39:17 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static int	st_window(t_window *win, t_pxml *xml, t_pxml_node *n_cam)
{
	int			e;
	t_pxml_node	*n_vp;

	e = 1;
	if ((n_vp = pxml_getchild(xml, n_cam, "width")) ||
		(e = 0))
		win->width = ft_max2f(1280., ft_ato2f(n_vp->content));
	if (e &&
		((n_vp = pxml_getchild(xml, n_cam, "height")) ||
		(e = 0)))
		win->height = ft_max2f(720., ft_ato2f(n_vp->content));
	return (e);
}

static int	st_viewplane(t_viewplane *vp, t_pxml *xml, t_pxml_node *n_cam)
{
	int			e;
	t_pxml_node	*n_vp;

	e = 1;
	if ((n_vp = pxml_getchild(xml, n_cam, "width")) ||
		(e = 0))
		vp->width = ft_ato2f(n_vp->content);
	if (e &&
		((n_vp = pxml_getchild(xml, n_cam, "height")) ||
		(e = 0)))
		vp->height = ft_ato2f(n_vp->content);
	if (e &&
		((n_vp = pxml_getchild(xml, n_cam, "distance")) ||
		(e = 0)))
		vp->distance = ft_ato2f(n_vp->content);
	return (e);
}

static int	st_ext_directories(t_env *env, t_pxml *xml, t_pxml_node *node)
{
	int					e;
	t_pxml_node			*n;
	t_config *const		cfg = &(env->engine.config);

	e = 1;
	if (e && ((n = pxml_getchild(xml, node, "images_dir")) ||
		(e = 0)))
		cfg->images_dir = ft_strtrim(n->content);
	if (e && ((n = pxml_getchild(xml, node, "materials_dir")) ||
		(e = 0)))
		cfg->materials_dir = ft_strtrim(n->content);
	if (e && ((n = pxml_getchild(xml, node, "textures_dir")) ||
		(e = 0)))
		cfg->textures_dir = ft_strtrim(n->content);
	if (e && ((n = pxml_getchild(xml, node, "composed_dir")) ||
		(e = 0)))
		cfg->composed_dir = ft_strtrim(n->content);
	return (e);
}

static int	st_ext_pars_conf(t_env *env, t_pxml *xml, t_pxml_node *node)
{
	int					e;
	t_pxml_node			*n;
	t_config *const		cfg = &(env->engine.config);

	e = 1;
	if ((n = pxml_getchild(xml, node, "nb_thread")) ||
		(e = 0))
		cfg->nb_thread = ft_max(1, ft_atoi(n->content));
	if (e && ((n = pxml_getchild(xml, node, "ambient")) ||
		(e = 0)))
		cfg->ambient = ft_get_v3color(n->content);
	if (e && ((n = pxml_getchild(xml, node, "antialiasing")) ||
		(e = 0)))
		cfg->antialiasing = ft_max(1, ft_atoi(n->content));
	if (e && ((n = pxml_getchild(xml, node, "ray_depth")) ||
		(e = 0)))
		cfg->ray_depth = ft_atoi(n->content);
	if (e && ((n = pxml_getchild(xml, node, "bias")) ||
		(e = 0)))
		cfg->bias = ft_ato2f(n->content);
	return (e);
}

int			parser_config(t_env *env, t_pxml *xml, t_pxml_node *node)
{
	int					e;
	t_window *const		win = &(env->pmlx.window);
	t_viewplane *const	vp = &(env->engine.camera.vp);
	t_pxml_node			*n;

	e = 1;
	if (e && ((n = pxml_getchild(xml, node, "window")) ||
		(e = 0)))
		st_window(win, xml, n);
	e = st_ext_pars_conf(env, xml, node);
	if (e && ((n = pxml_getchild(xml, node, "viewplane")) ||
		(e = 0)))
		st_viewplane(vp, xml, n);
	e = st_ext_directories(env, xml, node);
	return (e);
}
