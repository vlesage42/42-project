/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   textures_2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/12/01 06:30:38 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static void	st_get_tex_mode(t_texture **t_ptr, const char *mode)
{
	if (!ft_strcmp(mode, "uv_mapping") || !ft_strcmp(mode, "mapping"))
		(*t_ptr)->mode = k_uv_mapping;
	else if (!ft_strcmp(mode, "procedural") || !ft_strcmp(mode, "proc"))
		(*t_ptr)->mode = k_procedural;
}

static void	st_get_pattern(t_texture **t_ptr, const char *pattern)
{
	if (!ft_strcmp(pattern, "image"))
		(*t_ptr)->pattern = k_image;
	else if (!ft_strcmp(pattern, "sinus"))
		(*t_ptr)->pattern = k_sinus;
	else if (!ft_strcmp(pattern, "lines"))
		(*t_ptr)->pattern = k_lines;
	else if (!ft_strcmp(pattern, "checkerboard") ||
				!ft_strcmp(pattern, "checker"))
		(*t_ptr)->pattern = k_checkerboard;
}

static void	st_get_image(t_config *cfg, t_texture **t_ptr, const char *image)
{
	char *tmp;
	char *path;

	tmp = ft_strjoin(cfg->images_dir, "objs/");
	path = ft_strjoin(tmp, image);
	(*t_ptr)->image = (t_image*)ft_memalloc(sizeof(t_image));
	load_png((*t_ptr)->image, path);
	ft_strdel(&path);
}

static void	st_new_texture(t_texture **t_ptr, t_config *cfg,
							t_pxml *xml, t_pxml_node *n_ptr)
{
	const t_pxml_node	*c_ptr = NULL;
	const char			*tex_str = NULL;

	if (((*t_ptr) = (t_texture*)ft_memalloc(sizeof(t_texture))))
	{
		(*t_ptr)->name = ft_strdup(n_ptr->name);
		(*t_ptr)->blend = 1.;
		(*t_ptr)->scale = 1.;
		if ((c_ptr = pxml_getchild(xml, n_ptr, "tex_mode")))
			st_get_tex_mode(t_ptr, (tex_str = ft_strtrim(c_ptr->content)));
		ft_strdel((char **)&tex_str);
		if ((c_ptr = pxml_getchild(xml, n_ptr, "pattern")))
			st_get_pattern(t_ptr, (tex_str = ft_strtrim(c_ptr->content)));
		ft_strdel((char **)&tex_str);
		if ((*t_ptr)->mode == k_uv_mapping && ((*t_ptr)->pattern = k_image) &&
			(c_ptr = pxml_getchild(xml, n_ptr, "image")))
			st_get_image(cfg, t_ptr, (tex_str = ft_strtrim(c_ptr->content)));
		ft_strdel((char **)&tex_str);
		if ((c_ptr = pxml_getchild(xml, n_ptr, "blend")))
			(*t_ptr)->blend = ft_ato2f(c_ptr->content);
		if ((c_ptr = pxml_getchild(xml, n_ptr, "scale")))
			(*t_ptr)->scale = ft_ato2f(c_ptr->content);
		if ((c_ptr = pxml_getchild(xml, n_ptr, "angle")))
			(*t_ptr)->angle = ft_ato2f(c_ptr->content);
	}
}

void		st_get_texture(t_config *cfg, t_texture **tex, char *cnt)
{
	t_pxml		*xml;
	t_pxml_node	*node;
	char		*name;
	char		*path;

	if ((name = (!ft_strstr(cnt, ".xml")) ?
		ft_strjoin(cnt, ".xml") : ft_strdup(cnt)))
	{
		path = ft_strjoin(cfg->textures_dir, name);
		if ((xml = pxml_init(path)))
		{
			if ((node = (t_pxml_node*)pxml_root(xml)))
				st_new_texture(tex, cfg, xml, node);
			else
				ft_putendl(" texture pxml_root fail");
			pxml_close(&xml);
		}
		else
			ft_putendl(" texture pxml_init fail");
		ft_strdel(&path);
		ft_strdel(&name);
	}
}
