/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   deletes.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/04/13 16:42:42 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

void	rt_delete_objects(t_object *o)
{
	t_object	*ptr;

	if (o)
	{
		ptr = o->next;
		if (o->comp)
			ft_memdel((void**)&(o->comp));
		ft_memdel((void**)&(o));
		rt_delete_objects(ptr);
	}
}

void	rt_delete_materials(t_material *m)
{
	t_material	*ptr;

	if (m)
	{
		ft_strdel(&(m->name));
		ptr = m->next;
		ft_memdel((void**)&(m));
		rt_delete_materials(ptr);
	}
}

void	rt_delete_textures(t_texture *tex)
{
	t_texture *ptr;

	ptr = tex;
	while (tex)
	{
		if (tex->image)
		{
			if (tex->image->data)
				free(tex->image->data);
			free(tex->image);
		}
		ptr = tex->next;
		free(tex);
		tex = ptr;
	}
}

void	rt_delete_lights(t_light *l)
{
	t_light	*ptr;

	if (l)
	{
		ptr = l->next;
		ft_memdel((void**)&(l));
		rt_delete_lights(ptr);
	}
}
