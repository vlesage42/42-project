/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   scenes.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/04/13 16:42:42 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

int		parser_scene(t_engine *engine, t_pxml *xml, t_pxml_node *node)
{
	int		err;

	err = 1;
	if ((node = (t_pxml_node*)pxml_solve(xml, "xml/scene/objects")))
	{
		err = parser_objects(engine, xml, node);
	}
	engine->scene.lights = NULL;
	if (err && ((node = (t_pxml_node*)pxml_solve(xml, "xml/scene/lights"))))
	{
		err = parser_lights(&(engine->scene.lights), xml, node);
	}
	return (err);
}
