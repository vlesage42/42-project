/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/12/01 03:43:25 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static int	st_ext_parser(t_env *env, t_pxml *xml, t_pxml_node *node)
{
	int	err;

	err = 1;
	if (err && ((node = (t_pxml_node*)pxml_solve(xml, "xml/camera")) ||
		(err = 0)))
	{
		err = parser_camera(&(env->engine.camera), xml, node);
	}
	if (err && ((node = (t_pxml_node*)pxml_solve(xml, "xml/scene")) ||
		(err = 0)))
	{
		err = parser_scene(&(env->engine), xml, node);
	}
	return (err);
}

int			rt_parser(t_env *env)
{
	int			err;
	t_pxml		*pxml;
	t_pxml_node	*root;

	pxml = NULL;
	if ((err = open_xml(&pxml, &root, env->engine.config.file)))
	{
		err = st_ext_parser(env, pxml, root);
	}
	if (pxml)
	{
		pxml_close(&pxml);
	}
	return (err);
}
