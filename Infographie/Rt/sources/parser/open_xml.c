/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   open_xml.cpp                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 21:10:20 by abartz            #+#    #+#             */
/*   Updated: 2016/09/22 21:11:53 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

t_uint8		open_xml(t_pxml **pxml, t_pxml_node **root, const char *file)
{
	t_uint8	err;

	if (((*pxml) = pxml_init(file)))
	{
		if (((*root) = (t_pxml_node*)pxml_root(*pxml)))
			err = 1;
		else
		{
			err = 0;
			ft_putstr(file);
			ft_putendl(" : pxml_root fail");
		}
	}
	else
	{
		err = 0;
		ft_putstr(file);
		ft_putendl(" : pxml_init fail");
	}
	return (err);
}
