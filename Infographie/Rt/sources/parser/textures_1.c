/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   textures_1.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/01 06:22:41 by aboucher          #+#    #+#             */
/*   Updated: 2016/12/01 06:24:52 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

int			parser_textures(t_config *cfg,
				t_object **o_ptr, t_texture *tex, char *content)
{
	char	*name;

	name = ft_strtrim(content);
	while (tex && !((*o_ptr)->texture))
	{
		if (!ft_strcmp(tex->name, name))
			(*o_ptr)->texture = tex;
		else
			tex = tex->next;
	}
	if (!(*o_ptr)->texture)
	{
		st_get_texture(cfg, &tex, name);
		if (tex)
			(*o_ptr)->texture = tex;
	}
	ft_strdel(&name);
	return (1);
}
