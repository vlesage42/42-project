/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   axis_rotation.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/12/01 03:29:28 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static void		st_get_mrotate(t_matrix mrotate, t_object **obj,
								t_pxml_node *node)
{
	t_matrix	mat_xyz[3];
	t_matrix	tmp;
	t_vec3		rotate;

	if (node)
		(*obj)->rotation = ft_get_vec3(node->content);
	init_matrix(tmp);
	init_matrix(mrotate);
	rotate = (*obj)->rotation;
	init_matrix_rotation(mat_xyz[0], rotate.x, 'x');
	init_matrix_rotation(mat_xyz[1], rotate.y, 'y');
	init_matrix_rotation(mat_xyz[2], rotate.z, 'z');
	matrice_mult(mat_xyz[0], mat_xyz[1], tmp);
	matrice_mult(tmp, mat_xyz[2], mrotate);
}

void			rotate_composed(t_object **obj)
{
	t_matrix	mrotate;
	t_vec3		vrotate;

	st_get_mrotate(mrotate, obj, NULL);
	vrotate = (*obj)->axis;
	mat_vec3_mult(mrotate, vrotate, &((*obj)->axis));
	vrotate = (*obj)->comp->pos;
	mat_vec3_mult(mrotate, vrotate, &((*obj)->comp->pos));
	check_vector(&((*obj)->position));
	check_vector(&((*obj)->comp->pos));
	matrix_inverse(mrotate, (*obj)->comp->inv);
}

static void		st_ext_rotation(t_object **obj, t_pxml_node *c_ptr)
{
	t_matrix	mrotate;
	t_vec3		vrotate;

	st_get_mrotate(mrotate, obj, c_ptr);
	if ((*obj)->type == k_square || (*obj)->type == k_triangle)
		rotate_vertices(obj, mrotate);
	else if ((*obj)->type == k_plane || (*obj)->type == k_disk)
	{
		mat_vec3_mult(mrotate, (vrotate = (*obj)->normal), &((*obj)->normal));
		(*obj)->axis = (*obj)->normal;
	}
	else
		mat_vec3_mult(mrotate, (vrotate = (*obj)->axis), &((*obj)->axis));
	matrix_inverse(mrotate, (*obj)->inverse);
}

void			axis_rotation(t_object **obj, t_pxml *xml, t_pxml_node *n_ptr)
{
	t_pxml_node	*c_ptr;

	if ((*obj)->type == k_plane || (*obj)->type == k_disk)
	{
		if ((c_ptr = pxml_getchild(xml, n_ptr, "normal")))
		{
			(*obj)->normal = vec3_normalize(ft_get_vec3(c_ptr->content));
			(*obj)->axis = (*obj)->normal;
		}
	}
	else if ((*obj)->type == k_square || (*obj)->type == k_triangle)
	{
		get_square_triangle_normal(obj);
		(*obj)->axis = (*obj)->normal;
	}
	else if ((c_ptr = pxml_getchild(xml, n_ptr, "axis")))
		(*obj)->axis = vec3_normalize(ft_get_vec3(c_ptr->content));
	else
		(*obj)->axis = rt_init_vec3(0., 1., 0.);
	init_matrix_identity((*obj)->inverse);
	if ((c_ptr = pxml_getchild(xml, n_ptr, "rotation")))
		st_ext_rotation(obj, c_ptr);
}
