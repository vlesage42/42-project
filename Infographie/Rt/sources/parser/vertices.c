/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vertices.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/04/13 16:42:42 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static void	st_big_check(t_object **obj)
{
	check_vector(&((*obj)->position));
	check_vector(&((*obj)->normal));
	check_vector(&((*obj)->axis));
	check_vector(&((*obj)->vertices.v0));
	check_vector(&((*obj)->vertices.v1));
	check_vector(&((*obj)->vertices.v2));
	check_vector(&((*obj)->vertices.v3));
	check_vector(&((*obj)->edges[0]));
	check_vector(&((*obj)->edges[1]));
	check_vector(&((*obj)->edges[2]));
	check_vector(&((*obj)->edges[3]));
}

static void	st_get_position(t_object **o)
{
	if ((*o)->type == k_square)
	{
		(*o)->position.x = ((*o)->vertices.v0.x + (*o)->vertices.v2.x) / 2;
		(*o)->position.y = ((*o)->vertices.v0.y + (*o)->vertices.v2.y) / 2;
		(*o)->position.z = ((*o)->vertices.v0.z + (*o)->vertices.v2.z) / 2;
	}
	else
	{
		(*o)->position.x = (*o)->vertices.v0.x + (*o)->vertices.v1.x +
								(*o)->vertices.v2.x / 3;
		(*o)->position.y = (*o)->vertices.v0.y + (*o)->vertices.v1.y +
								(*o)->vertices.v2.y / 3;
		(*o)->position.z = (*o)->vertices.v0.z + (*o)->vertices.v1.z +
								(*o)->vertices.v2.z / 3;
	}
}

void		get_vertices(t_object **obj, t_pxml *xml, t_pxml_node *node)
{
	t_pxml_node	*t_ptr;

	(*obj)->vertices.v0 = rt_init_vec3(0., 0., 0.);
	(*obj)->vertices.v1 = rt_init_vec3(0., 0., 0.);
	(*obj)->vertices.v2 = rt_init_vec3(0., 0., 0.);
	(*obj)->vertices.v3 = rt_init_vec3(0., 0., 0.);
	if ((t_ptr = pxml_getchild(xml, node, "v0")))
		(*obj)->vertices.v0 = ft_get_vec3(t_ptr->content);
	if ((t_ptr = pxml_getchild(xml, node, "v1")))
		(*obj)->vertices.v1 = ft_get_vec3(t_ptr->content);
	if ((t_ptr = pxml_getchild(xml, node, "v2")))
		(*obj)->vertices.v2 = ft_get_vec3(t_ptr->content);
	if ((*obj)->type == k_square)
	{
		if ((t_ptr = pxml_getchild(xml, node, "v3")))
			(*obj)->vertices.v3 = ft_get_vec3(t_ptr->content);
	}
	st_get_position(obj);
}

void		rotate_vertices(t_object **obj, t_matrix mrotate)
{
	t_vec3		vrotate;

	mat_vec3_mult(mrotate, (vrotate = (*obj)->normal), &((*obj)->normal));
	(*obj)->axis = (*obj)->normal;
	vrotate = vec3_sub((*obj)->vertices.v0, (*obj)->position);
	mat_vec3_mult(mrotate, vrotate, &((*obj)->vertices.v0));
	vrotate = vec3_sub((*obj)->vertices.v1, (*obj)->position);
	mat_vec3_mult(mrotate, vrotate, &((*obj)->vertices.v1));
	vrotate = vec3_sub((*obj)->vertices.v2, (*obj)->position);
	mat_vec3_mult(mrotate, vrotate, &((*obj)->vertices.v2));
	if ((*obj)->type == k_square)
	{
		vrotate = vec3_sub((*obj)->vertices.v3, (*obj)->position);
		mat_vec3_mult(mrotate, vrotate, &((*obj)->vertices.v3));
	}
	(*obj)->vertices.v0 = vec3_sum((*obj)->vertices.v0, (*obj)->position);
	(*obj)->vertices.v1 = vec3_sum((*obj)->vertices.v1, (*obj)->position);
	(*obj)->vertices.v2 = vec3_sum((*obj)->vertices.v2, (*obj)->position);
	if ((*obj)->type == k_square)
		(*obj)->vertices.v3 = vec3_sum((*obj)->vertices.v3, (*obj)->position);
	get_square_triangle_edges(obj);
	st_big_check(obj);
}
