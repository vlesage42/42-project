/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   objects.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/04/13 16:42:42 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static void		st_set_object_type_method(t_object **obj, char *type,
											t_pxml *xml, t_pxml_node *node)
{
	if (!ft_strcmp(type, "plane") && ((*obj)->type = k_plane))
		(*obj)->intersect = &rt_plane_intersects;
	else if (!ft_strcmp(type, "triangle") && ((*obj)->type = k_triangle))
		(*obj)->intersect = &rt_triangle_intersects;
	else if (!ft_strcmp(type, "square") && ((*obj)->type = k_square))
		(*obj)->intersect = &rt_square_intersects;
	else if (!ft_strcmp(type, "disk") && ((*obj)->type = k_disk))
		(*obj)->intersect = &rt_disk_intersects;
	else if (!ft_strcmp(type, "sphere") && ((*obj)->type = k_sphere) &&
			((*obj)->uv_m = &tx_spherical))
		(*obj)->intersect = &rt_sphere_intersects;
	else if (!ft_strcmp(type, "cone") && ((*obj)->type = k_cone) &&
			((*obj)->uv_m = &tx_cylindrical))
		(*obj)->intersect = &rt_cone_intersects;
	else if (!ft_strcmp(type, "cylinder") && ((*obj)->type = k_cylinder) &&
			((*obj)->uv_m = &tx_cylindrical))
		(*obj)->intersect = &rt_cylinder_intersects;
	else if (!ft_strcmp(type, "paraboloid") && ((*obj)->type = k_paraboloid) &&
			((*obj)->uv_m = &tx_cylindrical))
		(*obj)->intersect = &rt_paraboloid_intersects;
	if (check_planar_types((*obj)))
		(*obj)->uv_m = &tx_planar;
	if ((*obj)->type == k_triangle || (*obj)->type == k_square)
		get_vertices(obj, xml, node);
}

static void		st_ext_new_object(t_object **obj, t_pxml_node *c_ptr,
											t_pxml *xml, t_pxml_node *node)
{
	char *mode;

	if ((c_ptr = pxml_getchild(xml, node, "position")))
		(*obj)->position = ft_get_vec3(c_ptr->content);
	if ((c_ptr = pxml_getchild(xml, node, "radius")))
		(*obj)->radius = ft_ato2f(c_ptr->content);
	if ((c_ptr = pxml_getchild(xml, node, "mode")))
	{
		mode = ft_strtrim(c_ptr->content);
		if (!ft_strcmp(mode, "infinity"))
			(*obj)->mode = k_infinity;
		else if (!ft_strcmp(mode, "center"))
			(*obj)->mode = k_center;
		else if (!ft_strcmp(mode, "offset"))
			(*obj)->mode = k_offset;
		ft_strdel(&mode);
	}
	axis_rotation(obj, xml, node);
	if ((c_ptr = pxml_getchild(xml, node, "angle")))
		(*obj)->angle = ft_ato2f(c_ptr->content);
	if ((c_ptr = pxml_getchild(xml, node, "height")))
		(*obj)->height = ft_ato2f(c_ptr->content);
	if ((c_ptr = pxml_getchild(xml, node, "kernel")))
		(*obj)->kernel = ft_ato2f(c_ptr->content);
}

t_object		*rt_new_object(t_engine *egn, t_pxml *xml, t_pxml_node *node)
{
	t_object	*obj;
	t_pxml_node	*n;
	int			e;

	n = NULL;
	if ((obj = (t_object*)ft_memalloc(sizeof(t_object))))
	{
		st_set_object_type_method(&obj, node->name, xml, node);
		if (obj->type == k_triangle && obj->type == k_square)
			axis_rotation(&obj, xml, node);
		else
			st_ext_new_object(&obj, n, xml, node);
		e = get_mat_tex(&obj, egn, xml, node);
		if (!e)
		{
			if (obj->comp)
				ft_memdel((void**)&(obj->comp));
			ft_memdel((void**)&(obj));
		}
	}
	return (obj);
}

static int		st_ext_parser(t_object **o, t_engine *engine,
								t_pxml *xml, t_pxml_node *child)
{
	if (!ft_strcmp(child->name, "composed"))
	{
		parser_conposed(o, engine, xml, child);
	}
	else
	{
		*o = rt_new_object(engine, xml, child);
	}
	return (*o != NULL);
}

int				parser_objects(t_engine *engine, t_pxml *xml, t_pxml_node *node)
{
	t_pxml_node	*child;
	t_object	*first;
	t_object	**o;

	first = NULL;
	if ((child = (t_pxml_node*)pxml_children(xml, node)))
	{
		o = &(engine->scene.objs);
		if (st_ext_parser(o, engine, xml, child))
		{
			first = (*o);
			while ((*o)->next)
				(*o) = (*o)->next;
			while ((child = (t_pxml_node*)pxml_next(xml, child))
				&& st_ext_parser(&((*o)->next), engine, xml, child))
			{
				while ((*o)->next)
					(*o) = (*o)->next;
			}
		}
		engine->scene.objs = first;
	}
	return (1);
}
