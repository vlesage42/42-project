/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_v3.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/04/13 16:42:42 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

t_vec3		ft_get_vec3(char *b)
{
	t_vec3	vec;

	vec.x = ft_ato2f(b);
	vec.y = ft_ato2f(ft_getword(2, b));
	vec.z = ft_ato2f(ft_getword(3, b));
	vec.len = 0.;
	return (vec);
}

t_v3color	ft_get_v3color(char *b)
{
	t_v3color	color;

	color.r = ft_ato2f(b);
	color.g = ft_ato2f(ft_getword(2, b));
	color.b = ft_ato2f(ft_getword(3, b));
	return (color);
}
