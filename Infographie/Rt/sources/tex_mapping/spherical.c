/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   spherical.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 21:10:20 by abartz            #+#    #+#             */
/*   Updated: 2016/09/22 21:11:53 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static void			st_get_spherical_uv(t_info info, double *u, double *v)
{
	double	phi;
	double	theta;
	t_vec3	t;

	t = world_to_local(info);
	t = cartesian2spherical(vec3_normalize(t));
	phi = phi_from_spherical(t);
	theta = theta_from_spherical(t);
	*u = phi / (2 * M_PI);
	*v = theta / M_PI;
}

t_v3color			tx_spherical(t_info info)
{
	int				i;
	t_vec2			uv;
	t_v3color		tex;
	t_uint8			*data;
	const t_image	*img = info.object->texture->image;

	st_get_spherical_uv(info, &(uv.x), &(uv.y));
	i = (int)(img->height * uv.y) * ((int)(img->width * img->bpp)) +
		(int)(img->width * uv.x) * img->bpp;
	check_data_index(img, &i);
	data = (img->data);
	tex.r = ((double)(data[i])) / 255.;
	tex.g = ((double)(data[i + 1])) / 255.;
	tex.b = ((double)(data[i + 2])) / 255.;
	return (tex);
}
