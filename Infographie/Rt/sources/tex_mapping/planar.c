/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   planar.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 21:10:20 by abartz            #+#    #+#             */
/*   Updated: 2016/09/22 21:11:53 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static void	st_ext_planar(t_info info, double *x, double *y)
{
	const double	h = info.object->texture->image->height;
	const double	w = info.object->texture->image->width;
	const double	s = info.object->texture->scale;
	t_vec3			t;

	t = world_to_local(info);
	while (t.x < 0)
		(t.x) += s;
	while (t.x > s)
		(t.x) -= s;
	while (t.z < 0)
		(t.z) += s;
	while (t.z > s)
		(t.z) -= s;
	*x = t.x / s * w;
	*y = (1 - (t.z / s)) * h;
}

t_v3color	tx_planar(t_info info)
{
	int				i;
	t_vec2			uv;
	t_v3color		tex;
	t_uint8			*data;
	const t_image	*img = info.object->texture->image;

	st_ext_planar(info, &(uv.x), &(uv.y));
	i = (int)uv.y * ((int)(img->width * img->bpp)) + (int)uv.x * img->bpp;
	check_data_index(img, &i);
	data = (img->data);
	tex.r = ((double)(data[i])) / 255.;
	tex.g = ((double)(data[i + 1])) / 255.;
	tex.b = ((double)(data[i + 2])) / 255.;
	return (tex);
}
