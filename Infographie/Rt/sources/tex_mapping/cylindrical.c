/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cylindrical.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 21:10:20 by abartz            #+#    #+#             */
/*   Updated: 2016/09/22 21:11:53 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static t_vec3	st_cartesian2cylindrical(t_vec3 vec)
{
	t_matrix	mat;
	t_vec3		result;

	init_matrix(mat);
	mat[0][0] = 1;
	mat[1][2] = 1;
	mat[2][1] = 1;
	init_vec3(&result);
	mat_vec3_mult(mat, vec, &result);
	return (result);
}

static double	st_theta_from_cylindrical(const t_vec3 v)
{
	const double p = atan2(v.y, v.x);

	return ((p < 0.) ? p + 2. * M_PI : p);
}

static void		st_get_cylindrical_uv(t_info info, double *u, double *v)
{
	t_vec3			t;
	double			theta;
	const double	h = info.object->height;

	t = world_to_local(info);
	t = st_cartesian2cylindrical(t);
	theta = st_theta_from_cylindrical(t);
	*u = theta / (2 * M_PI);
	while (t.z < 0)
		t.z += h;
	while (t.z > h)
		t.z -= h;
	*v = 1 - (t.z / info.object->height);
}

t_v3color		tx_cylindrical(t_info info)
{
	int				i;
	t_vec2			uv;
	t_v3color		tex;
	const t_image	*img = info.object->texture->image;

	st_get_cylindrical_uv(info, &(uv.x), &(uv.y));
	i = (int)(img->height * uv.y) * ((int)(img->width * img->bpp)) +
		(int)(img->width * uv.x) * img->bpp;
	check_data_index(img, &i);
	tex.r = ((double)(img->data[i])) / 255.;
	tex.g = ((double)(img->data[i + 1])) / 255.;
	tex.b = ((double)(img->data[i + 2])) / 255.;
	return (tex);
}
