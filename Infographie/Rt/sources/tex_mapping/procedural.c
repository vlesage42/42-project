/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   procedural.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/04/13 16:42:42 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

double			procedural_sinus(t_info info)
{
	const t_vec3 vlocal = world_to_local(info);
	const double scale = info.object->texture->scale;
	const double rad = RAD1 * info.object->texture->angle;
	const double s = vlocal.z * cos(rad) - vlocal.x * sin(rad);

	return ((sin(s * 2 * M_PI * scale) + 1) * 0.5);
}

static double	modulo(const double x)
{
	return (x - floor(x));
}

double			procedural_lines(t_info info)
{
	const t_vec3 vlocal = world_to_local(info);
	const double scale = info.object->texture->scale;
	const double rad = RAD1 * info.object->texture->angle;
	const double s = vlocal.z * cos(rad) - vlocal.x * sin(rad);

	return ((modulo(s * scale) < 0.5));
}

double			procedural_checkerboard(t_info info)
{
	const t_vec3 vlocal = world_to_local(info);
	const double scale = info.object->texture->scale;
	const double rad = RAD1 * info.object->texture->angle;
	const double s = vlocal.x * cos(rad) - vlocal.z * sin(rad);
	const double t = vlocal.z * cos(rad) + vlocal.x * sin(rad);

	return ((modulo(s * scale) < 0.5) ^ (modulo(t * scale) < 0.5));
}
