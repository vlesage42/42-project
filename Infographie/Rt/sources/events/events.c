/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   events.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/04/13 16:42:42 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

int		rt_mouse_up(int btn, int x, int y, void *param)
{
	t_env			*env;
	const t_vec2i	coord = {x, y};

	env = (t_env*)param;
	if (btn == 1 && ft_strcmp(env->ui.path, "./home/RT"))
	{
		if (!ft_strcmp(env->ui.path, "./home"))
		{
			rt_ui_home_click(env, coord);
		}
		else if (!ft_strncmp(env->ui.path, "./home/multi", 12))
		{
			rt_ui_multi_click(env, coord);
		}
		else if (!ft_strcmp(env->ui.path, "./home/options"))
		{
			;
		}
	}
	return (EXIT_SUCCESS);
}

int		rt_key_up(int keycode, void *param)
{
	t_env		*env;

	env = (t_env*)param;
	if (keycode == key_escape)
	{
		delete_rt(env);
		exit(EXIT_SUCCESS);
	}
	else if (!ft_strcmp(env->ui.path, "./home/RT"))
	{
		render_keyboard(env, keycode);
	}
	else if (!ft_strcmp(env->ui.path, "./home"))
	{
		rt_ui_home_keyup(env, keycode);
	}
	return (EXIT_SUCCESS);
}

int		rt_expose(void *param)
{
	static t_uint8	i = 0;
	t_env			*env;

	env = (t_env*)param;
	if (!i && ++i)
	{
		rt_init_ui(&env->ui, env->pmlx.window, &(env->engine.config));
		rt_ui_home(&(env->pmlx.interface), &(env->ui.home), env->ui.path);
	}
	if (!ft_strcmp(env->ui.path, "./home/RT"))
		mlx_put_image_to_window(env->pmlx.handler, env->pmlx.window.ptr,
								env->pmlx.rendering.ptr, 0, 0);
	else if (!ft_strncmp(env->ui.path, "./home", 6))
		mlx_put_image_to_window(env->pmlx.handler, env->pmlx.window.ptr,
								env->pmlx.interface.ptr, 0, 0);
	return (EXIT_SUCCESS);
}
