/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rt_events.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/12/01 11:24:40 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static void	st_refresh(t_env *env)
{
	clear_image(&env->pmlx.rendering);
	rt_render(env);
	glClear(GL_COLOR_BUFFER_BIT);
	mlx_put_image_to_window(env->pmlx.handler, env->pmlx.window.ptr,
		env->pmlx.rendering.ptr, 0, 0);
	glFlush();
}

static void	st_ext_init_cam(t_env *e, int k)
{
	if (k == key_x || k == key_y || k == key_z || k == key_a || k == key_d ||
		k == key_i || k == keypad_0 || k == keypad_1 || k == keypad_2)
	{
		rt_init_cam(e);
		st_refresh(e);
	}
}

void		render_keyboard(t_env *e, int k)
{
	if (k == key_x)
		e->engine.camera.pos = rt_init_vec3(-50., e->engine.camera.pos.y, 0.);
	else if (k == key_z)
		e->engine.camera.pos = rt_init_vec3(0., e->engine.camera.pos.y, -50);
	else if (k == key_i)
		e->engine.camera.dir = vec3_mult(e->engine.camera.dir, -1);
	else if (k == key_a && !e->engine.camera.pos.x)
		e->engine.camera.pos.z *= -1;
	else if (k == key_a && !e->engine.camera.pos.z)
		e->engine.camera.pos.x *= -1;
	else if (k == keypad_0 || k == keypad_1)
		e->engine.scene.objs[0].mode = (k == keypad_0) ? k_infinity : k_center;
	else if (k == keypad_2)
		e->engine.scene.objs[0].mode = k_offset;
	st_ext_init_cam(e, k);
	if (k == key_f5)
		st_refresh(e);
	else if (k == key_s)
		save_png(&(e->pmlx.window), &(e->pmlx.rendering), "out.png");
	else if (k == key_h)
		ui_loading(e);
}
