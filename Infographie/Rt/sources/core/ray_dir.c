/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   camera.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/04/13 16:42:42 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

void	rt_init_cam(t_env *env)
{
	t_camera		*c;

	c = &(env->engine.camera);
	c->dir = vec3_normalize(vec3_sub(c->look_at, c->pos));
	if (!c->dir.x && c->dir.y && !c->dir.z)
		c->right = rt_init_vec3(1., 0., 0.);
	else
		c->right = CROSS(rt_init_vec3(0., 1., 0.), c->dir);
	c->up = CROSS(c->dir, c->right);
	c->vp.upleft = vec3_sub(
		vec3_sum(
			c->pos, vec3_sum(
				vec3_mult(c->dir, c->vp.distance),
				vec3_mult(c->up, c->vp.height / 2.))),
			vec3_mult(c->right, c->vp.width / 2.));
}

t_vec3	ray_dir(t_env *env, t_vec2i coord, t_vec2 rez)
{
	t_camera *const		c = &(env->engine.camera);
	const double		indent[2] = {c->vp.width / rez.x, c->vp.height / rez.y};
	const t_vec3		v[2] = {vec3_mult(c->right, indent[0]),
								vec3_mult(c->up, indent[1])};

	return (vec3_sub(
				vec3_sub(
					vec3_sum(c->vp.upleft,
						vec3_mult(v[0], (double)(coord.x))),
					vec3_mult(v[1], (double)(coord.y))),
				c->pos));
}
