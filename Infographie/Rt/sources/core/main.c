/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/04/13 16:42:42 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static void	st_new_image(t_mlx *pmlx, t_image *img)
{
	const double	width = pmlx->window.width;
	const double	height = pmlx->window.height;
	const int		buffer = (int)(width * height * 4.);

	img->ptr = mlx_new_image(pmlx->handler, (int)width, (int)height);
	img->width = width;
	img->height = height;
	img->data = (unsigned char*)mlx_get_data_addr(
			img->ptr, &(img->bpp), &(img->size_line), 0);
	ft_bzero(img->data, buffer);
}

void		init_rt(t_mlx *pmlx)
{
	pmlx->handler = mlx_init();
	pmlx->window.ptr = mlx_new_window(pmlx->handler,
		(float)(pmlx->window.width), (float)(pmlx->window.height), "Raytracer");
	st_new_image(pmlx, &(pmlx->rendering));
	st_new_image(pmlx, &(pmlx->interface));
}

void		delete_rt(t_env *env)
{
	if (env->engine.config.file)
		ft_strdel(&(env->engine.config.file));
	if (env->engine.config.images_dir)
		ft_strdel(&(env->engine.config.images_dir));
	if (env->engine.config.materials_dir)
		ft_strdel(&(env->engine.config.materials_dir));
	if (env->engine.config.textures_dir)
		ft_strdel(&(env->engine.config.textures_dir));
	if (env->engine.config.composed_dir)
		ft_strdel(&(env->engine.config.composed_dir));
	if (!ft_strcmp(env->ui.path, "./home/RT"))
	{
		rt_delete_materials(env->engine.materials);
		rt_delete_textures(env->engine.textures);
		rt_delete_objects(env->engine.scene.objs);
		rt_delete_lights(env->engine.scene.lights);
	}
	rt_delete_ui(&(env->ui));
	free(env);
}

int			main(int argc, char **argv)
{
	t_env	*env;

	if (argc == 2)
	{
		env = (t_env*)ft_memalloc(sizeof(t_env));
		if (init_config(env, argv[1]))
		{
			init_rt(&(env->pmlx));
			mlx_key_hook(env->pmlx.window.ptr, &rt_key_up, env);
			mlx_mouse_hook(env->pmlx.window.ptr, &rt_mouse_up, env);
			mlx_expose_hook(env->pmlx.window.ptr, &rt_expose, env);
			mlx_loop(env->pmlx.handler);
		}
		else
			delete_rt(env);
	}
	else
		ft_putendl("Raytracer takes 1 parameter");
	return (EXIT_SUCCESS);
}
