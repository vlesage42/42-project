/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shadow.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/24 18:18:21 by vlesage           #+#    #+#             */
/*   Updated: 2016/12/02 19:33:11 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

void	rt_shadow(t_object *o, t_ray ray, t_info *inf)
{
	t_info			tmp;
	t_object *const	ptr = inf->object;
	t_object *const	first = o;

	init_info(&tmp);
	inf->object = NULL;
	while ((!inf->object || inf->object->material->type == k_refraction) && o)
	{
		if (o != ptr && o->material->type != k_negative)
			if (o->intersect(ray, *o, &tmp))
				st_check_dist(o, ray, inf, &tmp);
		o = o->next;
	}
	o = first;
	while (inf->object && o)
	{
		if (o != inf->object && o->material->type == k_negative)
		{
			if (vec3_square_length(vec3_sub(inf->intersect, o->position)) <=
			(o->radius * o->radius))
				init_info(inf);
		}
		o = o->next;
	}
}
