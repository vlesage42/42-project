/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   trace.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/24 18:18:21 by vlesage           #+#    #+#             */
/*   Updated: 2016/12/01 11:27:19 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

void		st_check_dist(t_object *obj, t_ray ray, t_info *inf, t_info *tmp)
{
	tmp->distance = vec3_square_length(vec3_sub(tmp->intersect, ray.origin));
	if (tmp->distance < inf->distance)
	{
		inf->distance = tmp->distance;
		inf->intersect = tmp->intersect;
		inf->normal = tmp->normal;
		inf->object = obj;
		init_info(tmp);
	}
}

static void	st_ext_primary(t_object *p_objs, t_ray ray, t_info *inf)
{
	t_info	tmp;

	init_info(&tmp);
	while (p_objs)
	{
		if (p_objs->intersect(ray, *p_objs, &tmp))
			st_check_dist(p_objs, ray, inf, &tmp);
		p_objs = p_objs->next;
	}
}

int			trace(t_object *p_objs, t_ray ray, t_info *inf, t_ray_type r)
{
	if (r == k_primary)
		st_ext_primary(p_objs, ray, inf);
	else if (r == k_shadow)
		rt_shadow(p_objs, ray, inf);
	if (inf->object)
		inf->albedo = get_tex(inf);
	return (inf->object != NULL);
}
