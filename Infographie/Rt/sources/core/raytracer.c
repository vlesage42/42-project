/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   raytracer.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/12/02 02:18:57 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <float.h>
#include "raytracer.h"

void			init_info(t_info *inf)
{
	inf->object = NULL;
	inf->distance = DBL_MAX;
	inf->intersect = rt_init_vec3(0., 0., 0.);
}

static t_vec3	st_get_bias(t_info info, double bias)
{
	return (vec3_sum(info.intersect, vec3_mult(info.normal, bias)));
}

t_v3color		cast_ray(t_env *env, t_ray ray, t_uint32 depth)
{
	t_info		info;
	t_v3color	hit_color;

	hit_color = rt_init_v3color(0.128, 0.128, 0.128);
	if (depth < env->engine.config.ray_depth)
	{
		init_info(&(info));
		if (trace(env->engine.scene.objs, ray, &info, k_primary))
		{
			if (info.object->material->type == k_phong ||
				info.object->material->type == k_reflection)
				info.intersect = st_get_bias(info, env->engine.config.bias);
			if (info.object->material->type == k_phong)
				hit_color = rt_phong(env, ray, info, env->engine.scene.lights);
			else if (info.object->material->type == k_reflection)
				hit_color = rt_reflection(env, ray, info, depth);
			else if (info.object->material->type == k_refraction)
				hit_color = rt_refraction(env, ray, info, depth);
			else if (info.object->material->type == k_negative)
				hit_color = rt_negative(env, ray, info, depth);
		}
	}
	return (hit_color);
}
