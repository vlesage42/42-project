/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_config.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/04/13 16:42:42 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

int		init_config(t_env *env, char *scene_file)
{
	int			err;
	t_pxml		*xml;
	t_pxml_node	*node;

	err = 0;
	if ((xml = pxml_init("./config.ini.xml")))
	{
		env->engine.config.file = ft_strdup(scene_file);
		if ((node = (t_pxml_node*)pxml_root(xml)))
		{
			if ((node = (t_pxml_node*)pxml_solve(xml, "xml/config")) ||
					(err = 0))
				err = parser_config(env, xml, node);
		}
		else
			ft_putendl("Config pxml_root fail");
		pxml_close(&xml);
	}
	else
		ft_putendl("Config pxml_init fail");
	return (err);
}
