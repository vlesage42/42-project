/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/12/01 00:46:28 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <pthread.h>
#include "raytracer.h"

static t_vec3	st_ext_raydir(t_env *env, t_vec2i coord, int aa, t_vec2i c)
{
	t_vec2i	co;
	t_vec2	rez;

	co.x = coord.x * aa + c.x;
	co.y = coord.y * aa + c.y;
	rez.x = env->pmlx.window.width * aa;
	rez.y = env->pmlx.window.height * aa;
	return (vec3_normalize(ray_dir(env, co, rez)));
}

t_v3color		rt_antialiasing(t_env *env, t_vec2i coord, t_ray ray)
{
	const int	antialiasing = env->engine.config.antialiasing;
	const int	a2 = antialiasing * antialiasing;
	t_v3color	ret;
	t_vec2i		c;

	c.y = 0;
	ret = rt_init_v3color(0., 0., 0.);
	while (c.y < antialiasing)
	{
		c.x = 0;
		while (c.x < antialiasing)
		{
			ray.type = k_primary;
			ray.dir = st_ext_raydir(env, coord, antialiasing, c);
			ret = v3color_sum(ret, cast_ray(env, ray, 0));
			++(c.x);
		}
		++(c.y);
	}
	ret = v3color_div(ret, a2);
	ret = v3color_mult(ret, 255.);
	return (ret);
}

void			*st_thread_task(void *arg)
{
	t_pth			*s;
	int				init_x;
	int				cond_x;
	t_vec2i			coord;
	t_v3color		color;

	s = (t_pth*)arg;
	init_x = (((int)s->env->pmlx.window.width / s->nb_thread) * s->i);
	cond_x = (((int)s->env->pmlx.window.width / s->nb_thread) * (s->i + 1));
	coord.y = 0;
	while (coord.y < s->env->pmlx.window.height)
	{
		coord.x = init_x;
		while (coord.x < cond_x)
		{
			color = rt_antialiasing(s->env, coord, s->ray);
			put_v3color_pixel(&(s->env->pmlx.rendering), coord, color);
			++(coord.x);
		}
		++(coord.y);
	}
	ft_memdel(&arg);
	pthread_exit(NULL);
}

static void		st_ext_pthread(t_env *env, t_ray ray)
{
	t_pth		*arg;
	const int	nb_thread = env->engine.config.nb_thread;
	pthread_t	thr[nb_thread];
	int			i;

	i = 0;
	while (i < nb_thread)
	{
		arg = (t_pth*)ft_memalloc(sizeof(t_pth));
		arg->env = env;
		arg->ray = ray;
		arg->nb_thread = nb_thread;
		arg->i = i;
		pthread_create(&thr[i], NULL, &st_thread_task, (void*)arg);
		++(i);
	}
	i = 0;
	while (i < nb_thread)
	{
		pthread_join(thr[i], NULL);
		++i;
	}
}

void			rt_render(t_env *env)
{
	t_ray			ray;
	t_vec2i			load_coord;

	load_coord.x = (int)(0.42 * env->pmlx.window.width);
	load_coord.y = (int)(0.37 * env->pmlx.window.height);
	st_ext_loading(env, load_coord, "Loading ...");
	ray.origin = env->engine.camera.pos;
	st_ext_pthread(env, ray);
}
