/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   row_operations.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/04/13 16:42:42 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

void	row_switching(t_matrix mat, t_uint8 r0, t_uint8 r1)
{
	unsigned	i;
	double		tmp;

	if (r0 != r1)
	{
		i = 0;
		while (i < 4)
		{
			tmp = mat[r0][i];
			mat[r0][i] = mat[r1][i];
			mat[r1][i] = tmp;
			++i;
		}
	}
	else
		ft_putendl("row_switching -> Same row !");
}

void	row_multiplication(t_matrix mat, t_uint8 row, double mult)
{
	int		i;

	i = 0;
	while (i < 4)
	{
		mat[row][i] *= mult;
		++i;
	}
}

void	row_addition(t_matrix mat, t_uint8 row, t_uint8 row2add)
{
	int		i;

	i = 0;
	while (i < 4)
	{
		mat[row][i] += mat[row2add][i];
		++i;
	}
}

void	row_soustraction(t_matrix mat, t_uint8 row, t_uint8 row2sub)
{
	int		i;

	i = 0;
	while (i < 4)
	{
		mat[row][i] -= mat[row2sub][i];
		++i;
	}
}
