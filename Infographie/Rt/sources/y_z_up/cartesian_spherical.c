/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cartesian_spherical.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/04/13 16:42:42 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

double	sin_theta(const t_vec3 v)
{
	return (sqrt(sin_theta2(v)));
}

double	cos_phi(const t_vec3 v)
{
	const double	st = sin_theta(v);

	return ((st) ? ft_clamp2f(v.x / st, -1., 1.) : 1.);
}

double	sin_phi(const t_vec3 v)
{
	const double	st = sin_theta(v);

	return ((st) ? ft_clamp2f(v.y / st, -1., 1.) : 0.);
}

t_vec3	spherical2cartesian(const double theta, const double phi)
{
	t_vec3 vec;

	vec.x = cos(phi) * sin(theta);
	vec.y = sin(phi) * sin(theta);
	vec.z = cos(theta);
	vec.len = 0;
	return (vec);
}

double	theta_from_spherical(const t_vec3 v)
{
	return (acos(ft_clamp2f(v.z, -1., 1.)));
}
