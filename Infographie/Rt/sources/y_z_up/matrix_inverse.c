/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrix_inverse.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/11/30 18:16:04 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static unsigned	st_step_one(t_matrix mat, t_matrix identity, unsigned column)
{
	unsigned	err;
	unsigned	row;
	unsigned	big;
	double		x;
	double		y;

	err = 1;
	if (mat[column][column] == 0)
	{
		big = column;
		row = -1;
		while (++row < 4 && ((x = fabs(mat[row][column])) || !x))
			if (((y = fabs(mat[big][column])) || !y) && x > y)
				big = row;
		if (big == column)
			err = 0;
		else
		{
			row_switching(mat, column, big);
			row_switching(identity, column, big);
		}
	}
	return (err);
}

static void		st_step_two(t_matrix mat, t_matrix identity, unsigned column)
{
	unsigned	j;
	unsigned	row;
	double		coeff;

	row = -1;
	while (++row < 4)
	{
		if (row != column)
		{
			coeff = mat[row][column] / mat[column][column];
			if (coeff)
			{
				j = -1;
				while (++j < 4)
				{
					mat[row][j] -= coeff * mat[column][j];
					identity[row][j] -= coeff * identity[column][j];
				}
				mat[row][column] = 0;
			}
		}
	}
}

void			matrix_inverse(t_matrix mat, t_matrix result)
{
	unsigned	column;
	unsigned	row;
	unsigned	err;
	t_matrix	identity;

	column = -1;
	err = 1;
	init_matrix_identity(identity);
	while (err && ++column < 4)
	{
		if ((err = st_step_one(mat, identity, column)))
			st_step_two(mat, identity, column);
	}
	row = -1;
	if (err)
	{
		while (++row < 4 && !(column = -1))
		{
			while (++column < 4)
				identity[row][column] /= mat[row][row];
		}
		copy_matrix(identity, result);
	}
}
