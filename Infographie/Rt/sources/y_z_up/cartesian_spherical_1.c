/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cartesian_spherical_1.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/04/13 16:42:42 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

t_vec3	cartesian2spherical(t_vec3 cartesian_vec)
{
	t_matrix	mat;
	t_vec3		result;

	init_matrix(mat);
	mat[0][0] = 1;
	mat[2][1] = 1;
	mat[1][2] = 1;
	init_vec3(&result);
	mat_vec3_mult(mat, cartesian_vec, &result);
	return (result);
}

double	theta_from_cartesian(t_vec3 v)
{
	return (acos(v.z));
}

double	phi_from_cartesian(t_vec3 v)
{
	return (atan2(v.y, v.x));
}

double	cos_theta(const t_vec3 v)
{
	return (v.z);
}

double	sin_theta2(const t_vec3 v)
{
	const double	ct = cos_theta(v);

	return (ft_max2f(0., 1. - ct * ct));
}
