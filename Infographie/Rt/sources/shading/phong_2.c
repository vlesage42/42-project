/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   phong_2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aboucher <aboucher@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/02 02:57:02 by aboucher          #+#    #+#             */
/*   Updated: 2016/12/02 02:57:25 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

t_v3color	st_spec(t_light *p_lgts, t_ray ray, t_v3color li, t_info info)
{
	double		a;
	t_vec3		r;
	t_vec3		light_dir;
	t_v3color	spec;

	light_dir = vec3_normalize(vec3_sub(info.intersect, p_lgts->get_pos(p_lgts,
		&info.intersect)));
	r = reflect(light_dir, info.normal);
	if ((a = DOT(r, vec3_mult(ray.dir, -1))) > 0)
		spec = v3color_mult(li, pow(a, info.object->material->ks_exp));
	else
		spec = rt_init_v3color(0., 0., 0.);
	return (spec);
}
