/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fresnel.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/05 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/12/01 09:31:11 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static double	st_ext_kr(double etat, double cosi, double etai, double sint)
{
	double		cost;
	double		rs;
	double		rp;
	double		kr;

	cost = sqrt(ft_max2f(0.f, 1 - sint * sint));
	rs = ((etat * cosi) - (etai * cost)) / ((etat * cosi) + (etai * cost));
	rp = ((etai * cosi) - (etat * cost)) / ((etai * cosi) + (etat * cost));
	kr = (rs * rs + rp * rp) / 2;
	return (kr);
}

double			fresnel(t_vec3 incident, t_vec3 normal, double ior)
{
	double		cosi;
	double		etai;
	double		etat;
	double		sint;
	double		kr;

	cosi = ft_clamp2f(DOT(incident, normal), -1, 1);
	etai = 1;
	etat = ior;
	sint = etai / etat * sqrt(ft_max2f(0.f, 1 - cosi * cosi));
	if (cosi > 0)
		ft_swap2f(&etai, &etat);
	if (sint >= 1)
		kr = 1;
	else
	{
		cosi = fabs(cosi);
		kr = st_ext_kr(etat, cosi, etai, sint);
	}
	return (kr);
}

/*
** As a consequence of the conservation of energy, transmittance is given by:
** kt = 1 - kr;
*/
