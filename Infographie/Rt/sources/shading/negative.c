/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   negative.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/10/23 17:05:37 by vlesage           #+#    #+#             */
/*   Updated: 2016/12/02 19:40:08 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static t_v3color	st_ext_negative(t_env *env, t_ray ray, t_info info,
					t_refract box)
{
	t_ray			r;
	t_v3color		color;
	t_vec3			dir;
	t_vec3			origin;
	const double	r2 = info.object->radius * info.object->radius;

	dir = ray.dir;
	origin = (box.outside == _true) ?
		vec3_sub(info.intersect, box.vbias) : info.intersect;
	while (vec3_square_length(vec3_sub(info.intersect, info.object->position))
	<= r2)
		info.intersect = vec3_sum(info.intersect, box.vbias);
	r.type = k_primary;
	r.origin = info.intersect;
	r.dir = dir;
	color = cast_ray(env, r, box.depth + 1);
	return (color);
}

t_v3color			rt_negative(t_env *env, t_ray ray, t_info info,
					t_uint32 depth)
{
	t_refract	box;

	box.ior = info.object->material->krefraction;
	box.outside = (DOT(ray.dir, info.normal) < 0) ? _true : _false;
	box.vbias = vec3_mult(ray.dir, env->engine.config.bias);
	box.depth = depth;
	return (st_ext_negative(env, ray, info, box));
}
