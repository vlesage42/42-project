/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   refraction.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/05 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/12/01 10:48:45 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

t_vec3				refract(t_vec3 incident, t_vec3 normal, double ior)
{
	double		cos_incident;
	double		etai;
	double		etat;
	double		eta;
	double		k;

	cos_incident = ft_clamp2f(DOT(incident, normal), -1, 1);
	etai = 1;
	etat = ior;
	if (cos_incident < 0)
		cos_incident = -cos_incident;
	else
	{
		ft_swap2f(&etai, &etat);
		normal = vec3_mult(normal, -1);
	}
	eta = etai / etat;
	k = 1 - eta * eta * (1 - cos_incident * cos_incident);
	return ((k < 0) ? rt_init_vec3(0., 0., 0.) : vec3_sum(vec3_mult(incident,
		eta), vec3_mult(normal, (eta * cos_incident - sqrt(k)))));
}

static t_v3color	st_ext_refract(t_env *env, t_ray ray, t_info info,
						t_refract box)
{
	t_v3color	color;
	t_ray		n_ray;

	if (box.kr < 1)
	{
		n_ray.dir = (check_planar_types(info.object)) ? vec3_sub(ray.dir,
			box.vbias) : vec3_normalize(refract(ray.dir, info.normal, box.ior));
		n_ray.origin = (box.outside == _true) ? vec3_sub(info.intersect,
			box.vbias) : info.intersect;
		color = v3color_mult(v3color_product(info.albedo, cast_ray(env, n_ray,
			box.depth + 1)), info.object->material->transparency);
	}
	else
		color = rt_init_v3color(0., 0., 0.);
	return (color);
}

static t_v3color	st_ext_reflect(t_env *env, t_ray ray, t_info info,
						t_refract box)
{
	t_ray	n_ray;

	n_ray.dir = vec3_normalize(reflect(ray.dir, info.normal));
	n_ray.origin = (box.outside == _true) ? vec3_sum(info.intersect,
		box.vbias) : vec3_sub(info.intersect, box.vbias);
	return (v3color_product(info.albedo, cast_ray(env, n_ray, box.depth + 1)));
}

t_v3color			rt_refraction(t_env *env, t_ray ray, t_info info,
						t_uint32 depth)
{
	t_refract	box;
	t_v3color	refract_clr;
	t_v3color	reflect_clr;
	t_v3color	clr;
	t_v3color	ambient;

	refract_clr = rt_init_v3color(0., 0., 0.);
	reflect_clr = rt_init_v3color(0., 0., 0.);
	ambient = v3color_product(env->engine.config.ambient, info.albedo);
	box.ior = info.object->material->krefraction;
	box.kr = fresnel(ray.dir, info.normal, box.ior);
	box.outside = (DOT(ray.dir, info.normal) < 0) ? _true : _false;
	box.vbias = vec3_mult(info.normal, env->engine.config.bias);
	box.depth = depth;
	refract_clr = st_ext_refract(env, ray, info, box);
	reflect_clr = st_ext_reflect(env, ray, info, box);
	clr = v3color_sum(v3color_mult(reflect_clr, box.kr),
		v3color_mult(refract_clr, (1 - box.kr)));
	ambient = v3color_product(ambient, clr);
	return (v3color_sum(ambient, clr));
}
