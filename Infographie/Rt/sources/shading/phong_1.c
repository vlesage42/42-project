/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   phong_1.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/05 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/12/02 03:03:49 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static t_v3color	st_ext_init_color(void)
{
	t_v3color		color;

	color.r = 0.;
	color.g = 0.;
	color.b = 0.;
	return (color);
}

static int			st_ext_if(t_v3color li, t_light *p_lgts, t_ray shadow_ray)
{
	if (li.r > 0 && li.g > 0 && li.b > 0)
	{
		if (p_lgts->type != k_spot || (p_lgts->type == k_spot &&
			DOT(shadow_ray.dir, p_lgts->direction) < cos((p_lgts->angle / 2) *
			RAD1) && DOT(shadow_ray.dir, p_lgts->direction) < 0))
			return (1);
	}
	return (0);
}

static int			st_ext_illuminate(t_env *e, t_ray sr, t_info f)
{
	return (!trace(e->engine.scene.objs, sr, &(f), k_shadow) ||
		f.object->material->type == k_refraction);
}

static t_v3color	st_ext_return(t_env *e, t_info i, t_v3color d, t_v3color s)
{
	return (v3color_sum(v3color_product(e->engine.config.ambient, i.albedo),
		v3color_sum(v3color_mult(d, i.object->material->kdiffuse),
		v3color_mult(s, i.object->material->kspecular))));
}

t_v3color			rt_phong(t_env *e, t_ray r, t_info i, t_light *p)
{
	t_info			f;
	t_ray			sr;
	t_v3color		l;
	t_v3color		d;
	t_v3color		s;

	d = st_ext_init_color();
	s = st_ext_init_color();
	sr.origin = vec3_sum(i.intersect, vec3_mult(i.normal,
		e->engine.config.bias));
	while (p && (sr.type = k_shadow))
	{
		sr.dir = vec3_sub(p->get_pos(p, &sr.origin), sr.origin);
		f.object = i.object;
		f.distance = vec3_square_length(sr.dir);
		sr.dir = vec3_mult(sr.dir, (1 / sqrt(f.distance)));
		l = st_ext_illuminate(e, sr, f) ? p->illuminate(*p, &d, &i) : l;
		if (!trace(e->engine.scene.objs, sr, &(f), k_shadow))
			s = (st_ext_if(l, p, sr)) ? v3color_sum(s, st_spec(p, r, l, i)) : s;
		else if (f.object->material->type == k_refraction)
			d = v3color_normalize(v3color_mult(v3color_product(d, f.albedo),
				f.object->material->transparency));
		p = p->next;
	}
	return (st_ext_return(e, i, d, s));
}
