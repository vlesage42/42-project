/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   reflection.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/05 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/12/01 10:39:55 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

t_vec3		reflect(t_vec3 incident, t_vec3 normal)
{
	return (vec3_sub(incident, vec3_mult(normal, 2 * DOT(incident, normal))));
}

t_v3color	rt_reflection(t_env *env, t_ray ray, t_info info, t_uint32 depth)
{
	t_v3color		color;
	t_vec3			vreflect;
	t_ray			n_ray;

	color.r = 0.;
	color.g = 0.;
	color.b = 0.;
	info.intersect = vec3_sum(info.intersect,
		vec3_mult(info.normal, env->engine.config.bias));
	vreflect = reflect(ray.dir, info.normal);
	n_ray.origin = info.intersect;
	n_ray.dir = vreflect;
	color = v3color_sum(color, v3color_mult(cast_ray(env, n_ray, depth + 1),
		info.object->material->kreflection));
	return (color);
}
