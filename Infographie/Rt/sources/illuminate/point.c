/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   point.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 20:49:56 by vlesage           #+#    #+#             */
/*   Updated: 2016/12/01 06:59:19 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

t_vec3		get_point_pos(t_light *light, t_vec3 *intersect)
{
	intersect = NULL;
	return (light->position);
}

t_vec3		get_point_dir(t_light *light, t_vec3 *intersect, t_uint8 i)
{
	t_vec3	dir;

	dir = vec3_sub(light->position, *intersect);
	dir.len = vec3_square_length(dir);
	return (vec3_mult(vec3_mult(dir, (1 / sqrt(dir.len))), i));
}

/*
** into function get_point_dir
** before dir = vec3_sub(light->position, *intersect);
** dir = vec3_sub(*intersect, light->position);
*/

t_v3color	rt_point(t_light point, t_v3color *color, t_info *info)
{
	double				angle;
	t_vec3				vlight;
	t_v3color			albedo;
	t_v3color			il;
	double				r2;

	vlight = vec3_sub(point.position, info->intersect);
	r2 = vec3_square_length(vlight);
	vlight = vec3_mult(vlight, (1 / sqrt(r2)));
	if ((angle = DOT(info->normal, vlight)) > 0)
	{
		albedo = info->albedo;
		il = v3color_div(v3color_mult(point.color, point.intensity),
		(4. * M_PI * r2));
		*color = v3color_sum(*color, v3color_mult(v3color_product(il, albedo),
		angle));
	}
	else
		il = rt_init_v3color(0, 0, 0);
	return (il);
}

/*
** into function rt_point
** before vlight = vec3_sub(point.position, info->intersect);
** vlight = get_point_dir(&point, &info->intersect, -1);
** r2 = vlight.len;
** after albedo = info->albedo;
** il = (point.color * point.intensity) / (4 * PI * r2);
*/
