/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   directional.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 20:50:08 by vlesage           #+#    #+#             */
/*   Updated: 2016/12/01 06:52:15 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

t_vec3		get_directional_pos(t_light *light, t_vec3 *intersect)
{
	return (vec3_sum(*intersect,
		vec3_mult(vec3_mult(light->direction, -1), 999.)));
}

t_vec3		get_directional_dir(t_light *light, t_vec3 *intersect, t_uint8 i)
{
	intersect = NULL;
	return (vec3_mult(light->direction, i));
}

t_v3color	rt_directional(t_light directional, t_v3color *color, t_info *info)
{
	double				angle;
	t_v3color			c;

	angle = 0.;
	c = info->albedo;
	if ((angle = DOT(info->normal, vec3_mult(directional.direction, -1))) > 0)
		*color = v3color_sum(*color,
					v3color_mult(
						v3color_product(c, directional.color), angle));
	return (v3color_mult(directional.color, directional.intensity));
}
