/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   spot.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/22 20:40:58 by vlesage           #+#    #+#             */
/*   Updated: 2016/12/01 07:14:15 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

t_vec3			get_spot_pos(t_light *light, t_vec3 *intersect)
{
	intersect = NULL;
	return (light->position);
}

t_vec3			get_spot_dir(t_light *light, t_vec3 *intersect, t_uint8 i)
{
	intersect = NULL;
	return (vec3_mult(light->direction, i));
}

static double	st_ext_get_anglefromspotlight(t_light spot, t_info *info)
{
	t_vec3			light_to_point;
	double			dot;

	light_to_point = vec3_sub(info->intersect, spot.position);
	dot = DOT(spot.direction, vec3_normalize(light_to_point));
	return (acos(dot));
}

t_v3color		rt_spot(t_light spot, t_v3color *color, t_info *info)
{
	const double	inner_angle = RAD1 * (0.25 * spot.angle);
	const double	outer_angle = RAD1 * (spot.angle);
	double			anglefromspotlight;
	double			coef;
	t_vec3			vlight;

	vlight = vec3_sub(spot.position, info->intersect);
	vlight.len = vec3_magnitude(vlight);
	vlight = vec3_normalize(vlight);
	anglefromspotlight = st_ext_get_anglefromspotlight(spot, info);
	coef = 0.;
	if (anglefromspotlight < outer_angle)
	{
		if (anglefromspotlight < inner_angle)
			coef = 1.;
		else
			coef = 1 - ((anglefromspotlight - inner_angle) /
			(outer_angle - inner_angle));
	}
	*color = v3color_sum(*color, v3color_mult(
		v3color_product(v3color_div(v3color_mult(spot.color, spot.intensity),
		(4. * M_PI * (vlight.len * vlight.len))), get_tex(info)), coef *
		ft_clamp2f(DOT(info->normal, vlight), 0., 1.)));
	return (v3color_div(v3color_mult(spot.color, spot.intensity), (4. * M_PI *
		(vlight.len * vlight.len))));
}

/*
** into function rt_spot
** t_material * const	ptr = (info->object->material);
*/
