/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acollet <acollet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/10 13:37:59 by acollet           #+#    #+#             */
/*   Updated: 2016/08/10 17:23:37 by acollet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "xmlparser_private.h"

void		freecontent(void *e, size_t size)
{
	(void)e;
	size = 0;
}

void		freenode(void *e, size_t size)
{
	ft_lstdel(&((t_node_priv *)e)->children, &freecontent);
	ft_strdel(&(((t_node_priv *)e)->node->name));
	ft_strdel(&(((t_node_priv *)e)->node->content));
	free(((t_node_priv *)e)->node);
	free((t_node_priv *)e);
	size = 0;
}

void		freetab(char **tab)
{
	int		len;

	len = 0;
	while (tab[len])
	{
		ft_strdel(&tab[len]);
		tab[len] = NULL;
		len++;
	}
	free(tab);
}
