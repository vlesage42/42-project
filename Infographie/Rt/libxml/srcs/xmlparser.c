/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   xmlparser.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acollet <acollet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/20 11:46:51 by acollet           #+#    #+#             */
/*   Updated: 2016/08/17 13:16:31 by acollet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <fcntl.h>
#include "libft.h"
#include "my_gnl.h"
#include "xmlparser_private.h"

static void	st_ext(char *line, char *content, t_app *app)
{
	ft_strdel(&line);
	ft_strdel(&content);
	ft_memdel((void**)&app);
}

t_pxml		*pxml_init(const char *fname)
{
	t_app	*app;
	t_pxml	*pxml;
	char	*line;
	char	*content;

	line = NULL;
	content = NULL;
	pxml = (t_pxml *)ft_memalloc(sizeof(t_pxml));
	if (pxml && (pxml->fd = OP(fname, O_RDONLY)) > 0)
	{
		if ((app = (t_app *)ft_memalloc(sizeof(t_app))))
		{
			while (my_get_next_line(pxml->fd, &line) >= 0 && line != NULL)
			{
				concatline(line, &content);
				ft_strdel(&line);
			}
			xmlparser(pxml, app, content);
			st_ext(line, content, app);
		}
		CL(pxml->fd);
	}
	else if (pxml)
		ft_memdel((void**)&pxml);
	return (pxml);
}

void		pxml_close(t_pxml **pxml)
{
	ft_lstdel(((t_list**)&((*pxml)->nodes)), &freenode);
	ft_memdel((void**)pxml);
}
