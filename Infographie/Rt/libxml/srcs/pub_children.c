/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pub_children.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acollet <acollet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/10 13:11:04 by acollet           #+#    #+#             */
/*   Updated: 2016/08/15 18:30:00 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "xmlparser_private.h"

int					pxml_countchildren(t_pxml *pxml, const t_pxml_node *e)
{
	t_node_priv		*tmp;

	if ((tmp = getelementbypnode(pxml, e)) == NULL)
		return (0);
	return (tmp->count_children);
}

const t_pxml_node	*pxml_child(t_pxml *pxml, const t_pxml_node *e, t_uint32 i)
{
	t_pxml_node	*node;
	t_node_priv	*tmp;
	t_list		*lst;

	node = NULL;
	if ((tmp = getelementbypnode(pxml, e)) &&
			tmp->count_children >= 1 &&
			tmp->count_children >= (int)i && (lst = tmp->children))
	{
		while (lst && !node)
		{
			if (i <= 0)
				node = ((t_node_priv *)lst->content)->node;
			else
			{
				lst = lst->next;
				i--;
			}
		}
	}
	return (node);
}

const t_pxml_node	*pxml_children(t_pxml *pxml, const t_pxml_node *e)
{
	return (pxml_child(pxml, e, 0));
}
