/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pub_path.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acollet <acollet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/05 12:41:18 by acollet           #+#    #+#             */
/*   Updated: 2016/08/18 15:42:51 by acollet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "libft.h"
#include "xmlparser_private.h"

const t_pxml_node	*pxml_root(t_pxml *pxml)
{
	return ((pxml->root) ?
		(*((t_node_priv **)pxml->root))->node : NULL);
}

char				*splitname(const char *str, int *cpt, int *i)
{
	char	**tab;
	char	*ret;
	int		len;

	ret = NULL;
	tab = ft_strsplit(str, '|');
	len = 0;
	*i = 0;
	while (tab[len] != NULL)
		len++;
	*cpt = 0;
	if (len > 2)
		return (NULL);
	else if (len == 1)
	{
		ret = ft_strdup(tab[0]);
		freetab(tab);
		return (ret);
	}
	*cpt = ft_atoi(tab[1]);
	ret = ft_strdup(tab[0]);
	freetab(tab);
	return (ret);
}

t_pxml_node			*pxml_getchild(t_pxml *pxml,
								t_pxml_node *e, const char *name)
{
	t_node_priv		*tmp;
	t_list			*lst;
	char			*newname;
	int				i;
	int				cpt;

	if ((tmp = getelementbypnode(pxml, e)) == NULL || tmp->count_children < 1)
		return (NULL);
	lst = tmp->children;
	newname = splitname(name, &cpt, &i);
	while (lst)
	{
		if (ft_strcmp(((t_node_priv *)lst->content)->node->name, newname) == 0)
		{
			if (i == cpt)
			{
				ft_strdel(&newname);
				return (((t_node_priv *)lst->content)->node);
			}
			i++;
		}
		lst = lst->next;
	}
	ft_strdel(&newname);
	return (NULL);
}

const t_pxml_node	*pxml_solve(t_pxml *pxml, const char *path)
{
	t_pxml_node		*root;
	char			**tab;
	t_uint32		len;
	t_uint32		i;

	tab = ft_strsplit(path, '/');
	len = 0;
	while (tab[len] != NULL)
		len++;
	root = (t_pxml_node *)pxml_root(pxml);
	if ((root && len && !ft_strcmp(root->name, tab[0])) || (root = NULL))
	{
		if (len > 1 && (i = 1))
		{
			while (i < len)
			{
				root = pxml_getchild(pxml, root, tab[i++]);
				if (root == NULL)
					i = len;
			}
			freetab(tab);
		}
	}
	return (root);
}

const t_pxml_node	*pxml_relsolve(
	t_pxml *px, const t_pxml_node *f, const char *path)
{
	t_pxml_node			*root;
	char				**tab;
	int					len;
	int					i;

	tab = ft_strsplit(path, '/');
	len = 0;
	while (tab[len] != NULL)
		len++;
	root = (t_pxml_node *)f;
	if (root == NULL || len == 0 || ft_strcmp(root->name, tab[0]) != 0)
		return (NULL);
	if (len == 1)
		return (root);
	i = 1;
	while (i < len)
	{
		root = pxml_getchild(px, root, tab[i++]);
		if (root == NULL)
			i = len;
	}
	freetab(tab);
	return (root);
}
