/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_elements.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acollet <acollet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/09 17:30:53 by acollet           #+#    #+#             */
/*   Updated: 2016/08/10 13:55:55 by acollet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "xmlparser_private.h"

t_pxml_node	*create_node(int uid, char *name, char *content)
{
	t_pxml_node	*node;

	node = (t_pxml_node*)malloc(sizeof(t_pxml_node));
	if (node)
	{
		node->uid = uid;
		node->name = ft_strdup(name);
		node->content = ft_strdup(content);
		ft_strdel(&name);
		ft_strdel(&content);
	}
	return (node);
}

t_node_priv	*create_element(t_app *app, t_pxml_node *node)
{
	t_node_priv	*e;

	e = (t_node_priv *)malloc(sizeof(t_node_priv));
	if (e)
	{
		e->node = node;
		e->parent = app->currentnode;
		e->count_children = 0;
		e->depths = app->currentdepth;
		e->children = NULL;
	}
	return (e);
}

void		free_node(t_pxml_node *node)
{
	ft_strdel(&(node->name));
	ft_strdel(&(node->content));
	free(node);
	node = NULL;
}

void		free_element(t_node_priv *e)
{
	free(e);
	e = NULL;
}
