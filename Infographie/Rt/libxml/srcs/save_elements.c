/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   save_elements.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acollet <acollet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/02 16:12:01 by acollet           #+#    #+#             */
/*   Updated: 2016/08/10 17:48:46 by acollet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "xmlparser_private.h"

static t_list	*new_list(t_node_priv *node)
{
	t_list	*e;

	if ((e = (t_list *)malloc(sizeof(t_list))))
	{
		e->content = node;
		e->content_size = sizeof(node);
		e->next = NULL;
		e->prev = NULL;
	}
	return (e);
}

static void		addchild(t_node_priv **parent, t_node_priv *child)
{
	(*parent)->count_children++;
	ft_lstaddqueue(&((*parent)->children), new_list(child));
}

static void		save_e(t_pxml *pxml, t_app *app, t_node_priv *e)
{
	if (app->currentnode != NULL)
		addchild(((t_node_priv **)app->currentnode), e);
	ft_lstadd(((t_list **)&(pxml->nodes)), new_list(e));
	app->currentnode = &((t_list *)pxml->nodes)->content;
	if (app->currentdepth == INITDEPTH && pxml->root == NULL)
		pxml->root = app->currentnode;
	else if (app->currentdepth == INITDEPTH)
		;
	app->currentdepth++;
}

void			new_e(t_pxml *pxml, t_app *app, char *n, char *c)
{
	t_node_priv	*e;
	t_pxml_node	*node;

	if ((node = create_node(app->max_uid++, n, c)))
	{
		if ((e = create_element(app, node)))
			save_e(pxml, app, e);
	}
}

void			close_e(t_pxml *pxml, t_app *app, char *n)
{
	app->currentnode = (*((t_node_priv **)app->currentnode))->parent;
	(void)pxml;
	app->currentdepth--;
	ft_strdel(&n);
}
