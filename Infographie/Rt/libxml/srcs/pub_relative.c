/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pub_relative.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acollet <acollet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/05 12:41:15 by acollet           #+#    #+#             */
/*   Updated: 2016/08/10 16:37:01 by acollet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "xmlparser_private.h"

t_node_priv			*getelementbyid(t_pxml *pxml, int id)
{
	t_list	*tmp;

	tmp = ((t_list *)pxml->nodes);
	while (tmp)
	{
		if (((t_node_priv *)tmp->content)->node->uid == id)
			return ((t_node_priv *)tmp->content);
		tmp = tmp->next;
	}
	return (NULL);
}

t_node_priv			*getelementbypnode(t_pxml *pxml, const t_pxml_node *e)
{
	t_node_priv		*tmp;

	if (e == NULL)
		return (NULL);
	tmp = getelementbyid(pxml, e->uid);
	if (tmp == NULL)
		return (NULL);
	return (tmp);
}

const t_pxml_node	*pxml_parent(t_pxml *pxml, const t_pxml_node *e)
{
	t_node_priv		*tmp;

	if ((tmp = getelementbypnode(pxml, e)) == NULL || tmp->parent == NULL)
		return (NULL);
	return ((*((t_node_priv **)tmp->parent))->node);
}

const t_pxml_node	*pxml_next(t_pxml *pxml, const t_pxml_node *e)
{
	t_node_priv		*tmp;
	t_list			*lst;
	int				depth;

	if ((tmp = getelementbypnode(pxml, e)) == NULL || tmp->parent == NULL)
		return (NULL);
	depth = tmp->depths;
	tmp = (*((t_node_priv **)tmp->parent));
	lst = tmp->children;
	while (lst)
	{
		if (((t_node_priv *)lst->content)->node->uid == e->uid && lst->next)
			return (((t_node_priv *)lst->next->content)->node);
		lst = lst->next;
	}
	return (NULL);
}

const t_pxml_node	*pxml_prev(t_pxml *pxml, const t_pxml_node *e)
{
	t_node_priv		*tmp;
	t_list			*lst;
	int				depth;

	if ((tmp = getelementbypnode(pxml, e)) == NULL || tmp->parent == NULL)
		return (NULL);
	depth = tmp->depths;
	tmp = (*((t_node_priv **)tmp->parent));
	lst = tmp->children;
	while (lst)
	{
		if (((t_node_priv *)lst->content)->node->uid == e->uid && lst->prev)
			return (((t_node_priv *)lst->prev->content)->node);
		lst = lst->next;
	}
	return (NULL);
}
