/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   reader.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acollet <acollet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/20 18:56:53 by acollet           #+#    #+#             */
/*   Updated: 2016/08/18 15:55:31 by acollet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "libft.h"
#include "xmlparser_private.h"

static int	isclosingtag(char **str)
{
	return (((**str) == '/') ? XTRUE : XFALSE);
}

static char	*getname(char **str, char *chevron)
{
	int		i;
	char	*ret;

	i = 0;
	ret = *str;
	while (str && *str && **str && *str != chevron)
	{
		++(*str);
		++i;
	}
	ret = ft_strndup(ret, i);
	return (ret);
}

static char	*getcontent(char **str)
{
	char	*chevron;
	char	*ret;

	ret = NULL;
	++(*str);
	chevron = ft_strchr(*str, '<');
	while (**str && ft_isblank(**str))
		++(*str);
	if (*str != chevron)
	{
		ret = ft_strndup(*str, chevron - *str);
	}
	while (**str && **str != '<')
		(*str)++;
	return (ret);
}

void		xmlparser(t_pxml *pxml, t_app *app, const char *str)
{
	char	*name;
	char	*chevron;
	char	*tmp;

	tmp = (char*)str;
	while (str && tmp && *tmp)
	{
		if (*tmp == '<' || ((++tmp) && !(tmp) && tmp))
		{
			if (!ft_strncmp(tmp, "<!--", 4))
			{
				while (tmp && ft_strncmp(tmp, "-->", 3))
					++tmp;
				tmp += 3;
			}
			else if ((chevron = ft_strchr(tmp, '>')) && (++tmp)
				&& (name = getname(&tmp, chevron)))
			{
				if (!isclosingtag(&name))
					new_e(pxml, app, name, getcontent(&tmp));
				else
					close_e(pxml, app, name);
			}
		}
	}
}

void		concatline(const char *line, char **content)
{
	static int	index = 0;
	char		*str;

	if (ft_strlen(line) == 0 || ft_isstrblank(line) == 1)
		return ;
	str = ft_strtrim(line);
	ft_strconcat(content, str);
	index++;
	ft_strdel(&str);
}
