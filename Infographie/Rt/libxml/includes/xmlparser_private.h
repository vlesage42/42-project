/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   xmlparser_private.h                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acollet <acollet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/20 14:32:34 by acollet           #+#    #+#             */
/*   Updated: 2016/08/10 17:22:04 by acollet          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef XMLPARSER_PRIVATE_H
# define XMLPARSER_PRIVATE_H
# define XTRUE 1
# define XFALSE 0
# define INITDEPTH 0
# include "xmlparser.h"

typedef struct	s_node_priv
{
	t_pxml_node			*node;
	void				*parent;
	int					count_children;
	int					depths;
	t_list				*children;
}				t_node_priv;

typedef enum	e_tagstatus
{
	E_TAGOPEN,
	E_TAGCLOSE,
	E_ENDTAGOPEN,
	E_ENDTAGCLOSE
}				t_tagstatus;

typedef struct	s_app
{
	void		*currentnode;
	int			currentdepth;
	int			max_uid;
	t_node_priv	*e;
}				t_app;

void			concatline(const char *line, char **content);
void			xmlparser(t_pxml *pxml, t_app *app, const char *str);

t_pxml_node		*create_node(int uid, char *name, char *content);
t_node_priv		*create_element(t_app *app, t_pxml_node *node);

void			free_node(t_pxml_node *node);
void			free_element(t_node_priv *e);
void			freecontent(void *e, size_t size);
void			freenode(void *e, size_t size);
void			freetab(char **tab);

void			new_e(t_pxml *pxml, t_app *app, char *n, char *c);
void			close_e(t_pxml *pxml, t_app *app, char *n);

t_node_priv		*getelementbyid(t_pxml *pxml, int id);
t_node_priv		*getelementbypnode(t_pxml *pxml, const t_pxml_node *e);

#endif
