/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   xmlparser.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: acollet <acollet@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/19 18:20:19 by acollet           #+#    #+#             */
/*   Updated: 2016/12/01 11:29:46 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef XMLPARSER_H
# define XMLPARSER_H

typedef	enum		e_pxml_errors
{
	E_XML_BADFILENAME = 1,
	E_XML_NOTWELLFORMATTED
}					t_pxml_errors;

typedef struct		s_pxml_node
{
	int		uid;
	char	*name;
	char	*content;
}					t_pxml_node;

typedef struct		s_pxml
{
	int				fd;
	void			*root;
	void			*nodes;
	t_pxml_errors	*errors;
}					t_pxml;

t_pxml				*pxml_init(const char *fname);
void				pxml_close(t_pxml **pxml);

const t_pxml_node	*pxml_root(t_pxml *pxml);
const t_pxml_node	*pxml_parent(t_pxml *pxml, const t_pxml_node *e);

int					pxml_countchildren(t_pxml *pxml, const t_pxml_node *e);
const t_pxml_node	*pxml_children(t_pxml *pxml, const t_pxml_node *e);
const t_pxml_node	*pxml_child(t_pxml *pxml, const t_pxml_node *e, t_uint32 i);
t_pxml_node			*pxml_getchild(t_pxml *pxml, t_pxml_node *e,
						const char *name);

const t_pxml_node	*pxml_next(t_pxml *pxml, const t_pxml_node *e);
const t_pxml_node	*pxml_prev(t_pxml *pxml, const t_pxml_node *e);

const t_pxml_node	*pxml_solve(t_pxml *pxml, const char *path);
void				pxml_print(t_pxml *xml, const t_pxml_node *node, int tab);
void				pxml_node_print(const t_pxml_node *node);
const t_pxml_node	*pxml_relsolve(
						t_pxml *px, const t_pxml_node *f, const char *path);

#endif
