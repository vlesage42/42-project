/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   random.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/09/22 21:20:29 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RANDOM_H
# define RANDOM_H

# include "shapes.h"

void		load_png(t_image *png, const char *file);
void		save_png(t_window *win, t_image *png, const char *file);

void		put_string_to_image(t_image *img, t_text text);
void		put_image_to_image(t_image *img0, t_vec2i coord, t_image img1);

t_v3color	get_tex(t_info *info);
void		transpose(t_matrix mat, t_matrix transp);

t_vec3		rt_init_vec3(double x, double y, double z);
t_v3color	rt_init_v3color(double r, double g, double b);

t_text		rt_init_text(t_vec2i coord, t_v3color color, const char *msg,
						t_uint8 police_size);
t_vec2i		rt_init_coord(int x, int y);

void		check_vector(t_vec3 *v);
int			check_planar_types(t_object *o);
void		check_data_index(const t_image *img, int *i);

t_vec3		world_to_local(t_info info);

#endif
