/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rt_types.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/12/01 00:47:33 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RT_TYPES_H
# define RT_TYPES_H

# include "textures.h"

typedef struct	s_equa_2nd
{
	double	a;
	double	b;
	double	c;
	double	x[2];
	double	t;
	double	det;
}				t_equa_2nd;

typedef struct	s_equa_shape
{
	int		err;
	t_vec3	x;
	double	dv;
	double	xv;
	double	k;
	double	n[2];
	double	m;
}				t_equa_shape;

enum			e_keys
{
	key_w = 13,
	key_a = 0,
	key_s = 1,
	key_d = 2,
	key_h = 4,
	key_i = 34,
	key_x = 7,
	key_y = 16,
	key_z = 6,
	key_f5 = 96,
	key_escape = 53,
	key_return = 36,
	keypad_enter = 76,
	key_delete = 117,
	key_back = 51,
	key_space = 49,
	key_div = 75,
	key_mult = 67,
	key_minus = 78,
	key_plus = 69,
	key_left = 123,
	key_right = 124,
	key_down = 125,
	key_up = 126,
	key_1 = 18,
	key_2 = 19,
	key_3 = 20,
	keypad_0 = 82,
	keypad_1 = 83,
	keypad_2 = 84,
	keypad_3 = 85,
	keypad_4 = 86,
	keypad_5 = 87,
	keypad_6 = 88,
	keypad_7 = 89,
	keypad_8 = 91,
	keypad_9 = 92,
	keypad_point = 65
};

typedef enum	e_ray_type
{
	k_primary = 1,
	k_shadow
}				t_ray_type;

typedef struct	s_ray
{
	t_ray_type	type;
	t_vec3		origin;
	t_vec3		dir;
}				t_ray;

typedef struct	s_viewplane
{
	double		width;
	double		height;
	double		distance;
	t_vec3		upleft;
}				t_viewplane;

typedef struct	s_camera
{
	t_vec3		pos;
	t_vec3		dir;
	t_vec3		up;
	t_vec3		right;
	t_vec3		look_at;
	t_viewplane	vp;
}				t_camera;

typedef struct	s_config
{
	char		*file;
	t_v3color	ambient;
	t_uint32	antialiasing;
	t_uint32	nb_thread;
	t_uint32	ray_depth;
	double		bias;
	char		*images_dir;
	char		*materials_dir;
	char		*textures_dir;
	char		*composed_dir;
}				t_config;

typedef struct	s_engine
{
	t_config			config;
	t_camera			camera;
	t_material			*materials;
	struct s_texture	*textures;
	t_scene				scene;
}				t_engine;

typedef struct	s_mlx
{
	void		*handler;
	t_window	window;
	t_image		interface;
	t_image		rendering;
}				t_mlx;

typedef struct	s_env
{
	t_mlx		pmlx;
	t_engine	engine;
	t_ui		ui;
}				t_env;

typedef struct	s_pth
{
	t_env	*env;
	t_ray	ray;
	int		nb_thread;
	int		i;
}				t_pth;

#endif
