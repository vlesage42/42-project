/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   textures.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/09/22 21:35:13 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TEXTURES_H
# define TEXTURES_H

# include "rt_scene_types.h"

typedef enum	e_tex_mode
{
	k_null,
	k_uv_mapping,
	k_procedural
}				t_tex_mode;

typedef enum	e_pattern
{
	k_image = 1,
	k_sinus,
	k_lines,
	k_checkerboard
}				t_pattern;

typedef struct	s_texture
{
	char				*name;
	t_tex_mode			mode;
	t_pattern			pattern;
	t_image				*image;
	double				blend;
	double				scale;
	double				angle;
	struct s_texture	*next;
}				t_texture;

t_v3color		tx_planar(struct s_info info);
t_v3color		tx_spherical(struct s_info info);
t_v3color		tx_cylindrical(struct s_info info);

double			procedural_sinus(struct s_info info);
double			procedural_lines(struct s_info info);
double			procedural_checkerboard(struct s_info info);

#endif
