/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rt_parser.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/12/01 06:41:43 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RT_PARSER_H
# define RT_PARSER_H

# include "shading.h"

int			rt_parser(t_env *env);
int			parser_config(t_env *env, t_pxml *xml, t_pxml_node *node);
int			parser_camera(t_camera *cam, t_pxml *xml, t_pxml_node *node);
int			parser_scene(t_engine *engine, t_pxml *xml, t_pxml_node *node);
int			parser_objects(t_engine *engine, t_pxml *xml, t_pxml_node *node);
int			parser_lights(t_light **lights, t_pxml *xml, t_pxml_node *node);

void		rt_delete_objects(t_object *o);
void		rt_delete_lights(t_light *l);
void		rt_delete_materials(t_material *m);
void		rt_delete_textures(t_texture *tex);

t_object	*rt_new_object(t_engine *egn, t_pxml *xml, t_pxml_node *node);

void		get_vertices(t_object **obj, t_pxml *xml, t_pxml_node *node);
void		rotate_vertices(t_object **obj, t_matrix mrotate);

void		axis_rotation(t_object **o_ptr, t_pxml *xml, t_pxml_node *n_ptr);
void		get_square_triangle_normal(t_object **o);
void		get_square_triangle_edges(t_object **o);

t_vec3		ft_get_vec3(char *b);
t_v3color	ft_get_v3color(char *b);
void		ft_put_v3color(t_v3color color);

t_uint8		open_xml(t_pxml **pxml, t_pxml_node **root, const char *file);

void		parser_conposed(t_object **o, t_engine *engine,
				t_pxml *xml, t_pxml_node *child);
void		rotate_composed(t_object **obj);

int			get_mat_tex(t_object **o, t_engine *egn,
				t_pxml *xml, t_pxml_node *node);

int			parser_materials(t_config *cfg, t_object **o_ptr,
				t_material *mat, char *content);
void		st_get_texture(t_config *cfg, t_texture **tex, char *cnt);
int			parser_textures(t_config *cfg, t_object **o_ptr,
				t_texture *tex, char *content);

#endif
