/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shading.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/12/02 02:59:32 by aboucher         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHADING_H
# define SHADING_H

# include "rt_types.h"

typedef struct	s_refract
{
	double		kr;
	double		ior;
	t_vec3		vbias;
	t_bool		outside;
	t_uint32	depth;
}				t_refract;

/*
** fresnel.cpp
*/
double			fresnel(t_vec3 incident, t_vec3 normal, double ior);

/*
** phong.cpp
*/
t_v3color		st_spec(t_light *p_lgts, t_ray ray, t_v3color li, t_info info);
t_v3color		rt_phong(t_env *env, t_ray ray, t_info info, t_light *p_lgts);

/*
** reflection.cpp
*/
t_v3color		rt_reflection(
					t_env *env, t_ray ray, t_info info, t_uint32 depth);
t_vec3			reflect(t_vec3 incident, t_vec3 normal);

/*
** refraction.cpp
*/
t_v3color		rt_refraction(
					t_env *env, t_ray ray, t_info info, t_uint32 depth);
t_vec3			refract(t_vec3 incident, t_vec3 normal, double ior);

/*
** negative.cpp
*/
t_v3color		rt_negative(t_env *env, t_ray ray, t_info info, t_uint32 depth);

#endif
