/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   illuminate.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/09/22 21:20:29 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ILLUMINATE_H
# define ILLUMINATE_H

# include "rt_ui.h"

/*
** point.cpp
*/
t_v3color	rt_point(t_light point, t_v3color *color, t_info *info);
t_vec3		get_point_pos(t_light *light, t_vec3 *intersect);
t_vec3		get_point_dir(t_light *light, t_vec3 *intersect, t_uint8 i);

/*
** directional.cpp
*/
t_v3color	rt_directional(t_light directional, t_v3color *color, t_info *info);
t_vec3		get_directional_pos(t_light *light, t_vec3 *intersect);
t_vec3		get_directional_dir(t_light *light, t_vec3 *intersect, t_uint8 i);

/*
** directional.cpp
*/
t_v3color	rt_spot(t_light spot, t_v3color *color, t_info *info);
t_vec3		get_spot_pos(t_light *light, t_vec3 *intersect);
t_vec3		get_spot_dir(t_light *light, t_vec3 *intersect, t_uint8 i);

#endif
