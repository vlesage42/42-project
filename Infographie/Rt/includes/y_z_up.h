/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   y_z_up.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/04/13 16:42:42 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef Y_Z_UP_H
# define Y_Z_UP_H

# include "rt_parser.h"

void	matrix_inverse(t_matrix mat, t_matrix result);

/*
** Row elementary operations.
*/
void	row_switching(t_matrix mat, t_uint8 r0, t_uint8 r1);
void	row_multiplication(t_matrix mat, t_uint8 row, double mult);
void	row_addition(t_matrix mat, t_uint8 row, t_uint8 row2add);
void	row_soustraction(t_matrix mat, t_uint8 row, t_uint8 row2sub);

/*
** Cartesian & Spherical Coordinate System
*/
t_vec3	cartesian2spherical(t_vec3 cartesian_vec);
double	theta_from_cartesian(t_vec3 v);
double	phi_from_cartesian(t_vec3 v);
double	cos_theta(const t_vec3 v);
double	sin_theta2(const t_vec3 v);
double	sin_theta(const t_vec3 v);
double	cos_phi(const t_vec3 v);
double	sin_phi(const t_vec3 v);
t_vec3	spherical2cartesian(const double theta, const double phi);
double	theta_from_spherical(const t_vec3 v);
double	phi_from_spherical(const t_vec3 v);

#endif
