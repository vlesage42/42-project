/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rt_ui.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/04/13 16:42:42 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RT_UI_H
# define RT_UI_H

# include "y_z_up.h"

# define RMASK	0x000000ff
# define GMASK	0x0000ff00
# define BMASK	0x00ff0000
# define AMASK	0xff000000

/*
** ui_init.c
*/
void	rt_init_ui(t_ui *ui, t_window win, t_config *cfg);

/*
** ui_delete.c
*/
void	rt_delete_ui(t_ui *ui);

/*
** ui_home.c
*/
void	rt_ui_home(t_image *screen, t_home *home, char *path);
void	rt_ui_home_click(t_env *env, const t_vec2i coord);

/*
** ui_multi.c
*/
void	rt_ui_multi(t_image *screen, t_multi *multi, char *path);
void	rt_ui_home_keyup(t_env *env, int keycode);
void	rt_ui_multi_click(t_env *env, const t_vec2i coord);

/*
** ui_render.c
*/
void	rt_ui_render(t_env *env, t_render *render);

/*
** ui_keypad.c
*/
void	rt_init_keypad(t_config *cfg, t_multi *multi, t_vec2i coord);
void	rt_ui_keypad(t_image *screen, t_multi *multi);

/*
** ui_events.c
*/
void	ui_keyboard(t_env *env, int keycode);

/*
** ui_button.c
*/
int		check_btn_click(t_button btn, t_vec2i coord);
void	set_button(t_button *btn, const char *file, t_text txt);
t_uint8	check_radio_btn(t_button *radio, t_vec2i coord, char **path);

/*
** ui_loading.c
*/
void	ui_loading(t_env *env);
void	st_ext_loading(t_env *env, t_vec2i coord, const char *msg);

#endif
