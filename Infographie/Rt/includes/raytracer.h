/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   raytracer.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/12/01 07:19:59 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RAYTRACER_H
# define RAYTRACER_H

# include <OpenGL/gl.h>
# include "SDL2/SDL.h"
# include "SDL2/SDL_ttf.h"
# include "SDL2/SDL_image.h"
# include "mlx.h"
# include "random.h"

# define HIT	1
# define MISS	0

# define ZERO	1e-6

# define RAD1	(M_PI / 180.)

# define FONT_PATH "/Library/Fonts/Arial Black.ttf"

/*
** main.cpp
*/
void		init_rt(t_mlx *ptr);
void		delete_rt(t_env *env);
int			init_config(t_env *env, char *scene_file);

/*
** camera.cpp
*/
void		rt_init_cam(t_env *env);
t_vec3		ray_dir(t_env *env, t_vec2i coord, t_vec2 rez);

/*
** shadow.c
*/
void		rt_shadow(t_object *o, t_ray ray, t_info *inf);
void		st_check_dist(t_object *obj, t_ray ray, t_info *inf, t_info *tmp);
/*
** events.cpp ~ rt_events.cpp
*/
int			rt_mouse_up(int btn, int x, int y, void *param);
int			rt_key_up(int keycode, void *param);
int			rt_expose(void *param);
void		render_keyboard(t_env *env, int keycode);

/*
** materials.cpp
*/
void		rt_set_material(t_env *env, t_object *obj, char *name);

/*
** render.cpp
*/
t_v3color	rt_antialiasing(t_env *env, t_vec2i coord, t_ray ray);
void		rt_render(t_env *env);

/*
** raytracer.cpp
*/
void		init_info(t_info *inf);
int			trace(t_object *p_objs, t_ray ray, t_info *inf, t_ray_type r);
t_v3color	cast_ray(t_env *env, t_ray ray, t_uint32 depth);

/*
** second_degree.cpp
*/
void		st_equation_2nd(t_equa_2nd *equa, t_equa_shape *sh, t_object obj);
int			st_limitation(int mode, double height, double n);

#endif
