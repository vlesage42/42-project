/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shapes.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/09/22 21:20:29 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SHAPES_H
# define SHAPES_H

# include "illuminate.h"

int		rt_sphere_intersects(const t_ray ray, t_object sphere, t_info *tmp);
int		rt_plane_intersects(const t_ray ray, t_object plane, t_info *tmp);

int		rt_cylinder_intersects(const t_ray ray, t_object cylinder, t_info *tmp);
int		rt_cone_intersects(const t_ray ray, t_object cone, t_info *tmp);

int		rt_triangle_intersects(const t_ray ray, t_object triangle, t_info *tmp);
int		rt_square_intersects(const t_ray ray, t_object square, t_info *tmp);
int		rt_disk_intersects(const t_ray ray, t_object disk, t_info *tmp);

void	p_n(t_equa_2nd *equa, t_equa_shape *sh, const t_ray *ray, t_object *p);
int		rt_paraboloid_intersects(const t_ray ray, t_object para, t_info *tmp);

#endif
