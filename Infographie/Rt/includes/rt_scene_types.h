/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rt_scene_types.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/10/22 17:21:20 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RT_SCENE_TYPES_H
# define RT_SCENE_TYPES_H

# include "rt_ui_types.h"

struct s_info;
struct s_light;
struct s_ray;
struct s_object;

typedef enum	e_material_type
{
	k_phong = 1,
	k_reflection,
	k_refraction,
	k_negative
}				t_material_type;

typedef struct	s_material
{
	t_material_type		type;
	char				*name;
	t_v3color			albedo;
	double				transparency;
	double				kdiffuse;
	double				kspecular;
	double				ks_exp;
	double				kreflection;
	double				krefraction;
	struct s_material	*next;
}				t_material;

typedef int(*t_intersect)(
	struct s_ray ray, struct s_object obj, struct s_info *inf);

typedef struct	s_v4_vertices
{
	t_vec3	v0;
	t_vec3	v1;
	t_vec3	v2;
	t_vec3	v3;
}				t_v4_vertices;

typedef enum	e_object_type
{
	k_plane = 1,
	k_triangle,
	k_square,
	k_disk,
	k_sphere,
	k_cone,
	k_cylinder,
	k_paraboloid
}				t_object_type;

typedef enum	e_mode
{
	k_infinity = 1,
	k_center,
	k_offset
}				t_mode;

typedef struct	s_composed
{
	t_vec3			pos;
	t_matrix		inv;
}				t_composed;

typedef double(*t_func_procedural)(struct s_info);
typedef t_v3color(*t_func_uv_mapping)(struct s_info);

typedef struct	s_object
{
	t_object_type		type;
	t_vec3				position;
	t_composed			*comp;
	t_intersect			intersect;
	t_material			*material;
	struct s_texture	*texture;
	t_func_procedural	proc;
	t_func_uv_mapping	uv_m;
	t_mode				mode;
	t_vec3				axis;
	t_vec3				rotation;
	t_matrix			inverse;
	t_vec3				normal;
	t_v4_vertices		vertices;
	t_vec3				edges[4];
	double				kernel;
	double				radius;
	double				angle;
	double				height;
	struct s_object		*next;
}				t_object;

typedef struct	s_info
{
	t_vec3		intersect;
	t_vec3		normal;
	double		distance;
	t_object	*object;
	t_v3color	albedo;
}				t_info;

typedef t_v3color(*t_illuminate)(
	struct s_light light, t_v3color *color, t_info *info);

typedef t_vec3(*t_get_light_pos)(
	struct s_light *light, t_vec3 *intersect);

typedef t_vec3(*t_get_light_dir)(
	struct s_light *light, t_vec3 *intersect, t_uint8 i);

typedef enum	e_light_type
{
	k_point = 1,
	k_spot,
	k_directional
}				t_light_type;

typedef struct	s_light
{
	t_light_type	type;
	t_vec3			position;
	t_vec3			direction;
	double			angle;
	double			intensity;
	t_v3color		color;
	t_illuminate	illuminate;
	t_get_light_pos	get_pos;
	t_get_light_dir	get_dir;
	struct s_light	*next;
}				t_light;

typedef struct	s_scene
{
	t_object	*objs;
	t_light		*lights;
}				t_scene;

#endif
