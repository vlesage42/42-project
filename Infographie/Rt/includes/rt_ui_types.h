/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rt_ui_types.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abartz <abartz@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/13 16:42:42 by abartz            #+#    #+#             */
/*   Updated: 2016/04/13 16:42:42 by abartz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RT_UI_TYPES_H
# define RT_UI_TYPES_H

# include "libft.h"
# include "ft_math.h"
# include "infographics.h"
# include "get_next_line.h"
# include "xmlparser.h"

typedef struct	s_text
{
	t_vec2i		coord;
	t_v3color	color;
	char		*str;
	t_uint8		police_size;
}				t_text;

typedef struct	s_button
{
	t_vec2i	coord;
	t_image	img;
}				t_button;

typedef struct	s_home
{
	t_button	btn_solo;
	t_button	btn_multi;
	t_button	btn_options;
}				t_home;

typedef struct	s_multi
{
	t_button	back[2];
	t_button	radio[3];
	t_button	kp[11];
	t_button	client[3];
	t_button	server[3];
}				t_multi;

typedef struct	s_render
{
	t_button	btn_help;
}				t_render;

typedef struct	s_ui
{
	char		*path;
	t_home		home;
	t_multi		multi;
	t_render	render;
}				t_ui;

#endif
