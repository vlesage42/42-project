/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mandelbar.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/26 20:13:50 by vlesage           #+#    #+#             */
/*   Updated: 2016/03/24 21:16:10 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		ft_mandelbar(t_env *e)
{
	e->x = 0;
	while (e->x < e->width)
	{
		e->y = 0;
		while (e->y < e->height)
		{
			ft_mandel(e);
			while ((e->z_r * e->z_r) - (e->z_i * e->z_i) < 4 &&
			e->i < e->ite_max)
			{
				e->tmp = e->z_r;
				e->z_r = e->z_r * e->z_r - e->z_i * e->z_i + e->c_r;
				e->z_i = -(2 * e->z_i * e->tmp + e->c_i);
				e->i++;
			}
			ft_color(e);
			e->y++;
		}
		e->x++;
	}
	return (0);
}
