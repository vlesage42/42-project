/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_mandelbrot.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/23 14:47:45 by vlesage           #+#    #+#             */
/*   Updated: 2016/04/14 15:54:50 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		ft_mandel(t_env *e)
{
	e->zoom_x = (e->width * e->zoom) / (e->x2 - e->x1);
	e->zoom_y = (e->height * e->zoom) / (e->y2 - e->y1);
	e->c_r = e->x / e->zoom_x + e->x1;
	e->c_i = e->y / e->zoom_y + e->y1;
	e->z_r = 0;
	e->z_i = 0;
	e->i = 0;
	return (0);
}

int		ft_mandelbrot(t_env *e)
{
	e->x = 0;
	while (e->x < e->width)
	{
		e->y = 0;
		while (e->y < e->height)
		{
			ft_mandel(e);
			while ((e->z_r * e->z_r) - (e->z_i * e->z_i) < 4 &&
			e->i < e->ite_max)
			{
				e->tmp = e->z_r;
				e->z_r = e->z_r * e->z_r - e->z_i * e->z_i + e->c_r;
				e->z_i = 2 * e->z_i * e->tmp + e->c_i;
				e->i++;
			}
			ft_color(e);
			e->y++;
		}
		e->x++;
	}
	return (0);
}
