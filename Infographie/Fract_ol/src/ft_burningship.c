/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_burningship.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/24 21:20:51 by vlesage           #+#    #+#             */
/*   Updated: 2016/03/24 21:25:37 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		ft_burningship(t_env *e)
{
	e->x = 0;
	while (e->x < e->width)
	{
		e->y = 0;
		while (e->y < e->height)
		{
			ft_mandel(e);
			while ((e->z_r * e->z_r) - (e->z_i * e->z_i) < 4 &&
			e->i < e->ite_max)
			{
				e->tmp = e->z_r;
				e->z_r = e->z_r * e->z_r - e->z_i * e->z_i + e->c_r;
				e->z_i = fabs(2 * e->z_i * e->tmp) + e->c_i;
				e->i++;
			}
			ft_color(e);
			e->y++;
		}
		e->x++;
	}
	return (0);
}
