/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/24 20:59:51 by vlesage           #+#    #+#             */
/*   Updated: 2016/04/15 16:57:08 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		ft_init(t_env *e)
{
	if (e->av == 1)
		ft_init_mandelbrot(e);
	else if (e->av == 2)
		ft_init_julia(e);
	else if (e->av == 3)
		ft_init_mandelbar(e);
	else if (e->av == 4)
		ft_init_burningship(e);
	else if (e->av == 5)
		ft_init_celtic(e);
	e->zoom = 1;
	e->ite_max = 150;
	return (0);
}

void	ft_init_mandelbrot(t_env *e)
{
	e->x1 = -2.1;
	e->x2 = 0.6;
	e->y1 = -1.2;
	e->y2 = 1.2;
}

void	ft_init_mandelbar(t_env *e)
{
	e->x1 = -1.4;
	e->x2 = 0.8;
	e->y1 = -1.2;
	e->y2 = 1.2;
}

void	ft_init_burningship(t_env *e)
{
	e->x1 = -1.6;
	e->x2 = 1.0;
	e->y1 = -1.9;
	e->y2 = 1.0;
}

void	ft_init_celtic(t_env *e)
{
	e->x1 = -2.1;
	e->x2 = 0.6;
	e->y1 = -1.4;
	e->y2 = 1.4;
}
