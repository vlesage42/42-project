/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_draw.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/23 13:42:10 by vlesage           #+#    #+#             */
/*   Updated: 2016/03/11 06:21:14 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	my_pixel_put_to_image(t_env *e, int x, int y)
{
	unsigned char	r;
	unsigned char	g;
	unsigned char	b;

	e->img_color *= e->coef;
	r = (e->img_color >> 24);
	g = (e->img_color >> 16);
	b = (e->img_color);
	if (x >= 0 && x <= e->width && y >= 0 && y <= e->height)
	{
		e->data[y * e->sizeline + x * e->bpp / 8] = b;
		e->data[y * e->sizeline + x * e->bpp / 8 + 1] = g;
		e->data[y * e->sizeline + x * e->bpp / 8 + 2] = r;
	}
}

int		ft_color(t_env *e)
{
	if (e->i == e->ite_max)
	{
		e->img_color = mlx_get_color_value(e->mlx, 0x000000);
		my_pixel_put_to_image(e, e->x, e->y);
	}
	else
	{
		e->img_color = e->i * 255 / e->ite_max;
		e->img_color = mlx_get_color_value(e->mlx, e->img_color);
		my_pixel_put_to_image(e, e->x, e->y);
	}
	return (0);
}
