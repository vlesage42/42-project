/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cmd.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/04 20:33:57 by vlesage           #+#    #+#             */
/*   Updated: 2016/04/15 16:53:20 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		ft_change_fract(t_env *e, int keycode)
{
	int		cpt;

	cpt = e->av;
	if (keycode == 18 || keycode == 83)
		e->av = 1;
	if (keycode == 19 || keycode == 84)
		e->av = 2;
	if (keycode == 20 || keycode == 85)
		e->av = 3;
	if (keycode == 21 || keycode == 86)
		e->av = 4;
	if (keycode == 23 || keycode == 87)
		e->av = 5;
	if (cpt != e->av)
		ft_init(e);
	return (0);
}

int		ft_keycode(int keycode, t_env *e)
{
	ft_change_fract(e, keycode);
	if (keycode == 15)
		e->coef = 0xFF250000;
	if (keycode == 5)
		e->coef = 0x01312d00;
	if (keycode == 11)
		e->coef = 0x000186a0;
	if (keycode == 45)
		e->coef = 0x03030303;
	if (keycode == 49)
	{
		e->coef = 0x01020305;
		ft_init(e);
	}
	return (keycode);
}

int		ft_mouse_zoom(t_env *e, int button, int x, int y)
{
	if (button == 5 || button == 4)
	{
		e->x1 += ((double)x - e->height / 2) / e->width / e->zoom;
		e->x2 += ((double)x - e->height / 2) / e->width / e->zoom;
		e->y1 += ((double)y - e->width / 2) / e->height / e->zoom;
		e->y2 += ((double)y - e->width / 2) / e->height / e->zoom;
	}
	if (button == 5)
	{
		e->zoom *= 1.1;
		e->ite_max += 2;
	}
	else if (button == 4)
	{
		e->zoom /= 1.1;
		e->ite_max -= 2;
	}
	return (0);
}

int		mouse_motion(int x, int y, t_env *e)
{
	if (x >= 0 && x <= e->width && y >= 0 && y <= e->height)
	{
		if (e->av == 2 && e->motion == 1)
		{
			e->c_r = (double)x / (e->width * 2);
			e->c_i = (double)y / (e->height * 2);
		}
	}
	return (0);
}

int		loop_hook(t_env *e)
{
	mlx_clear_window(e->mlx, e->win);
	ft_memset(e->data, 0, (4 * e->width * e->height));
	expose_hook(e);
	return (0);
}
