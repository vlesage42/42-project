/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/02 14:24:21 by vlesage           #+#    #+#             */
/*   Updated: 2016/03/24 21:33:44 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		ft_aff_error(int err)
{
	if (err != 0)
	{
		ft_putstr("*- Error : Bad argument -*\n");
		ft_putstr("\n");
		ft_putstr("     *-* Argument *-*\n");
		ft_putstr("    ------------------ \n");
		ft_putstr("   |  - mandelbrot -  |\n");
		ft_putstr("   |    - julia -     |\n");
		ft_putstr("   |  - mandelbar -   |\n");
		ft_putstr("   | - burningship -  |\n");
		ft_putstr("   |    - celtic -    |\n");
		ft_putstr("    ------------------ \n");
		exit(0);
	}
	return (0);
}

int		ft_error(t_env *e, char *tab)
{
	int		err;

	if (tab == NULL)
		err = -1;
	else
	{
		if ((err = ft_strcmp(tab, "mandelbrot")) == 0)
			e->av = 1;
		else if ((err = ft_strcmp(tab, "julia")) == 0)
			e->av = 2;
		else if ((err = ft_strcmp(tab, "mandelbar")) == 0)
			e->av = 3;
		else if ((err = ft_strcmp(tab, "burningship")) == 0)
			e->av = 4;
		else if ((err = ft_strcmp(tab, "celtic")) == 0)
			e->av = 5;
	}
	ft_aff_error(err);
	return (0);
}
