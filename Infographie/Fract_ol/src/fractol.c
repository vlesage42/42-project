/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/22 17:19:29 by vlesage           #+#    #+#             */
/*   Updated: 2016/04/12 19:12:48 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		expose_hook(t_env *e)
{
	if (e->av == 1)
		ft_mandelbrot(e);
	else if (e->av == 2)
		ft_julia(e);
	else if (e->av == 3)
		ft_mandelbar(e);
	else if (e->av == 4)
		ft_burningship(e);
	else if (e->av == 5)
		ft_celtic(e);
	mlx_put_image_to_window(e->mlx, e->win, e->img, 0, 0);
	return (0);
}

int		key_hook(int keycode, t_env *e)
{
	if (keycode == 53)
	{
		mlx_destroy_image(e->mlx, e->img);
		mlx_destroy_window(e->mlx, e->win);
		exit(0);
	}
	ft_keycode(keycode, e);
	return (0);
}

int		mouse_hook(int button, int x, int y, t_env *e)
{
	if (button == 1)
		e->motion *= (-1);
	ft_mouse_zoom(e, button, x, y);
	return (0);
}

int		ft_fractol(t_env *e)
{
	e->mlx = mlx_init();
	e->win = mlx_new_window(e->mlx, e->width, e->height, "Fract'ol");
	e->img = mlx_new_image(e->mlx, e->width, e->height);
	e->data = mlx_get_data_addr(e->img, &e->bpp, &e->sizeline, &e->endian);
	mlx_hook(e->win, 12, 0, &expose_hook, e);
	mlx_hook(e->win, 2, 0, &key_hook, e);
	mlx_hook(e->win, 4, 0, &mouse_hook, e);
	mlx_hook(e->win, 6, 0, &mouse_motion, e);
	mlx_loop_hook(e->mlx, loop_hook, e);
	mlx_loop(e->mlx);
	return (0);
}

int		main(int argc, char **argv)
{
	t_env	e;

	if (argc == 2)
	{
		ft_error(&e, argv[1]);
		ft_init(&e);
		e.width = 700;
		e.height = 700;
		e.coef = 0x01020305;
		e.motion = (-1);
		ft_fractol(&e);
	}
	else
		ft_error(&e, NULL);
	return (0);
}
