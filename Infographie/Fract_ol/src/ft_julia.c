/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_julia.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/02 19:45:22 by vlesage           #+#    #+#             */
/*   Updated: 2016/04/14 15:55:02 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void	ft_init_julia(t_env *e)
{
	e->x1 = -1;
	e->x2 = 1;
	e->y1 = -1.2;
	e->y2 = 1.2;
	e->c_r = 0.285;
	e->c_i = 0.01;
}

int		ft_calc_julia(t_env *e)
{
	e->zoom_x = (e->width * e->zoom) / (e->x2 - e->x1);
	e->zoom_y = (e->height * e->zoom) / (e->y2 - e->y1);
	e->z_r = e->x / e->zoom_x + e->x1;
	e->z_i = e->y / e->zoom_y + e->y1;
	e->i = 0;
	return (0);
}

int		ft_julia(t_env *e)
{
	e->x = 0;
	while (e->x < e->width)
	{
		e->y = 0;
		while (e->y < e->height)
		{
			ft_calc_julia(e);
			while ((e->z_r * e->z_r) - (e->z_i * e->z_i) < 4 &&
			e->i < e->ite_max)
			{
				e->tmp = e->z_r;
				e->z_r = e->z_r * e->z_r - e->z_i * e->z_i + e->c_r;
				e->z_i = 2 * e->z_i * e->tmp + e->c_i;
				e->i++;
			}
			ft_color(e);
			e->y++;
		}
		e->x++;
	}
	return (0);
}
