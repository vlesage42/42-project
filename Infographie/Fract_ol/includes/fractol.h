/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/02/22 17:19:11 by vlesage           #+#    #+#             */
/*   Updated: 2016/04/14 16:10:04 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H

# include <mlx.h>
# include <math.h>
# include <stdio.h>
# include "libft.h"

typedef struct		s_env
{
	void			*mlx;
	void			*win;
	void			*img;
	int				av;
	char			*data;
	int				width;
	int				height;
	int				bpp;
	int				sizeline;
	int				endian;
	int				img_color;
	int				x;
	int				y;
	int				motion;
	double			zoom;
	double			x1;
	double			x2;
	double			y1;
	double			y2;
	int				ite_max;
	double			zoom_x;
	double			zoom_y;
	double			c_r;
	double			c_i;
	double			z_r;
	double			z_i;
	int				i;
	double			tmp;
	long long int	coef;
}					t_env;

int					expose_hook(t_env *e);
int					key_hook(int keycode, t_env *e);
int					mouse_hook(int button, int x, int y, t_env *e);
int					mouse_motion(int x, int y, t_env *e);
int					loop_hook(t_env *e);
int					ft_mandel(t_env *e);
void				ft_init_mandelbrot(t_env *e);
void				ft_init_julia(t_env *e);
void				ft_init_mandelbar(t_env *e);
void				ft_init_burningship(t_env *e);
void				ft_init_celtic(t_env *e);
int					ft_init(t_env *e);
int					ft_change_fract(t_env *e, int keycode);
int					ft_mandelbrot(t_env *e);
int					ft_mandelbar(t_env *e);
int					ft_julia(t_env *e);
int					ft_burningship(t_env *e);
int					ft_celtic(t_env *e);
int					ft_fractol(t_env *e);
int					ft_color(t_env *e);
void				my_pixel_put_to_image(t_env *e, int x, int y);
int					ft_error(t_env *e, char *tab);
int					ft_keycode(int keycode, t_env *e);
int					ft_mouse_zoom(t_env *e, int button, int x, int y);

#endif
