/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlist_remove_id.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/19 18:34:15 by vlesage           #+#    #+#             */
/*   Updated: 2015/12/19 18:48:15 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_dlist	*ft_dlist_remove_id(t_dlist *p_list, int pos)
{
	struct s_elem	*p_temp;
	int				i;

	if (p_list != NULL)
	{
		p_temp = p_list->p_head;
		i = 1;
		while (p_temp != NULL && i <= pos)
		{
			if (pos == i)
				p_list = ft_dlist_move(p_list, p_temp);
			else
				p_temp = p_temp->p_next;
			i++;
		}
	}
	return (p_list);
}
