/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlistinsert.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/18 17:47:32 by vlesage           #+#    #+#             */
/*   Updated: 2015/12/19 18:48:43 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_dlist	*ft_listmid(t_dlist *p_list, struct s_elem *p_temp, int data)
{
	struct s_elem	*p_new;

	p_new = malloc(sizeof(*p_new));
	if (p_new != NULL)
	{
		p_new->data = data;
		p_temp->p_next->p_prev = p_new;
		p_temp->p_prev->p_next = p_new;
		p_new->p_prev = p_temp->p_prev;
		p_new->p_next = p_temp;
		p_list->id++;
	}
	return (p_list);
}

t_dlist	*ft_dlistinsert(t_dlist *p_list, int data, int pos)
{
	struct s_elem	*p_temp;
	int				i;

	if (p_list != NULL)
	{
		p_temp = p_list->p_head;
		i = 1;
		while (p_temp != NULL && i <= pos)
		{
			if (pos == i)
			{
				if (p_temp->p_next == NULL)
					p_list = ft_dlistend(p_list, data);
				else if (p_temp->p_prev == NULL)
					p_list = ft_dlistbegin(p_list, data);
				else
					p_list = ft_listmid(p_list, p_temp, data);
			}
			else
				p_temp = p_temp->p_next;
			i++;
		}
	}
	return (p_list);
}
