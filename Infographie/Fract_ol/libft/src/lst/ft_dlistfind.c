/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlistfind.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/19 19:32:15 by vlesage           #+#    #+#             */
/*   Updated: 2015/12/19 19:38:41 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_dlist	*ft_dlistfind(t_dlist *p_list, int data)
{
	t_dlist			*ret;
	int				i;
	struct s_elem	*p_temp;

	ret = NULL;
	if (p_list != NULL)
	{
		p_temp = p_list->p_head;
		i = 0;
		while (p_temp != NULL && !i)
		{
			if (p_temp->data == data)
			{
				ret = ft_dlistnew();
				ret = ft_dlistend(ret, data);
				i = 1;
			}
			else
				p_temp = p_temp->p_next;
		}
	}
	return (ret);
}
