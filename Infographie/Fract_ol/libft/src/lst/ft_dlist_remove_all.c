/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlist_remove_all.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/19 18:12:09 by vlesage           #+#    #+#             */
/*   Updated: 2015/12/19 18:12:15 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_dlist	*ft_dlist_remove_all(t_dlist *p_list, int data)
{
	struct s_elem	*p_temp;
	struct s_elem	*p_del;

	if (p_list != NULL)
	{
		p_temp = p_list->p_head;
		while (p_temp != NULL)
		{
			if (p_temp->data == data)
			{
				p_del = p_temp;
				p_temp = p_temp->p_next;
				p_list = ft_dlist_move(p_list, p_del);
			}
			else
				p_temp = p_temp->p_next;
		}
	}
	return (p_list);
}
