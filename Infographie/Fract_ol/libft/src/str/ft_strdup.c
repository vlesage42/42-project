/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/03 16:12:36 by vlesage           #+#    #+#             */
/*   Updated: 2014/11/12 18:31:08 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./libft.h"
#include <string.h>
#include <stdlib.h>

char	*ft_strdup(const char *src)
{
	char	*dst;
	int		i;

	if (src == NULL)
		return (NULL);
	i = ft_strlen(src) + 1;
	dst = (char *)malloc(sizeof(*dst) * i);
	dst = ft_strcpy(dst, src);
	return (dst);
}
