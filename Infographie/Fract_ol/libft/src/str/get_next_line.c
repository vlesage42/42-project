/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/24 15:21:23 by vlesage           #+#    #+#             */
/*   Updated: 2015/03/03 20:12:55 by vlesage          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"
#include "libft.h"
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>

int		ft_position_line(char *buf, int c)
{
	int		i;

	i = -1;
	while (buf[++i])
		if (buf[i] == c)
			return (++i);
	return (-1);
}

int		ft_error(int nb, char *buf, char **line)
{
	if (nb < 0)
		return (-1);
	else if (nb == 0 && buf[0] == '\0' && **line == '\0')
		return (0);
	else if (nb == 0 && buf[0] == '\0')
		return (1);
	else
		return (2);
}

int		get_next_line(int const fd, char **line)
{
	static char	buf[BUFF_SIZE + 1];
	char		*tmp;
	int			nb;
	int			new_line;
	int			err;

	if (line == NULL)
		return (-1);
	tmp = ft_strdup(buf);
	*line = (char *)(malloc(sizeof(char)));
	while ((new_line = ft_position_line(tmp, '\n')) == -1)
	{
		*line = ft_strjoin(*line, tmp);
		free(tmp);
		nb = read(fd, buf, BUFF_SIZE);
		if ((err = ft_error(nb, buf, line)) != 2)
			return (err);
		buf[nb] = '\0';
		tmp = ft_strdup(buf);
	}
	ft_strlcpy(tmp, tmp, new_line);
	*line = ft_strjoin(*line, tmp);
	free(tmp);
	ft_strcpy(buf, &buf[new_line]);
	return (1);
}
