# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: vlesage <vlesage@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/02/22 17:14:32 by vlesage           #+#    #+#              #
#    Updated: 2016/04/14 18:44:42 by vlesage          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

.PHONY: all clean fclean re

SRC = fractol.c ft_draw.c ft_mandelbrot.c ft_mandelbar.c ft_error.c ft_julia.c\
	ft_cmd.c ft_init.c ft_burningship.c ft_celtic.c

NAME = fractol

SRCS = $(addprefix src/, $(SRC))

OBJ = $(SRCS:.c=.o)

CC = clang

CFLAGS = -Wall -Wextra -Werror -g

LIB = -L./minilibx_macos -lmlx -L./libft -lft -framework OpenGL -framework AppKit

INCLUDES = -I./includes -I./libft/includes

all: $(NAME)

$(NAME): $(OBJ) make_libft make_minilibx
	@$(CC) $(CFLAGS) $(LIB) $(OBJ) -o $(NAME)
	@echo "\033[32m$(NAME) OK !\033[0m"
	@echo "\033[32m      :::::::::: :::::::::      :::      :::::::: ::::::::::: ::::::::  :::\033[0m"
	@echo "\033[32m     :+:        :+:    :+:   :+: :+:   :+:    :+:    :+:    :+:    :+: :+:\033[0m"
	@echo "\033[32m    +:+        +:+    +:+  +:+   +:+  +:+           +:+    +:+    +:+ +:+\033[0m"
	@echo "\033[32m   :#::+::#   +#++:++#:  +#++:++#++: +#+           +#+    +#+    +:+ +#+\033[0m"
	@echo "\033[32m  +#+        +#+    +#+ +#+     +#+ +#+           +#+    +#+    +#+ +#+\033[0m"
	@echo "\033[32m #+#        #+#    #+# #+#     #+# #+#    #+#    #+#    #+#    #+# #+#\033[0m"
	@echo "\033[32m###        ###    ### ###     ###  ########     ###     ########  ##########\033[0m"

%.o: %.c
	@$(CC) $(CFLAGS) $(INCLUDES) -o $@ -c $<

make_libft:
	@make -C libft/

make_minilibx:
	@make -C minilibx_macos/

clean:
	@rm -f $(OBJ)
	@make -C libft/ clean
	@make -C minilibx_macos/ clean

fclean: clean
	@rm -f $(NAME)
	@echo "\033[31m    +\033[0m"
	@echo "\033[31m  .-'-. \033[0m"
	@echo "\033[31m / RIP \ \033[0m"
	@echo "\033[31m |     | \033[0m"
	@echo "\033[31m\\|  ___|// \033[0m"
	@echo "\033[31m$(NAME) delete !\033[0m"
	@make -C libft/ fclean

re: fclean all
